package eu.taxfree4u.client.services;

public final class ConstEX {
    /* EXTRA DATA NAMES */
    public static final String SERIALIZABLE_REQUEST = "SERIALIZABLE";
    public static final String EX_EXIT = "EXIT";
    public static final String EX_EVENT = "EVENT";
    public static final String EX_imgFilePathFrom = "imgFilePathFrom";
    public static final String EX_imgFilePathTo = "imgFilePathTo";
    public static final String EX_uri = "uri";
    public static final String EX_CARD_TYPE = "CARD_TYPE";
    public static final String EX_CARD_NAME= "CARD_NAME";
    public static final String EX_URL = "EX_URL";
    public static final String EX_CARD_POSITION = "CARD_ID";
    public static final String EX_TRANSACTION_ID = "TRANSACTION_ID";
    public static final String EX_CARD_NUMBER = "CARD_NUMBER";
    public static final String EX_CARD_CURRENCY = "CARD_CURRENCY";
    public static final String EX_CARD_AMMOUNT = "CARD_AMMOUNT";

}