//package eu.taxfree4u.client.services.gcm;
//
//import android.app.IntentService;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.preference.PreferenceManager;
//import android.util.Log;
//
//import com.google.android.gms.gcm.GoogleCloudMessaging;
//import com.google.android.gms.iid.InstanceID;
//import com.helpshift.Core;
//
//import java.io.IOException;
//
//public class RegistrationIntentService extends IntentService {
//
//    private static final String TAG         = "RegIntentService";
//    private static final String SENDER_ID   = "262906023699";  1:178343371199:android:31b35ee7ba0546d3
//
//
//    public RegistrationIntentService() {
//        super(TAG);
//    }
//
//    @Override
//    protected void onHandleIntent(Intent intent) {
//
//
//        // Make a call to Instance API
//        InstanceID instanceID = InstanceID.getInstance(this);
//        try {
//            // request token that will be used by the server to send push notifications
//            String token = instanceID.getToken(SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
//
////            utilLog.myLog("GCM Registration Token: " + token);
//
//            // pass along this data
//            sendRegistrationToServer(token);
//            saveToken(token);
//        } catch (IOException e) {
//
//        }
//    }
//
//    private void saveToken(String token) {
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//        sharedPreferences.edit().putString("token", token).apply();
//    }
//
//    private void sendRegistrationToServer(String token) {
//        // Add custom implementation, as needed.
//        Core.registerDeviceToken(this, token);
//
//        Log.d(TAG, "GCM token: "  +  token);
//    }
//}
