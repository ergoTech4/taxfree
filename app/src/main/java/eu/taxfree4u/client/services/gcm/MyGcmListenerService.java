//package eu.taxfree4u.client.services.gcm;
//
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.BitmapFactory;
//import android.graphics.Color;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.NotificationCompat;
//import android.util.Log;
//
//
//import com.helpshift.Core;
//
//import eu.taxfree4u.client.R;
//import eu.taxfree4u.client.activities.MainActivity;
//import eu.taxfree4u.client.tools.AppDelegate;
//
//public class MyGcmListenerService extends GcmListenerService {
//
//    private static final String TAG = "MyGcmListenerService";
//
//    @Override
//    public void onMessageReceived(String from, Bundle data) {
//        String origin = data.getString("origin");
//
//        if (origin != null && origin.equals("helpshift")) {
//            Core.handlePush(this, data);
//
//            AppDelegate.getInstance().setNotificationCount(1);
//        }
//
//        Log.d(TAG, "gcm:" + data.toString());
//
//        sendNotification(origin);
//    }
//
//    private void sendNotification(String messageBody) {
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(getNotificationIcon())
//
//                .setContentTitle("TaxFree Message")
//                .setContentText(messageBody)
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
//    }
//
//    private int getNotificationIcon() {
//        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
//        return useWhiteIcon ? R.drawable.logo_tf : R.drawable.ic_launcher;
//    }
//}