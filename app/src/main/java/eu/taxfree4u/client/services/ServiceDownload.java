package eu.taxfree4u.client.services;

import android.app.IntentService;
import android.content.Intent;

import java.io.Serializable;

import eu.taxfree4u.client.database.UtilDbEdite;
import eu.taxfree4u.client.database.UtilDbGet;
import eu.taxfree4u.client.tools.App;

public class ServiceDownload extends IntentService  {

    private int             event;
    private Serializable    serializableObj;
    private App             app;
    private UtilDbGet       utilDbGet;
    private UtilDbEdite     utilDbEdite;


    public ServiceDownload(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        utilDbEdite         = new UtilDbEdite(this);
        utilDbGet           = new UtilDbGet(this);


        app                 = (App) getApplicationContext();
        event               = intent.getIntExtra(ConstEX.EX_EVENT, 0);

        startCurrentRequest();

    }

    private void startCurrentRequest() {

        switch (event) {
            case ConstBC.BC_EVENT_HELLO:                refreshToken(); break;

        }
    }

    private void refreshToken() {


    }


}
