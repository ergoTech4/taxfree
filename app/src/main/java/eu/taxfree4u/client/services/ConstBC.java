package eu.taxfree4u.client.services;

public final class ConstBC {
    /* BROADCAST */
    public static final int BC_EVENT_REFRESH_TOKEN          = 1;
    public static final int BC_EVENT_TOAST                  = 2;

    public static final int BC_EVENT_RESIZE_IMAGE           = 3;

    public static final int BC_EVENT_SIGNUP_PHONE           = 5;
    public static final int BC_EVENT_SIGNUP_PIN             = 6;
    public static final int BC_EVENT_SIGNUP_PHOTO           = 7;
    public static final int BC_EVENT_SIGNUP_PASS            = 8;
    public static final int BC_EVENT_SIGNUP_CARD_ID         = 9;
    public static final int BC_EVENT_HELLO                  = 10;
    public static final int BC_EVENT_GET_AUTH_TOKEN         = 11;
    public static final int BC_EVENT_GET_CARD_LIST          = 12;
    public static final int BC_EVENT_GET_TRANSACTION_LIST   = 13;
    public static final int BC_EVENT_SIGNIN_PHONE           = 14;

    public static final int BC_EVENT_GET_STARTED            = 15;
    public static final int BC_EVENT_GET_MONEYPOLOLINK      = 16;

    public static final int BC_EVENT_GET_CARD_LIST_AVAILABLE_IN_REG = 17;

    public static final int BC_EVENT_CARD_VALIDATION        = 18;
    public static final int BC_EVENT_FORGOT_PASSWORD        = 19;
    public static final int BC_EVENT_FORGOT_PASSWORD1       = 20;
    public static final int BC_EVENT_FORGOT_PASSWORD2       = 21;

    public static final int BC_EVENT_GET_TRANSACTION_LIST_BEFORE       = 22;

    public static final int BC_EVENT_CHANGE_PASSWORD        = 23;
    public static final int BC_EVENT_GET_ACCOUNT_DETAILS    = 24;

    public static final int BC_EVENT_SIGNIN_PIN             = 25;

    public static final int BC_EVENT_RESEND_SMS             = 26;
}