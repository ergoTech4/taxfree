package eu.taxfree4u.client.services.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.activities.MainActivity;
import eu.taxfree4u.client.tools.AppDelegate;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            // TODO parse message  {custom={"view":2,"entity_id":"","entity":""}, title=TaxFree4U, message=123456}

            int view = -1;
            int subview = -1;
            int id  = -1;


            String json = remoteMessage.getData().get("custom");

            try {
                JSONObject obj = new JSONObject(json);
                view    = obj.optInt("view");
                subview = obj.optInt("subview");
                id      = obj.optInt("id");
            } catch (Exception t) {}

//            if ( AppDelegate.getInstance().isActivity() > 2 ) {
                sendNotification(remoteMessage.getData().get("message"), view, subview, id);
//            }

            if ( view == 2 ) {
                AppDelegate.getInstance().setNotificationCount(1);
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }


        Log.d (TAG, "isActivity: " +  AppDelegate.getInstance().isActivity());

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     * @param view
     */
    private void sendNotification(String messageBody, int view, int subview, int id) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.putExtra("view", view);
        intent.putExtra("subview", subview);
        intent.putExtra("id", id);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_tf)
                .setContentTitle("TaxFree4U")
                .setContentText(messageBody)
                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(pendingIntent);

        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN ) {
            if ( AppDelegate.getInstance().isActivity() == 0 ) {
                notificationBuilder.setDefaults(Notification.DEFAULT_ALL);
                defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                notificationBuilder.setPriority(Notification.PRIORITY_HIGH);
            } else if ( AppDelegate.getInstance().isActivity() == 1 ) {
                defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                notificationBuilder.setPriority(Notification.PRIORITY_LOW);
                notificationBuilder.setDefaults(Notification.DEFAULT_SOUND);
            } else if ( AppDelegate.getInstance().isActivity() == 2 ) {
                defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                notificationBuilder.setPriority(Notification.PRIORITY_MIN);
//                notificationBuilder.setDefaults(Notification.DEFAULT_SOUND);
            }
        }

        notificationBuilder.setSound(defaultSoundUri);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
//            AppDelegate.getInstance().setNotificationCount(1);
