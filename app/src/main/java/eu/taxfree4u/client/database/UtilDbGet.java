package eu.taxfree4u.client.database;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;

import eu.taxfree4u.client.tools.App;

public class UtilDbGet {

    private SQLiteDatabase db;
    private App             app;
    private Context context;

    public UtilDbGet(Context context) {
        app     = (App) (context).getApplicationContext();
        db      = app.getDb();
        this.context = context;
    }

    public String getFirstCardID() {
        String res;
        Cursor cur = db.query(ConstDb.tblCards, ConstDb.columnsCards, null, null, null, null, "cardId");
        if (cur.getCount() < 1) {
            res = null;
        } else {
            cur.moveToFirst();
            res = cur.getString(cur.getColumnIndex("cardID"));
        }
        cur.close();
        return res;
    }

    public int getCardCount() {
        Cursor cur = db.query(ConstDb.tblCards, ConstDb.columnsCards, null, null, null, null, null);
        int res = cur.getCount();
        cur.close();
        return res;
    }

    public int getFavTransactionCount() {
        Cursor cur = db.query(ConstDb.tblTransactions, ConstDb.columnsTransactions, "fav = 1", null, null, null, null);
        int res = cur.getCount();
        cur.close();
        return res;
    }

    public String getCardIdByOrderNumber(int orderNumber) {
        String res = "";
        Cursor cur = db.query(ConstDb.tblCards, ConstDb.columnsCards, null, null, null, null, "cardId");
        cur.moveToPosition(orderNumber);
        res = cur.getString(cur.getColumnIndex("cardID"));
        cur.close();
        return res;
    }

//    public Card getCardDataByOrderNumber(int orderNumber) {
//        Cursor cur = db.query(ConstDb.tblCards, ConstDb.columnsCards, null, null, null, null, "cardId");
//        cur.moveToPosition(orderNumber);
//
//        String cardID = cur.getString(cur.getColumnIndex("cardID"));
//        String cardNo = cur.getString(cur.getColumnIndex("cardNo"));
//        String cardCurrency = cur.getString(cur.getColumnIndex("cardCurrency"));
//        String availableBalance = cur.getString(cur.getColumnIndex("availableBalance"));
//        String holdBalance = cur.getString(cur.getColumnIndex("holdBalance"));
//        String effectiveDate = cur.getString(cur.getColumnIndex("effectiveDate"));
//        String expiryDate = cur.getString(cur.getColumnIndex("expiryDate"));
//        String cardEmbossLine = cur.getString(cur.getColumnIndex("cardEmbossLine"));
//        Card card = new Card(availableBalance, cardCurrency, cardEmbossLine, cardID, cardNo, effectiveDate, expiryDate, holdBalance);
//        cur.close();
//        return card;
//    }

    public String getPrevDate(int position, String cardId) {
        String dateTimeUtcPrev = null;
        Cursor cur = db.query(ConstDb.tblTransactions, ConstDb.columnsTransactionsLight,
                "cardId = " + cardId, null, null, null, "id DESC");
        if ( position > 0 ) {
            try {
                cur.moveToPosition(position - 1);
                dateTimeUtcPrev = cur.getString(cur.getColumnIndex("dateTimeUtc"));
            } catch (CursorIndexOutOfBoundsException ignore) {}
        }
        cur.close();
        return dateTimeUtcPrev;
    }

    public String getNextDate(int position, String cardId) {
        String dateTimeUtcNext = null;
        Cursor cur = db.query(ConstDb.tblTransactions, ConstDb.columnsTransactionsLight,
                "cardId = " + cardId, null, null, null, "id");
        cur.moveToPosition(position);
        if (!cur.isLast()) {

            try {
                cur.moveToPosition(position + 1);
                dateTimeUtcNext = cur.getString(cur.getColumnIndex("dateTimeUtc"));

            }catch (CursorIndexOutOfBoundsException e) {

            }
        }
        cur.close();
        return dateTimeUtcNext;
    }

//    public Transaction getTransaction(int transactionId) {
//        Cursor cur = db.query(ConstDb.tblTransactions, ConstDb.columnsTransactions,
//                "id = " + transactionId, null, null, null, null);
//        cur.moveToLast();
//        String category = cur.getString(cur.getColumnIndex("category"));
//        String id = cur.getString(cur.getColumnIndex("id"));
//        String dateTimeUtc = cur.getString(cur.getColumnIndex("dateTimeUtc"));
//        String amount = cur.getString(cur.getColumnIndex("amount"));
//        String currencyCode = cur.getString(cur.getColumnIndex("currencyCode"));
//        String amountInCardCurrency = cur.getString(cur.getColumnIndex("amountInCardCurrency"));
//        String additionalCommissionInCardCurrency = cur.getString(cur.getColumnIndex("additionalCommissionInCardCurrency"));
//        String info = cur.getString(cur.getColumnIndex("info"));
//        String transactionType = cur.getString(cur.getColumnIndex("transactionType"));
//        String mcc = cur.getString(cur.getColumnIndex("mcc"));
//        String comment = cur.getString(cur.getColumnIndex("comment"));
//        String cardId = cur.getString(cur.getColumnIndex("cardId"));
//        String imgUrl = cur.getString(cur.getColumnIndex("imgUrl"));
//        cur.close();
//
//        return new Transaction(additionalCommissionInCardCurrency,
//                amount,
//                amountInCardCurrency,
//                cardId,
//                category,
//                comment,
//                currencyCode,
//                dateTimeUtc,
//                id,
//                imgUrl,
//                info,
//                mcc,
//                transactionType);
//    }

//    public Transaction getFavoritesTransaction(int position) {
//        Cursor cur = db.query(ConstDb.tblTransactions, ConstDb.columnsTransactions,
//                "fav = 1", null, null, null, null);
//        cur.moveToPosition(position);
//
//        String category = cur.getString(cur.getColumnIndex("category"));
//        String id = cur.getString(cur.getColumnIndex("id"));
//        String dateTimeUtc = cur.getString(cur.getColumnIndex("dateTimeUtc"));
//        String amount = cur.getString(cur.getColumnIndex("amount"));
//        String currencyCode = cur.getString(cur.getColumnIndex("currencyCode"));
//        String amountInCardCurrency = cur.getString(cur.getColumnIndex("amountInCardCurrency"));
//        String additionalCommissionInCardCurrency = cur.getString(cur.getColumnIndex("additionalCommissionInCardCurrency"));
//        String info = cur.getString(cur.getColumnIndex("info"));
//        String transactionType = cur.getString(cur.getColumnIndex("transactionType"));
//        String mcc = cur.getString(cur.getColumnIndex("mcc"));
//        String comment = cur.getString(cur.getColumnIndex("comment"));
//        String cardId = cur.getString(cur.getColumnIndex("cardId"));
//        String imgUrl = cur.getString(cur.getColumnIndex("imgUrl"));
//        cur.close();
//
//        return new Transaction(additionalCommissionInCardCurrency,
//                amount,
//                amountInCardCurrency,
//                cardId,
//                category,
//                comment,
//                currencyCode,
//                dateTimeUtc,
//                id,
//                imgUrl,
//                info,
//                mcc,
//                transactionType);
//    }
}