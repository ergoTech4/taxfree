package eu.taxfree4u.client.database;

public final class ConstStatus {

    /* СТАТУСЫ */
    public static final String ST_TRUE = "TRUE";
    public static final String ST_FALSE = "FALSE";
    public static final String ST_NOINTERNET = "STATUS_NOINTERNET";
    public static final String ST_CONNECTION_ABSENT = "STATUS_CONNECTION_ABSENT";
    public static final String ST_MAKE_LATER = "STATUS_MAKE_LATER";
    public static final String ST_SERVERERROR = "SERVERERROR";
    public static final String ST_ERROR = "ERROR";
    public static final String ST_OLD = "OLD";
    public static final String ST_ESC = "ESC";
    public static final String ST_SUCCESS = "SUCCESS";

    public static final String ST_TERMS = "ST_TERMS";
}
