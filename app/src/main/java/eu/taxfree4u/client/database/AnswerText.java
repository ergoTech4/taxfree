package eu.taxfree4u.client.database;

public class AnswerText {
    public String status;
    public String text;
    public String log;

    public AnswerText(String status, String text, String log) {
        this.status = status;
        this.text = text;
        this.log = log;
    }
}
