package eu.taxfree4u.client.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import org.json.JSONArray;

import eu.taxfree4u.client.tools.App;

public class UtilDbEdite {

    private static final String TAG = "UtilDbEdite";
    private SQLiteDatabase db;

    private App app;
    private Context context;

    public UtilDbEdite(Context context) {
        app         = (App) (context).getApplicationContext();
        db          = app.getDb();
        this.context = context;
    }

    // метод для создания строки для вставки в БД *******************************************
    private String createInsert(final String tableName, final String[] columnNames) {
        if (tableName == null || columnNames == null || columnNames.length == 0) {
            throw new IllegalArgumentException();
        }
        final StringBuilder s = new StringBuilder();
        s.append("INSERT OR IGNORE INTO ").append(tableName).append(" (");
        for (String column : columnNames) {
            s.append(column).append(" ,");
        }
        int length = s.length();
        s.delete(length - 2, length);
        s.append(") VALUES( ");
        for (int i = 0; i < columnNames.length; i++) {
            s.append(" ? ,");
        }
        length = s.length();
        s.delete(length - 2, length);
        s.append(")");
        return s.toString();
    }

    // запрос на удаление **********************************************************************

    public AnswerText deleteContentInTable(String tableName) {
        try {
            db.execSQL("DELETE FROM " + tableName);
        } catch (Exception e) {
            return new AnswerText(ConstStatus.ST_FALSE, "", "Чистка в БД таблицы " + tableName);
        }
        return new AnswerText(ConstStatus.ST_TRUE, "", "");
    }

    public AnswerText writeTrips(JSONArray transactions, String cardId) {
        String table = ConstDb.tblTransactions;
        String status = ConstStatus.ST_TRUE;
        String msgLog = "";
//        deleteContentInTable(table);

        String INSERT_QUERY = createInsert(table, ConstDb.columnsTransactions);
        final SQLiteStatement statement = db.compileStatement(INSERT_QUERY);


            db.beginTransaction();
            for (int i = 0; i < transactions.length(); i++) {
                try {
                    statement.clearBindings();
                    statement.bindString(1, transactions.getJSONObject(i).getString("category"));
                    statement.bindString(2, transactions.getJSONObject(i).getString("id"));
                    statement.bindString(3, transactions.getJSONObject(i).getString("dateTimeUtc"));
                    statement.bindString(4, transactions.getJSONObject(i).getString("amount"));
                    statement.bindString(5, transactions.getJSONObject(i).getString("currencyCode"));
                    statement.bindString(6, transactions.getJSONObject(i).getString("amountInCardCurrency"));
                    statement.bindString(7, transactions.getJSONObject(i).getString("additionalCommissionInCardCurrency"));
                    statement.bindString(8, transactions.getJSONObject(i).getString("info"));
                    statement.bindString(9, transactions.getJSONObject(i).getString("transactionType"));
                    statement.bindString(10, transactions.getJSONObject(i).getString("mcc"));
                    statement.bindString(11, transactions.getJSONObject(i).getString("comment"));
                    statement.bindString(12, cardId);
                    statement.bindString(13, transactions.getJSONObject(i).getString("imgUrl"));

//                    if ( i % 3 == 0 ) {
//                        statement.bindString(14, "1");
//                    } else {
//                        statement.bindString(14, "0");
//                    }
                    statement.execute();
                } catch (Exception ignored) {
                    Log.d(TAG, "sql error: "  + ignored);
                }
            }
            db.setTransactionSuccessful();


        db.endTransaction();
        return new AnswerText(status, "", msgLog);
    }

    // запрос на запись ************************************************************************
    public AnswerText writeCards(JSONArray user) {
        String table = ConstDb.tblCards;
        String status = ConstStatus.ST_TRUE;
        String msgLog = "";
        deleteContentInTable(table);

        String INSERT_QUERY = createInsert(table, ConstDb.columnsCards);
        final SQLiteStatement statement = db.compileStatement(INSERT_QUERY);

        try {
            db.beginTransaction();
            for (int i = 0; i < user.length(); i++) {
                statement.clearBindings();
                statement.bindString(1, user.getJSONObject(i).getString("cardID"));
                statement.bindString(2, user.getJSONObject(i).getString("cardNo"));
                statement.bindString(3, user.getJSONObject(i).getString("cardCurrency"));
                statement.bindString(4, user.getJSONObject(i).getString("availableBalance"));
                statement.bindString(5, user.getJSONObject(i).getString("holdBalance"));
                statement.bindString(6, user.getJSONObject(i).getString("effectiveDate"));
                statement.bindString(7, user.getJSONObject(i).getString("expiryDate"));
                statement.bindString(8, user.getJSONObject(i).getString("cardEmbossLine"));
                statement.execute();
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            status = ConstStatus.ST_FALSE;
            msgLog = "Запись в БД " + table;
        } finally {
            db.endTransaction();
        }

        return new AnswerText(status, "", msgLog);
    }
}