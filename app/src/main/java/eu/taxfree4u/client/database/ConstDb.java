package eu.taxfree4u.client.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class ConstDb {
    private ConstDb() {}

    /* TABLES */
    public static final String tblCards         = "cards";
    public static final String tblTransactions  = "transactions";
    public static final String tblTrips         = "trips";

    public static final String[] columnsCards = {
            "cardID",
            "cardNo",
            "cardCurrency",
            "availableBalance",
            "holdBalance",
            "effectiveDate",
            "expiryDate",
            "cardEmbossLine"};

    public static final String[] columnsTransactions = {
            "category",
            "id",
            "dateTimeUtc",
            "amount",
            "currencyCode",
            "amountInCardCurrency",
            "additionalCommissionInCardCurrency",
            "info",
            "transactionType",
            "mcc",
            "comment",
            "cardId",
            "imgUrl",
            "fav"};

    public static final String[] columnsTransactionsLight = {
            "_id",
            "category",
            "id",
            "dateTimeUtc",
            "amount",
            "currencyCode",
            "amountInCardCurrency",
            "additionalCommissionInCardCurrency",
            "info",
            "transactionType",
            "mcc",
            "comment",
            "cardId",
            "imgUrl",
            "fav"};

    public static class DatabaseHelper extends SQLiteOpenHelper {

        public static final String DB_NAME = "TAXXFREE4U.sqlite";

        public DatabaseHelper(Context context) {
            super(context, DB_NAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("CREATE TABLE "+ tblCards + " (" +
                    "_id" + " INTEGER , "+
                    "cardID" + " INT NULL , "+
                    "cardNo" + " TEXT NULL , "+
                    "cardCurrency" + " TEXT NULL , "+
                    "availableBalance" + " TEXT NULL , "+
                    "holdBalance" + " TEXT NULL , "+
                    "effectiveDate" + " TEXT NULL , "+
                    "expiryDate" + " TEXT NULL , "+
                    "cardEmbossLine" + " TEXT NULL)");

    //        CREATE TABLE "cards" (
    //                _id INTEGER
    //                , cardID INT NULL,
    //        cardNo TEXT NULL,
    //                cardCurrency TEXT NULL, availableBalance
    //        TEXT NULL, holdBalance TEXT NULL, effectiveDate TEXT NULL, expiryDate TEXT NULL,
    //        cardEmbossLine TEXT NULL)



            db.execSQL("CREATE TABLE "+ tblTrips + " (" +
                    "_id INT ," +
                    "category TEXT ," +
                    "id INT ," +
                    "dateTimeUtc TEXT ," +
                    "amount TEXT ," +
                    "currencyCode TEXT ," +
                    "amountInCardCurrency TEXT ," +
                    "additionalCommissionInCardCurrency TEXT ," +
                    "info TEXT ," +
                    "transactionType TEXT ," +
                    "mcc TEXT ," +
                    "comment TEXT ," +
                    "cardId INT ," +
                    "imgUrl TEXT ," +
                    "fav INT , " +
                    "PRIMARY KEY(id)" +
                    ")");

    //        InsertDepts(db);
    //
    //        CREATE TABLE transactions (
    //        `_id`	INT,
    //        `category`	TEXT,
    //        `id`	INT,
    //        `dateTimeUtc`	TEXT,
    //        `amount`	TEXT,
    //        `currencyCode`	TEXT,
    //        `amountInCardCurrency`	TEXT,
    //        `additionalCommissionInCardCurrency`	TEXT,
    //        `info`	TEXT,
    //        `transactionType`	TEXT,
    //        `mcc`	TEXT,
    //        `comment`	TEXT,
    //        `cardId`	INT,
    //        `imgUrl`	TEXT,
    //        `fav`	INT,
    //                PRIMARY KEY(id)
    //        )
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS "+ tblCards);
            db.execSQL("DROP TABLE IF EXISTS "+ tblTransactions);

            onCreate(db);
        }
    }
}