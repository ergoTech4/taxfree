package eu.taxfree4u.client.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import eu.taxfree4u.client.R;

public class VideoActivity extends Activity {

    private TextView skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_video);

        String LINK = "android.resource://" + getPackageName() + "/" + R.raw.video;

        VideoView videoHolder = (VideoView) findViewById(R.id.video_view);
        Uri video = Uri.parse(LINK);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        videoHolder.setMediaController(new MediaController(this));
        videoHolder.setVideoURI(video);
        videoHolder.start();

        initialize();

        videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                boolean tutorial = getIntent().getBooleanExtra("tutorial", false);
                if ( tutorial ) {
                    startActivity(new Intent(VideoActivity.this, SignInActivity.class));
                    finish();
                } else {
                    onBackPressed();
                }
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean tutorial = getIntent().getBooleanExtra("tutorial", false);
                if ( tutorial ) {
                    startActivity(new Intent(VideoActivity.this, SignInActivity.class));
                    finish();
                } else {
                   onBackPressed();
                }
            }
        });

    }

    private void initialize() {
        skip        = (TextView) findViewById(R.id.video_skip_tv);
    }
}
