package eu.taxfree4u.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import eu.taxfree4u.client.R;

import eu.taxfree4u.client.fragments.ViewPageFragment;
import eu.taxfree4u.client.tools.AppDelegate;
import me.relex.circleindicator.CircleIndicator;

public class PromoActivity extends FragmentActivity {

    private Button          btnStart;
    private ViewPager       pager;
    private PagerAdapter    pagerAdapter;
    private CircleIndicator titleIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_promo);

        if ( !AppDelegate.getInstance().getNeedToLogin() ) {
            AppDelegate.getInstance().setToken("");
            AppDelegate.getInstance().setNeedToLogin(true);
        }


        if ( AppDelegate.getInstance().getToken().length() > 1 ) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {

            initialize();

            pager.setAdapter(pagerAdapter);
            titleIndicator.setViewPager(pager);

            btnStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(PromoActivity.this, VideoActivity.class);
                    intent.putExtra("tutorial", true);
                    startActivity(intent);
                    finish();
                }
            });
        }

    }

    private void initialize() {
        pagerAdapter    = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        btnStart        = (Button) findViewById(R.id.promo_start);
        pager           = (ViewPager) findViewById(R.id.pager);
        titleIndicator  = (CircleIndicator)findViewById(R.id.titles);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: return ViewPageFragment.instantiate(getString(R.string.preview1), R.drawable.image_preview1, R.color.black);
                case 1: return ViewPageFragment.instantiate(getString(R.string.preview2), R.drawable.image_preview2, R.color.white);
                case 2: return ViewPageFragment.instantiate(getString(R.string.preview3), R.drawable.image_preview3, R.color.white);
                case 3: return ViewPageFragment.instantiate(getString(R.string.preview4), R.drawable.image_preview4, R.color.black);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

}
