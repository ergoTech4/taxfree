package eu.taxfree4u.client.activities;


import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.widget.CheckBox;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Modifier;
import java.net.InetAddress;
import java.util.Locale;
import java.util.regex.Pattern;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.interfaces.ServiceApi;
import eu.taxfree4u.client.tools.AppDelegate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.R.attr.versionCode;


public class SignInActivity extends AppCompatActivity {

    private static final String     TAG = "SignInActivity";
    private MaterialEditText        email, password, confirm;
    private Button                  signIn;
    private ProgressDialog          progressDialog;
    private TextView                login, register, forgotPass, termsAndCond, promocode, promocodeTV;
    private ImageView               mainImage1, mainImage2;
    private CheckBox                terms;
    private String                  promocodeText = "";

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

//    private final String clientId       = "2_3e8ski6ramyo4wc04ww44ko84w4sowgkkc8ksokok08o4k8osk";
//    private final String clientSecret   = "592xtbslpsw08gow4s4s4ckw0cs0koc0kowgw8okg8cc0oggwk";
//    private final String baseUrl        = "http://tax-free-4u.com/app_dev.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        if ( AppDelegate.getInstance().getFireBasePushToken().length() == 0 ) {
            String token = FirebaseInstanceId.getInstance().getToken();
            AppDelegate.getInstance().saveFireBasePushToken(token);
        }

        initialize();
        initFields();

        login.setSelected(true);
        register.setSelected(false);

        clickListener();
    }

    private void clickListener() {

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( register.isSelected() ) {

                    if ( !terms.isChecked()  ) {
                        myToast(getString(R.string._agree_with_terms));
                    } else if ( password.getText().length() > 1 && !password.getText().toString().equals(confirm.getText().toString()) ) {
                        myToast(getString(R.string._password_dont_match));
                    } else {
                        registration(email.getText().toString(), password.getText().toString());
                    }
                } else {
                    signin(email.getText().toString(), password.getText().toString());
                }
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogin();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirm.setVisibility(View.VISIBLE);
                if ( !register.isSelected() ) {
                    register.setSelected(true);
                    login.setSelected(false);
                    signIn.animate().translationY(signIn.getHeight()).setDuration(500).setInterpolator(new AccelerateDecelerateInterpolator());
                    signIn.setText(getResources().getString(R.string._reg));
                    Animation fadeIn = AnimationUtils.loadAnimation(SignInActivity.this, R.anim.fade_in);
                    mainImage2.startAnimation(fadeIn);
                    terms.setVisibility(View.VISIBLE);
                    termsAndCond.setVisibility(View.VISIBLE);
                    forgotPass.setVisibility(View.INVISIBLE);
                    register.setBackgroundResource(R.drawable.underline);
                    login.setBackgroundColor(getResources().getColor(R.color.white));
                    promocode.setVisibility(View.VISIBLE);
                    promocodeTV.setVisibility(View.VISIBLE);
                }
            }
        });

        password.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1 && !register.isSelected() ) {
//                    startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    signin(email.getText().toString(), password.getText().toString());

                    return true;
                }
                return false;
            }
        });

        confirm.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( !terms.isChecked()  ) {
                    myToast(getString(R.string._agree_with_terms));
                } else if ( keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 1  ) {
//                    startActivity(new Intent(SignInActivity.this, MainActivity.class));
                    if ( confirm.getText().toString().equals(password.getText().toString()) ) {
                        registration(email.getText().toString(), password.getText().toString());
                    } else {
                        myToast(getString(R.string._password_dont_match));
                    }
                    return true;
                }
                return false;
            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignInActivity.this, ForgotPassword.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if ( email.getText().toString().length() > 5 ) {
                    intent.putExtra("email", email.getText().toString());
                }
                startActivity(intent);
            }
        });

        termsAndCond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, FaqActivity.class).putExtra("faq", 1));
            }
        });

        promocode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promocodeDialog();
            }
        });

    }

    private void showLogin() {
        confirm.setVisibility(View.GONE);
        if ( !login.isSelected() ) {
            login.setSelected(true);
            register.setSelected(false);
            signIn.animate().translationY(0).setDuration(500).setInterpolator(new AccelerateDecelerateInterpolator());
            signIn.setText(getResources().getString(R.string._login));
            Animation fadeout = AnimationUtils.loadAnimation(SignInActivity.this, R.anim.fade_out);
            mainImage2.startAnimation(fadeout);
            terms.setVisibility(View.GONE);
            termsAndCond.setVisibility(View.GONE);
            forgotPass.setVisibility(View.VISIBLE);
            login.setBackgroundResource(R.drawable.underline);
            register.setBackgroundColor(getResources().getColor(R.color.white));
            promocode.setVisibility(View.GONE);
            promocodeTV.setVisibility(View.GONE);
        }
    }

    private boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    private void signin(String email, String password) {
        if ( checkEmail(email) ) {
            showMyProgressDialog(getString(R.string._login));
            hideKeyboard();
            String android_id = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            Gson gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ServiceApi serviceApi = retrofit.create(ServiceApi.class);

            Call<JsonObject> call = serviceApi.login(Locale.getDefault().getLanguage(), email, password,
                    android_id,
                    prepareDeviceData());

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.body() != null) {
                        Log.d(TAG, "response.body():" + response.body());

                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());

                            if (jsonObject.optInt("status") == 0) {

                                JSONObject jsonData = new JSONObject(jsonObject.optString("data"));

                                AppDelegate.getInstance().setToken(jsonData.optString("access-token"));
                                AppDelegate.getInstance().setCurrentTripId(jsonData.optInt("current_trip_id"));
                                AppDelegate.getInstance().setHasPassport(jsonData.optBoolean("has_passport"));
                                AppDelegate.getInstance().setFirstLogin(jsonData.optInt("first_login"));

                                Log.d(TAG, "jsonData.optInt:" + jsonData.optInt("current_trip_id"));
//
//                                if (jsonData.optInt("current_trip_id") != -1) {
//                                    loadCurrentTrip();

//                                } else {
                                    startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                    finish();
//                                }
                            } else {
                                JSONObject jsonObjectError = new JSONObject(jsonObject.optString("error"));
                                myToast(jsonObjectError.optString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(errorConvector(response));
                            JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                            myToast(jsonObject1.optString("message"));

                            Log.d(TAG, "response error code:" + response.code());
                            Log.d(TAG, "response error:" + jsonObject1.optString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "response error:" + errorConvector(response));
                        }
                    }
                    dismissDialog();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d(TAG, "response error: " + t.getMessage());
                    dismissDialog();
                }
            });
        } else {
            myToast(getString(R.string.validate_email));
        }
    }

    private String prepareDeviceData() {
        JSONObject data = new JSONObject();

        String token = AppDelegate.getInstance().getFireBasePushToken();


        Log.d(TAG, "device_data: push token   " + token);

        try {
            String versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

            data.put("push_token", token);
            data.put("build_number", versionCode);
            data.put("description", "android phone");

            data.put("model", getDeviceName());
            data.put("platform_name", "android");
            data.put("platform_version", Build.VERSION.RELEASE);

            data.put("app_language", Locale.getDefault().getLanguage());
            data.put("os_language", Locale.getDefault());

        } catch (JSONException | PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "device_data:   " + data.toString());

        return data.toString();
    }


    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

//        String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
//                phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
//            phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }

    private void loadCurrentTrip() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getCurrentTrip(AppDelegate.getInstance().getToken());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if ( jsonObject.optInt("status") == 0 ) {

                            JSONObject jsonData = new JSONObject(jsonObject.optString("data"));



                            startActivity(new Intent(SignInActivity.this, MainActivity.class));
                            finish();

                        } else {
                            JSONObject jsonObjectError = new JSONObject(jsonObject.optString("error"));
                            myToast(jsonObjectError.optString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        myToast(jsonObject1.optString("message"));

                        Log.d(TAG, "response error code:" + response.code());
                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
                dismissDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
                dismissDialog();
            }
        });
    }

    private void registration(final String email, final String password) {
        if ( checkEmail(email) ) {
            showMyProgressDialog(getString(R.string._registration));
            hideKeyboard();

            Gson gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ServiceApi serviceApi = retrofit.create(ServiceApi.class);

            // TODO set correct token

            Call<JsonObject> call = serviceApi.registration(Locale.getDefault().getLanguage(), email, password,
                    Settings.Secure.getString(getBaseContext().getContentResolver(),
                            Settings.Secure.ANDROID_ID), prepareDeviceData(), promocodeText);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.body() != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());

                            if (jsonObject.optInt("status") == 0) {
//                            AppDelegate.getInstance().setToken(jsonObject.optString("token"));
                                myToast(getString(R.string._confirm_email));
                                signin(email, password);
                                showLogin();
                            } else {
                                JSONObject jsonObjectError = new JSONObject(jsonObject.optString("error"));
                                myToast(jsonObjectError.optString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
//                else {
//                    try {
//                        JSONObject jsonObject = new JSONObject(errorConvector(response));
//                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));
//
//                        myToast(jsonObject1.optString("message"));
//
//                        Log.d(TAG, "response error code:" + response.code());
//                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG, "response error:" + errorConvector(response));
//                    }
//                }
                    dismissDialog();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d(TAG, "response error: " + t.getMessage());
                    dismissDialog();
                }
            });
        } else {
            myToast(getString(R.string.validate_email));
        }


    }

    private void initFields() {
        if ( AppDelegate.getInstance().getUser() != null ) {
            email.setText(AppDelegate.getInstance().getUser().email);
            email.setSelection(AppDelegate.getInstance().getUser().email.length());
        }

        terms.setChecked(false);
    }

    private void initialize() {
        email       = (MaterialEditText) findViewById(R.id.sing_in_email);
        password    = (MaterialEditText) findViewById(R.id.sing_in_password);
        confirm     = (MaterialEditText) findViewById(R.id.sing_in_confirm);

        signIn      = (Button) findViewById(R.id.sign_in_button);

        login       = (TextView) findViewById(R.id.sign_in_login);
        register    = (TextView) findViewById(R.id.sign_in_register);
        forgotPass  = (TextView) findViewById(R.id.sign_in_forgot_password);

        termsAndCond = (TextView) findViewById(R.id.sign_in_terms_tv);
        promocode   = (TextView) findViewById(R.id.promocode);
        promocodeTV = (TextView) findViewById(R.id.promocode_text);

        mainImage1  = (ImageView) findViewById(R.id.sign_in_image1);
        mainImage2  = (ImageView) findViewById(R.id.sign_in_image2);


        terms       = (CheckBox) findViewById(R.id.sign_in_terms);

    }

    private void myToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)this.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException ignored){}
    }

    protected void showMyProgressDialog(String msg) {
        if ( progressDialog == null || !progressDialog.isShowing() ) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
            try {
                progressDialog.show();
            } catch (Exception e) {
            }
        }
    }

    protected void dismissDialog() {
        if ( progressDialog != null && progressDialog.isShowing() ) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {}
        }
    }

    private String errorConvector(Response<JsonObject> response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if ( isNetworkConnected() ) {
            myToast(getString(R.string.no_internet));
        }

    }

    public void promocodeDialog() {
        final Dialog delDialog = new Dialog(this, R.style.Base_Theme_AppCompat_Dialog);
        delDialog.setContentView(R.layout.dialog_promocode);

        final TextView ok    = (TextView)       delDialog.getWindow().findViewById(R.id.account_yes);
        final MaterialEditText editText   = (MaterialEditText) delDialog.getWindow().findViewById(R.id.promocode_field);

        editText.setText(promocodeText);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)SignInActivity.this.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                try {
                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                } catch (NullPointerException ignored){}
                promocodeText = editText.getText().toString();
                promocodeTV.setText(promocodeText);
                hideKeyboard();


                delDialog.dismiss();
            }
        });

        delDialog.show();
    }
}
