package eu.taxfree4u.client.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.adapters.ReceiptsAdapter;
import eu.taxfree4u.client.interfaces.ChatInterface;
import eu.taxfree4u.client.model.Chat;
import eu.taxfree4u.client.model.Receipt;
import eu.taxfree4u.client.tools.AppDelegate;

public class ReceiptsGridActivity extends AppCompatActivity implements ChatInterface {

    private static final String TAG = "InvoiceActivity";

    private ImageView backButton, fromAlbum;
    private TextView clientName, sendButton;
    private GridView invoiceGrid;
    private ReceiptsAdapter adapter;

    private ArrayList<Receipt>  invoices = new ArrayList<>();
    private ArrayList<Receipt>  sendList = new ArrayList<>();
    private FirebaseStorage     storage  = FirebaseStorage.getInstance();
    private DatabaseReference   mFirebaseRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipts);

        final StorageReference storageRef = storage.getReferenceFromUrl(getString(R.string.base_firebase));


        FirebaseDatabase base = FirebaseDatabase.getInstance();
        mFirebaseRef = base.getReference("firebase/chat/rooms/" + AppDelegate.getInstance().getFireBaseRoom());
        mFirebaseRef = mFirebaseRef.child("/messages/");

        initialize();

        invoices = AppDelegate.getInstance().getReceipsList();

        adapter = new ReceiptsAdapter(invoices, this);

        invoiceGrid.setAdapter(adapter);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( sendList.size() > 0 ) {
//                    sendFileFirebase(storageRef, sendList.get(0).file);
                    for ( int i = 0; i < sendList.size(); i++ ) {
                        sendFileFirebase(storageRef, sendList.get(i));
                    }

                    finish();
                }
            }
        });
    }

    private void initialize() {
        clientName  = (TextView) findViewById(R.id.title_text);
        sendButton = (TextView) findViewById(R.id.title_send);

        backButton  = (ImageView) findViewById(R.id.img_back);

        invoiceGrid = (GridView) findViewById(R.id.invoice_gridview);
    }

    public void myToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

//    @Override
//    public void showInvoice(ParseObject invoice) {
//        final Dialog dialog = new Dialog( this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//
//        dialog.setContentView(R.layout.dialog_image_layout);
//
//        DisplayImageOptions options = new DisplayImageOptions.Builder()
//                .resetViewBeforeLoading(true)
//                .cacheInMemory(true)
//                .cacheOnDisc(true)
//                .build();
//
//        TouchImageView image      = (TouchImageView)  dialog.getWindow().findViewById(R.id.dialog_image);
//        ImageView cancel          = (ImageView)  dialog.getWindow().findViewById(R.id.dialog_image_cancel);
//        TextView reject          = (TextView)  dialog.getWindow().findViewById(R.id.dialog_image_reject);
//
//        ImageLoader.getInstance().displayImage(invoice.getParseFile("photo").getUrl(), image, options);
//
//        if ( invoice.get("comment") != null ) {
//            reject.setText(String.format("%s %s", getString(R.string.comment), invoice.get("comment").toString()));
//        } else  if (invoice.get("rejectComment") != null ) {
//            reject.setText(String.format("%s %s", getString(R.string.reject_comment), invoice.get("rejectComment").toString()));
//        }
//
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
//    }XZWidth = preview.getWidth();
//            int previewSurfaceHeight = preview.getHeight();
//
//            ViewGroup.LayoutParams lp = preview.getLayoutParams();
//
//            lp.width = previewSurfaceWidth;
//            lp.height = previewSurfaceHeight;
//
//            if ("Nexus 5X".contains(getDeviceName())) {
//                camera.setDisplayOrientation(270);
//            } else {
//                camera.setDisplayOrientation(90);
//            }
//
//            preview.setLayoutParams(lp);
//            camera.startPreview();
//        } catch (IOException | RuntimeException e) {
//            e.printStackTrace();
//
//        }
//
//    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)this.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException ignored){}
    }

    @Override
    public void onPause() {
        sendList = new ArrayList<>();
        super.onPause();
    }

    @Override
    public void setSendList(ArrayList<Receipt> sendList) {
        this.sendList = sendList;
        if ( sendList.size() > 0 ) {
            sendButton.setText(getString(R.string._send) + " (" + sendList.size() + ")");
        } else {
            sendButton.setText(getString(R.string._send));
        }
    }

    private void sendFileFirebase(StorageReference storageReference, Receipt receipt) {
        if (storageReference != null) {
            final String name =  "/" + System.currentTimeMillis() + "/image.jpg";

            Chat chat = new Chat();
//                    TODO userModel,"",Calendar.getInstance().getTime().getTime()+"",fileModel
            chat.dataType       = 3;
            chat.time           = System.currentTimeMillis();
            chat.seen           = 0;
            chat.user_id        = AppDelegate.getInstance().getUserId();

            chat.metaData.put("id", String.valueOf(receipt.id));

            mFirebaseRef.push().setValue(chat);
        }
    }
}
