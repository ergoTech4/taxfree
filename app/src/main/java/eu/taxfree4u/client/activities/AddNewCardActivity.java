package eu.taxfree4u.client.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Locale;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.interfaces.ServiceApi;
import eu.taxfree4u.client.model.Card;
import eu.taxfree4u.client.tools.AppDelegate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddNewCardActivity extends Activity {

    private static final String TAG = "AddNewCardActivity";
    private EditText        cardNumber, cardExpire, cardName;
    private LinearLayout    saveCard, delCard;
    private String          tempCardNumber, year, month;
    private ProgressDialog  progressDialog;
    private int             cardId;
    private Card            card;
    private ArrayList<Card> cardArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_add_card);
        initialize();

        cardArrayList = AppDelegate.getInstance().getCards();

        cardId = getIntent().getIntExtra("cardId", -1);

        if ( cardId > 0 ) {
            card = findCardById(cardId);
        } else if ( getIntent().getStringExtra("cardNumber").length() > 0 ) {
            card = new Card();

            card.number = getIntent().getStringExtra("cardNumber");
            card.expire_month = String.format("%02d", getIntent().getIntExtra("expiryMonth", 0));
//            card.expire_year = "20";
            card.expire_year = getIntent().getIntExtra("expiryYear", 0) + "";
        }


        if ( card != null ) {
            cardName.setText(card.name);
            String cardNumberTemp = card.number.substring(0,4) + "-" + card.number.substring(4,8)
                    + "-" + card.number.substring(8,12) + "-" + card.number.substring(12,16);

            cardNumber.setText(cardNumberTemp);
            String year = card.expire_year.substring(2,4);

            cardExpire.setText(String.format("%s/%s", card.expire_month, year));
            delCard.setVisibility(View.VISIBLE);
        }

        if ( cardId < 0 ) {
            delCard.setVisibility(View.GONE);
            saveCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( validateCardNumber() && validateExpireDate() && validateExistCard() ) {
                        addCard();
                    }
                }
            });
        } else {
            saveCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( validateCardNumber() && validateExpireDate() ) {
                        editCard();
                    }
                }
            });
        }

        cardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = cardNumber.getText().toString();
                int textLength = cardNumber.getText().length();

                if(text.endsWith("-"))
                    return;

                if(textLength == 5 || textLength == 10 || textLength == 15) {
                    cardNumber.setText(new StringBuilder(text).insert(text.length()-1, "-").toString());
                    cardNumber.setSelection(cardNumber.getText().length());
                }
            }
        });

        cardExpire.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = cardExpire.getText().toString();
                int textLength = cardExpire.getText().length();

                if(text.endsWith("/"))
                    return;

                if( textLength == 3 ) {
                    cardExpire.setText(new StringBuilder(text).insert(text.length()-1, "/").toString());
                    cardExpire.setSelection(cardExpire.getText().length());
                }
            }
        });

        delCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteCard();
            }
        });

    }

    private boolean validateExistCard() {
        tempCardNumber = cardNumber.getText().toString();
        tempCardNumber = tempCardNumber.replaceAll("-", "");

        for ( int i = 0; i < cardArrayList.size(); i++) {
            if ( cardArrayList.get(i).number.equals(tempCardNumber) ) {
                String number = tempCardNumber.substring(0, 4);

                number += " **** **** ";
                number += tempCardNumber.substring(12, 16);

                myToast(getString(R.string.card_with) + " " + number + " " + getString(R.string.already_exist));
                return false;
            }
        }

        return true;
    }

    private Card findCardById(int cardId) {
        for ( int i = 0; i < cardArrayList.size(); i++) {
            if ( cardArrayList.get(i).id == cardId ) {
                return cardArrayList.get(i);
            }
        }

        return null;
    }

    private boolean validateCardNumber() {
        tempCardNumber = cardNumber.getText().toString();
        tempCardNumber = tempCardNumber.replaceAll("-", "");

        if ( tempCardNumber.length() == 16 ) {
            return true;
        } else {
            myToast(getString(R.string.incorect_card_number));
            return false;
        }
    }

    private boolean validateExpireDate() {
        try {
            String tempCardDate = cardExpire.getText().toString();
            tempCardDate = tempCardDate.replaceAll("/", "");

            month = tempCardDate.substring(0, 2);
            year = tempCardDate.substring(2, 4);

            year = "20" + year;

//            Log.d(TAG, "month: "  + month );
//            Log.d(TAG, "year: "  + year );

            if ( Integer.valueOf(month) <= 12 && Integer.valueOf(year) > 2015 ) {
                return true;
            } else {
                myToast("enter valid date please");
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            myToast("enter valid date please");
            return false;
        }
    }

    private void initialize() {
        cardNumber  = (EditText) findViewById(R.id.add_card_number_field);
        cardExpire  = (EditText) findViewById(R.id.add_card_exripe_field);
        cardName    = (EditText) findViewById(R.id.add_card_name_field);

        saveCard   = (LinearLayout) findViewById(R.id.add_card_save_card);
        delCard    = (LinearLayout) findViewById(R.id.add_card_del_card);
    }

    private void addCard() {
        int isDefault = 0;
        showMyProgressDialog(getString(R.string._loading));

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);
//
//        Log.d(TAG, "request card number : " + tempCardNumber);
//        Log.d(TAG, "request month: " + month);
//        Log.d(TAG, "request year: " + year);

        if ( AppDelegate.getInstance().getCards().size() == 0 ) {
            isDefault = 1;
        }

        Call<JsonObject> call = serviceApi.addCard(Locale.getDefault().getLanguage(), AppDelegate.getInstance().getToken(), cardName.getText().toString(),
                                                    tempCardNumber, month, year, isDefault);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response add card:" + response.body());
                    try {

                        JSONObject status = new JSONObject(response.body().toString());
                        if ( status.optInt("status") == 0 ) {
                            myToast(getString(R.string.card_added));
                            AppDelegate.getInstance().setNeedUpdate(true);
                            finish();
                        }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "response error: " + e.getMessage());
                }
            } else {
                    Log.d(TAG, "response error: " + errorConvector(response));
                    myToast("card invalidate");
            }
                dismissDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
                dismissDialog();
            }
        });


    }

    private void editCard() {
        showMyProgressDialog(getString(R.string._loading));

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.editCard(AppDelegate.getInstance().getToken(), cardId, cardName.getText().toString(),
                tempCardNumber, month, year, card.is_default);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response add card:" + response.body());
                    try {

                        JSONObject status = new JSONObject(response.body().toString());
                        if ( status.optInt("status") == 0 ) {
                            myToast(" card success edited");
                            AppDelegate.getInstance().setNeedUpdate(true);
                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response error: " + errorConvector(response));
                    myToast("card invalidate");
                }
                dismissDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
                dismissDialog();
            }
        });


    }

    private void deleteCard() {
        showMyProgressDialog(getString(R.string._loading));

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.deleteCard(AppDelegate.getInstance().getToken(), cardId);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response add card:" + response.body());
                    try {
                        JSONObject status = new JSONObject(response.body().toString());
                        if ( status.optInt("status") == 0 ) {
                            myToast(getString(R.string.card_deleted));
                            cardArrayList.remove(findCardById(cardId));
                            if ( cardArrayList.size() > 0 ) {
                                card = cardArrayList.get(0);
                                cardId = card.id;
                                tempCardNumber = card.number;

                                month = card.expire_month;
                                year = card.expire_year;
                                card.is_default = true;

                                editCard();
                            } else {
                                AppDelegate.getInstance().setNeedUpdate(true);
                                finish();
                            }

//                            AppDelegate.getInstance().setNeedUpdate(true);
//                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response error: " + errorConvector(response));
                    myToast("card invalidate");
                }
                dismissDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
                dismissDialog();
            }
        });



    }

    private void myToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void showMyProgressDialog(String msg) {
        if ( progressDialog == null || !progressDialog.isShowing() ) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
            try {
                progressDialog.show();
            } catch (Exception e) {
            }
        }
    }

    protected void dismissDialog() {
        if ( progressDialog != null && progressDialog.isShowing() ) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {}
        }
    }

    private String errorConvector(Response<JsonObject> response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
