package eu.taxfree4u.client.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Modifier;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.interfaces.ServiceApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgotPassword extends Activity {

    private static final String TAG = "ForgotPassword";
    private MaterialEditText    emailField;
    private Button              recoverButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);

        initialize();

        if ( getIntent().getStringExtra("email") != null ) {
            emailField.setText(getIntent().getStringExtra("email"));
        }

        recoverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( emailField.getText().toString().length() > 3 ) {
                    recover();
                } else {
                    myToast("please enter valid email");
                }
            }
        });
    }

    private void recover() {
        hideKeyboard();

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.forgotPassword(emailField.getText().toString());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response.body() forgot:" + response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if ( jsonObject.optInt("status") == 0 ) {
                            myToast(getString(R.string.check_your_email));
                            startActivity(new Intent(ForgotPassword.this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                            finish();
                        } else {
                            JSONObject jsonObjectError = new JSONObject(jsonObject.optString("error"));
                            myToast(jsonObjectError.optString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        myToast(jsonObject1.optString("message"));

                        Log.d(TAG, "response error code:" + response.code());
                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    private void initialize() {
        emailField      = (MaterialEditText) findViewById(R.id.forgot_pass_email);

        recoverButton   = (Button) findViewById(R.id.recover_button);
    }

    protected void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)this.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException ignored){}
    }

    private void myToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private String errorConvector(Response<JsonObject> response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
