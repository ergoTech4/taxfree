package eu.taxfree4u.client.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;

import eu.taxfree4u.client.R;

public class FaqActivity extends Activity {

    private static final String TAG = "FaqActivity";

//    private ExpandableListView listView;
//    private ArrayList<ArrayList<String>>    arrayLists = new ArrayList<ArrayList<String>>();
//    private ExpListAdapter                  adapter;
//    private ImageView                       imgBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_faq1);

        WebView webView = (WebView) findViewById(R.id.web_view);

        webView.getSettings().setJavaScriptEnabled(true);

        if (getIntent().getIntExtra("faq", 0) == 0) {
            if (Locale.getDefault().getLanguage().equals("ru") || Locale.getDefault().getLanguage().equals("ua")) {
                webView.loadUrl("file:///android_asset/faq_ru.html");
            } else {
                webView.loadUrl("file:///android_asset/faq.html");
            }
        } else if (getIntent().getIntExtra("faq", 0) == 1) {
            webView.loadUrl("file:///android_asset/tnc.html");
        } else if (getIntent().getIntExtra("faq", 0) == 2) {
            webView.loadUrl("https://taxfree4u.eu/page/aboutcard");
        } else if (getIntent().getIntExtra("faq", 0) == 3) {
            webView.loadUrl("https://card.taxfree4u.eu/card/fullv2?mobile=1");
        }


//
//        initialize();
//
//        makeFaqListFromJson();
//
//
//        adapter = new ExpListAdapter(this, arrayLists);
//
//        int width = getResources().getDisplayMetrics().widthPixels;
//
//        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
//            listView.setIndicatorBounds(width - getPixelValue(40), width - getPixelValue(10));
//        } else {
//            listView.setIndicatorBoundsRelative(width - getPixelValue(40), width - getPixelValue(10));
//        }
//
//        listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//            int previousItem = -1;
//
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                if( groupPosition != previousItem ) {
//                    listView.collapseGroup(previousItem);
//                }
//                previousItem = groupPosition;
//            }
//        });
//
//        listView.setAdapter(adapter);

//        imgBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
    }

//    private void makeFaqListFromJson() {
//
//        try {
//            JSONObject jsonObject = new JSONObject(loadJSONFromAsset());
//
//            JSONArray jsonArray = jsonObject.getJSONArray("questions");
//
//            int size = jsonArray.length();
//
//            for ( int i = 0; i < size; i++ ) {
//                ArrayList<String> children1 = new ArrayList<String>();
//               // ArrayList<String> children2 = new ArrayList<String>();
//
//                JSONObject object = jsonArray.getJSONObject(i);
//
//                children1.add(object.getString("title"));
//                children1.add(object.getString("description"));
//
//                arrayLists.add(children1);
//
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public String loadJSONFromAsset() {
//        String json = null;
//        try {
//            InputStream is;
//
//            String local = Locale.getDefault().getLanguage();
//
//            if (local.contains("es")) {
//                is = this.getAssets().open("faq_es.json");
//            } else if (local.contains("ru") || local.contains("uk")) {
//                is = this.getAssets().open("faq_ru.json");
//            } else if (local.contains("ar") ) {
//                is = this.getAssets().open("faq_ar.json");
//            } else {
//                is = this.getAssets().open("faq.json");
//            }
//
//            int size = is.available();
//            byte[] buffer = new byte[size];
//            is.read(buffer);
//            is.close();
//            json = new String(buffer, "UTF-8");
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            return null;
//        }
//        return json;
//    }
//
//    public int getPixelValue(int dp) {
//
//        float scale = getResources().getDisplayMetrics().density;
//        return (int) (dp * scale + 0.5f);
//    }
//
//    private void initialize() {
//        listView    = (ExpandableListView) findViewById(R.id.faq_list_view);
//        imgBack     = (ImageView)   findViewById(R.id.img_back);
//    }
//
//    private class ExpListAdapter extends BaseExpandableListAdapter {
//        private ArrayList<ArrayList<String>> mGroups;
//        private Context mContext;
//
//        public ExpListAdapter (Context context, ArrayList<ArrayList<String>> groups){
//            mContext    = context;
//            mGroups     = groups;
//        }
//
//        @Override
//        public int getGroupCount() {
//            return mGroups.size();
//        }
//
//        @Override
//        public int getChildrenCount(int groupPosition) {
//            return 1;
//        }
//
//        @Override
//        public Object getGroup(int groupPosition) {
//            return mGroups.get(groupPosition);
//        }
//
//        @Override
//        public Object getChild(int groupPosition, int childPosition) {
//            return 0;
//        }
//
//        @Override
//        public long getGroupId(int groupPosition) {
//            return groupPosition;
//        }
//
//        @Override
//        public long getChildId(int groupPosition, int childPosition) {
//            return childPosition;
//        }
//
//        @Override
//        public boolean hasStableIds() {
//            return true;
//        }
//
//        @Override
//        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
//
//            if (convertView == null) {
//                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                convertView = inflater.inflate(R.layout.faq_main_view, null);
//            }
//
//            TextView textGroup = (TextView) convertView.findViewById(R.id.group_item_tv);
//            textGroup.setText(mGroups.get(groupPosition).get(0));
//
//            return convertView;
//        }
//
//        @Override
//        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
//
//            if (convertView == null) {
//                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                convertView = inflater.inflate(R.layout.faq_child_view, null);
//            }
//
//            TextView textChild = (TextView) convertView.findViewById(R.id.group_item_tv);
//
//            textChild.setText(mGroups.get(groupPosition).get(1));
//
//
//            return convertView;
//        }
//
//        @Override
//        public boolean isChildSelectable(int groupPosition, int childPosition) {
//            return true;
//        }
//    }


}
