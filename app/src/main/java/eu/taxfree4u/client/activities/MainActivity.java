package eu.taxfree4u.client.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.createTrip.CameraBasicFragment;
import eu.taxfree4u.client.createTrip.CameraPassportFragment;
import eu.taxfree4u.client.fragments.CardsFragment;
import eu.taxfree4u.client.fragments.ChatFragment;
import eu.taxfree4u.client.fragments.CountriesChoicerFragment;
import eu.taxfree4u.client.fragments.DetailsFragment;
import eu.taxfree4u.client.fragments.NewTripFragment;
import eu.taxfree4u.client.fragments.ProfileFragment;
import eu.taxfree4u.client.fragments.ReceiptsFragment;
import eu.taxfree4u.client.fragments.VatFormsFragment;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.ChatFragmentInterface;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.interfaces.ServiceApi;
import eu.taxfree4u.client.model.Card;
import eu.taxfree4u.client.model.Chat;
import eu.taxfree4u.client.model.Country;
import eu.taxfree4u.client.model.Receipt;
import eu.taxfree4u.client.model.TripObj;
import eu.taxfree4u.client.model.User;
import eu.taxfree4u.client.model.VatForm;
import eu.taxfree4u.client.services.ConstBC;
import eu.taxfree4u.client.services.ConstEX;
import eu.taxfree4u.client.services.ServiceDownload;
import eu.taxfree4u.client.tools.AppDelegate;
import io.card.payment.CardIOActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements AppInterface {

    private static final String TAG = "MainActivity";
    private static final int MY_SCAN_REQUEST_CODE = 16;

    private Fragment            homeFragment, detailsFragment, vatFormsFragment, cameraFragment, currentFragment, chatFrg;
    private ProfileFragment     profileFragment;
    private ArrayList<Country>  countries = new ArrayList<>();
    private ArrayList<Receipt>  receipts = new ArrayList<>();
    private ArrayList<VatForm>  vatForms = new ArrayList<>();
    private ArrayList<Card>     cards = new ArrayList<>();
    private ProgressDialog      progressDialog;
    private TripObj             currentTrip;
    private User                currentUser;
    private AHBottomNavigation  bottomNavigation;
    private byte[]              receiptPhoto;
    private boolean             isOpened = false;
    private int                 loadCheck = -1;
    private boolean             bytesOfPassport;
    private boolean             doubleBackToExitPressedOnce = false;
    private Stack<Fragment>     fragmentStack  = new Stack<>();
    private DatabaseReference   mFirebaseRef;
    private FirebaseAuth        mAuth;
    private String              userId;

    int viewToOpen          =  -1;
    int idToOpen            =  -1;
    int subviewToOpen       =  -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        receiver = new MyReceiver();

        userId = AppDelegate.getInstance().getUserId();
        mAuth = FirebaseAuth.getInstance();

        getToken();

        loadCountries();

        setContentView(R.layout.activity_main);

        setListenerToRootView();

        profileFragment = ProfileFragment.newInstance();
        detailsFragment = DetailsFragment.newInstance();
        vatFormsFragment = VatFormsFragment.newInstance();
        cameraFragment = CameraBasicFragment.newInstance();
        chatFrg         = ChatFragment.newInstance();
        currentFragment = detailsFragment;

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

        AHBottomNavigationItem item0 = new AHBottomNavigationItem(R.string.trips, R.drawable.icon_trips_normal, R.color.gray);
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.cards, R.drawable.icon_cards, R.color.gray);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.chat, R.drawable.icon_chat, R.color.gray);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.profile, R.drawable.profile1, R.color.gray);
//        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.history, R.drawable.icon_history, R.color.gray);

        bottomNavigation.addItem(item0);
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
//        bottomNavigation.addItem(item4);

        bottomNavigation.setAccentColor(getResources().getColor(R.color.orange));
        bottomNavigation.setInactiveColor(getResources().getColor(R.color.gray));

//        bottomNavigation.setForceTint(true);

        bottomNavigation.setNotificationBackgroundColorResource(R.color.red);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position, boolean wasSelected) {
                switch (position) {
                    case 0:
                        if (homeFragment == null) {
                            homeFragment = new ReceiptsFragment();
                        }

                        if (fragmentStack.size() == 0 ) {
                            getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.rv_container, homeFragment, homeFragment.getTag())
                                    .commit();

                            fragmentStack.push(homeFragment);
                        } else {
                            getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.rv_container, homeFragment, homeFragment.getTag())
                                    .commit();
                        }

                        bottomNavigation.setNotification(0, 0);

                        break;
                    case 1:
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.rv_container, CardsFragment.newInstance(), CardsFragment.TAG)
                                .commit();
                        break;
                    case 2:
                        AppDelegate.getInstance().setNotificationCount(0);
                        bottomNavigation.setNotification(0, 2);
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.rv_container, chatFrg, CardsFragment.TAG)
                                .commit();

//                        Support.showConversation(MainActivity.this);
                        break;
                    case 3:
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.rv_container, profileFragment, ProfileFragment.TAG)
                                .commit();
                        currentFragment = profileFragment;
                        break;
//                    case 4:
//                        previosMenu = 4;
//                        getSupportFragmentManager()
//                                .beginTransaction()
//                                .replace(R.id.rv_container, profileFragment, ProfileFragment.TAG)
//                                .commit();
//                        currentFragment = profileFragment;
//                        break;
                }
            }
        });

        downloadData(ConstBC.BC_EVENT_REFRESH_TOKEN);

        if ( AppDelegate.getInstance().isFirstLogin() == 1 ) {
            changePasswordDialog(false);
        }

//
//        SpaceNavigationView spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
//        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
//        spaceNavigationView.addSpaceItem(new SpaceItem(getString(R.string.trips), R.drawable.icon_trips_normal));
//        spaceNavigationView.addSpaceItem(new SpaceItem(getString(R.string.cards), R.drawable.icon_cards));
//        spaceNavigationView.addSpaceItem(new SpaceItem(getString(R.string.profile), R.drawable.profile1));
//        spaceNavigationView.addSpaceItem(new SpaceItem(getString(R.string.more), R.drawable.icon_cards));
//
//        spaceNavigationView.setCentreButtonIcon(R.drawable.icon_chat);
//        spaceNavigationView.setCentreButtonColor(ContextCompat.getColor(this, R.color.white));
//
//        spaceNavigationView.showIconOnly();
//        spaceNavigationView.setSpaceBackgroundColor(ContextCompat.getColor(this, R.color.gray_text));
//
//
//        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
//            @Override
//            public void onCentreButtonClick() {
//                Toast.makeText(MainActivity.this,"onCentreButtonClick", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onItemClick(int itemIndex, String itemName) {
//                switch (itemIndex) {
//                    case 0:
//                        if ( homeFragment == null ) {
//                            homeFragment =  new ReceiptsFragment();
//
//                        }
//                        getSupportFragmentManager()
//                                .beginTransaction()
//                                .replace(R.id.rv_container, homeFragment, ReceiptsFragment.TAG)
//                                .commit();
//                        break;
//                    case 1:
//
//                        getSupportFragmentManager()
//                                .beginTransaction()
//                                .replace(R.id.rv_container, CardsFragment.newInstance(), CardsFragment.TAG)
//                                .commit();
//
//                        break;
//                    case 2:
//                        getSupportFragmentManager()
//                                .beginTransaction()
//                                .replace(R.id.rv_container, profileFragment, ProfileFragment.TAG)
//                                .commit();
//                        currentFragment = profileFragment;
//                        break;
//
//                }
//            }
//
//            @Override
//            public void onItemReselected(int itemIndex, String itemName) {
//                Toast.makeText(MainActivity.this, itemIndex + " " + itemName, Toast.LENGTH_SHORT).show();
//            }
//        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    private void downloadData(int event) {
        Intent intent = new Intent(this, ServiceDownload.class);

        intent.putExtra(ConstEX.EX_EVENT, event);

        this.startService(intent);
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException ignored) {
        }
    }

    @Override
    protected void onPause() {
        if ( AppDelegate.getInstance().isFirstLogin() == 1 ) {
            AppDelegate.getInstance().setToken("");
        }

        AppDelegate.getInstance().setActivity(0);

        viewToOpen      = -1;
        subviewToOpen   = -1;
        idToOpen        = -1;

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppDelegate.getInstance().setActivity(1);

        hideKeyboard();

        viewToOpen      =  getIntent().getIntExtra("view", -1);
        subviewToOpen   =  getIntent().getIntExtra("subview", -1);
        idToOpen        = getIntent().getIntExtra("id", -1);

        loadCurrentTrip();
        getUser();


        if ( cards.size() == 0 ) {
            loadCards();
        }

        if ( viewToOpen == -1 && AppDelegate.getInstance().getCurrentTripId() == -1) {
            homeFragment = NewTripFragment.newInstance();

            if (getSupportFragmentManager().findFragmentById(R.id.rv_container) == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.rv_container, homeFragment, ReceiptsFragment.TAG)
                        .commit();
            }
        }

        switch (viewToOpen) {
            case 0:
                bottomNavigation.setCurrentItem(0);
                loadCurrentTripReceipts();

                 if (subviewToOpen == 1) {
                     callDetails();
                     // TODO open dialog with info
                     viewToOpen = -1;
                     subviewToOpen = -1;
                } else if (subviewToOpen == 2) {
                    loadCurrentTripVatForms();
                }
                break;
            case 1:
                bottomNavigation.setCurrentItem(1);
                viewToOpen = -1;
                break;
            case 2:
                bottomNavigation.setCurrentItem(2);
                break;
        }

//        if ( bottomNavigation.getCurrentItem() == 2 ) {
//            bottomNavigation.setCurrentItem(previosMenu);
//        }

        bottomNavigation.setNotification(AppDelegate.getInstance().getNotificationCounter(), 2);

        Log.d(TAG, "token: " + AppDelegate.getInstance().getToken());
    }

    @Override
    public void logout() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .disableHtmlEscaping()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.logout(AppDelegate.getInstance().getToken());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                Log.d(TAG, "response logout" );
                if (response.body() == null) {
                    Log.d(TAG, "response countries try 3" );
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        myToast(jsonObject1.optString("message"));
                        Log.d(TAG, "1response error code:" + response.code());
                        Log.d(TAG, "1response error:" + jsonObject1.optString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                } else {
                    Log.d(TAG, "response logout: " +  response.body());

                    AppDelegate.getInstance().logout();
                    startActivity(new Intent(MainActivity.this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                    finish();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response countries error: " + t.getMessage());
            }
        });


    }

    @Override
    public ArrayList<Receipt> getCurrentReceipts() {
        if (receipts == null) {
//            loadCurrentTripReceipts();
            return new ArrayList<>();
        } else {
            return receipts;
        }
    }

    @Override
    public ArrayList<VatForm> getCurrentVatForms() {
        return vatForms;
    }

    @Override
    public void callDetails() {
        fragmentStack.push(detailsFragment);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rv_container, fragmentStack.lastElement(), DetailsFragment.TAG)
                .commit();
        currentFragment = detailsFragment;
    }

    @Override
    public void callVatFrom() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rv_container, vatFormsFragment, ReceiptsFragment.TAG)
                .commit();
    }

    @Override
    public void callReceipts() {
        if (homeFragment == null && currentTrip == null) {
            homeFragment = NewTripFragment.newInstance();
        } else {
            homeFragment = ReceiptsFragment.newInstance();
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rv_container, homeFragment, ReceiptsFragment.TAG)
                .commit();
    }

    private String errorConvector(Response<JsonObject> response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    public void loadCountries() {
        Log.d(TAG, "response countries try" );

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .disableHtmlEscaping()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getCountries(Locale.getDefault().getLanguage(),
                AppDelegate.getInstance().getToken(), 0);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.d(TAG, "response countries try 2" );
                if (response.body() != null) {
                    Log.d(TAG, "response countries:" + response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        if (jsonObject.optInt("status") == 0) {

//                            JSONArray jsonData = new JSONArray(jsonObject.get("data"));
                            countryParser(jsonObject.getJSONArray("data"));
                            AppDelegate.getInstance().saveCountries(jsonObject.getJSONArray("data").toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    transportParser(response.body().getAsJsonArray("transport"));
//                    currencyParser(response.body().getAsJsonArray("currency"));
                } else {
                    Log.d(TAG, "response countries try 3" );
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        myToast(jsonObject1.optString("message"));

                        Log.d(TAG, "1response error code:" + response.code());
                        Log.d(TAG, "1response error:" + jsonObject1.optString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response countries error: " + t.getMessage());
            }
        });
    }

    private void countryParser(JSONArray jsonCountries) {
        int size = jsonCountries.length();

        for (int i = 0; i < size; i++) {
            Country country = new Country();
            try {
                JSONObject object = new JSONObject(jsonCountries.get(i).toString());

                country.id          = object.optInt("id");
                country.name        = object.optString("name");
                country.iso_name    = object.optString("iso_name");
                country.phone_code  = object.optString("phone_code");
                country.is_europe   = object.optBoolean("is_europe");

//                Log.d(TAG, "country: " + country.name + " , is_europe: " +  country.is_europe);

                countries.add(country);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseTrip(JSONObject jsonObject) {
        currentTrip = new TripObj();

        currentTrip.id = jsonObject.optInt("id");
        currentTrip.date_start = jsonObject.optLong("date_start");
        currentTrip.date_end = jsonObject.optLong("date_end");
        currentTrip.destination_country = jsonObject.optInt("destination_country");
        currentTrip.departure_country = jsonObject.optInt("departure_country");
        currentTrip.transport_leaving = jsonObject.optInt("transport_leaving");
        currentTrip.race_number_enter = jsonObject.optString("race_number_enter");
        currentTrip.race_number_leaving = jsonObject.optString("race_number_leaving");
        currentTrip.address = jsonObject.optString("address");
        currentTrip.flightNumber = jsonObject.optString("flight_number");
        currentTrip.city = jsonObject.optString("city");
        currentTrip.paymentCard = jsonObject.optString("payment_card");


//        AppDelegate.getInstance().setTotalRefundVAT(jsonObject.optString("client_refund"));
        AppDelegate.getInstance().setCurrentTrip(currentTrip);
        AppDelegate.getInstance().setCurrentTripId(currentTrip.id);
    }

    protected void showMyProgressDialog(String msg) {
        if (progressDialog == null || !progressDialog.isShowing()) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
            try {
                progressDialog.show();
            } catch (Exception e) {
            }
        }
    }

    protected void dismissDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
            }
        }
    }

    private void myToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void loadCheckPhoto() {
        if (loadCheck == 0) {
//            if (cameraFragment.isResumed()) {
//                callReceipts();
//                Log.d(TAG, "DO WORK 1");
//            }

            File receipt_file = new File(MainActivity.this.getExternalFilesDir(null), "checkPhoto.jpg");

            try {
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(receipt_file));
                bos.write(receiptPhoto);

                bos.flush();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), receipt_file);

            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("receipt_file", "receipt_file", requestFile);

            Gson gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                    .create();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(200, TimeUnit.SECONDS)
                    .readTimeout(25, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getString(R.string.base_url))
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ServiceApi serviceApi = retrofit.create(ServiceApi.class);

            Call<JsonObject> call = serviceApi.uploadCheckPhoto(AppDelegate.getInstance().getToken(), body);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.body() != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());

                            if (jsonObject.optInt("status") == 0) {
                                myToast(getString(R.string.check_add_success));

                                receiptPhoto = null;
                                loadCheck = -1;

                                if (homeFragment instanceof ReceiptsFragment && homeFragment.isResumed()) {
                                    Log.d(TAG, "DO WORK 2");
                                    loadCurrentTripReceipts();
                                }

                                Log.d(TAG, "DO WORK 3");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(errorConvector(response));
                            JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                            myToast(jsonObject1.optString("message"));

                            Log.d(TAG, "response error code:" + response.code());
                            Log.d(TAG, "response error:" + jsonObject1.optString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "response error:" + errorConvector(response));
                        }
                    }
                    loadCheck = -1;
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d(TAG, "response error: " + t.getMessage());
                }
            });
        } else {
            loadCheck = 0;
        }
    }

    @Override
    public void addReceipt() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rv_container, cameraFragment, CameraBasicFragment.TAG)
                .commit();
        fragmentStack.push(cameraFragment);
    }

    @Override
    public boolean isCheckPhoto() {
        return receiptPhoto != null;
    }

    @Override
    public void savePhotoCheck(final Bitmap image) {
        if ( image != null ) {
            new Thread(new Runnable() {
                public void run() {

                    Bitmap imageTemp = scaleBmp(image);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();

                    if ( imageTemp.getHeight() < imageTemp.getWidth() ) {
                        imageTemp = rotateImage(90, imageTemp);
                    }

                    imageTemp.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                    receiptPhoto = stream.toByteArray();

                    try {
                        File mFile = new File(MainActivity.this.getExternalFilesDir(null), "checkPhoto.jpg");

                        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(mFile));
                        bos.write(stream.toByteArray());
                        bos.flush();
                        bos.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if( loadCheck > 0) {
                        updateCheckPhoto(loadCheck);
                    } else {
                        loadCheckPhoto();
                    }
                }
            }).start();
        } else {
            loadCheck = -1;
        }
    }

    public Bitmap rotateImage(int angle, Bitmap bitmapSrc) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        return Bitmap.createBitmap(bitmapSrc, 0, 0,
                bitmapSrc.getWidth(), bitmapSrc.getHeight(), matrix, true);

    }

    private Bitmap scaleBmp(Bitmap bmp) {
        final int maxSize = 1500;
        int outWidth;
        int outHeight;
        int inWidth = bmp.getWidth();
        int inHeight = bmp.getHeight();

        if (inWidth < inHeight) {
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }

        return Bitmap.createScaledBitmap(bmp, outWidth, outHeight, false);
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

//        String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
//                phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
//            phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }

    @Override
    public void hideBottomMenu(boolean hide) {
        if (hide) {
            bottomNavigation.hideBottomNavigation();
        } else {
            bottomNavigation.restoreBottomNavigation();
        }
    }

    @Override
    public void loadCurrentTrip() {
        showMyProgressDialog("loading...");

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getCurrentTrip(AppDelegate.getInstance().getToken());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "loadCurrentTrip response.body(): " + response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if (jsonObject.optInt("status") == 0) {
                            parseTrip(new JSONObject(jsonObject.optString("data")));
                            getTotalRefundReceipt();
                            loadCurrentTripReceipts();
                            loadCurrentTripVatForms();
                            loadTotalRefundVAT();

                            if (!(homeFragment instanceof ReceiptsFragment)) {
                                homeFragment = new ReceiptsFragment();

                                if ( viewToOpen < 0 ) {
                                    getSupportFragmentManager()
                                            .beginTransaction()
                                            .replace(R.id.rv_container, homeFragment, ReceiptsFragment.TAG)
                                            .commit();
//
//                                    fragmentStack = new Stack<>();
//
//                                    fragmentStack.push(homeFragment);
                                }
                            }
                        } else if ( jsonObject.optInt("status") == 1 ) {
                            JSONObject error = new JSONObject(jsonObject.optString("error"));

                            if (error.optInt("code") == 10 ) {
                                if ( !(homeFragment instanceof NewTripFragment) ) {
                                    AppDelegate.getInstance().setCurrentTripId(-1);
                                    homeFragment = new NewTripFragment();
                                    if ( viewToOpen < 0 ) {
                                        bottomNavigation.setCurrentItem(0);
                                    }
                                }
                            } else {
                                startActivity(new Intent(MainActivity.this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                MainActivity.this.finish();
                            }
                        }
                    } catch (JSONException | IllegalStateException e) {
                        e.printStackTrace();
                    }

                    viewToOpen = -1;
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        if (jsonObject1.optString("message").length() > 2) {
                            myToast(jsonObject1.optString("message"));
                        }
                        Log.d(TAG, "response error code:" + response.code());
                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));

                        if (response.code() == 401) {
                            startActivity(new Intent(MainActivity.this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                            MainActivity.this.finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
                dismissDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
                if (getSupportFragmentManager().findFragmentById(R.id.rv_container) == null) {

                    if (homeFragment == null) {
                        homeFragment = NewTripFragment.newInstance();
                    }

                    try {
                        getSupportFragmentManager()
                                .beginTransaction()
                                .add(R.id.rv_container, homeFragment, homeFragment.getTag())
                                .commit();

                    } catch (Exception w) {
                    }
                }


                dismissDialog();
            }
        });
    }

    public void loadTotalRefundVAT() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getTotalRefund(AppDelegate.getInstance().getToken());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response.body() total refund: " + response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if (jsonObject.optInt("status") == 0) {
                            JSONObject data = new JSONObject(jsonObject.optString("data"));
                            AppDelegate.getInstance().setTotalRefundVAT(data.optString("total_refund"));
                        }

                    } catch (JSONException | IllegalStateException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        if (jsonObject1.optString("message").length() > 2 && jsonObject1.optString("message").equals("Invalid token")) {
                            myToast("Your session has expired, please login again.");
                        }
                        Log.d(TAG, "response error code:" + response.code());
                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
                dismissDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());

                dismissDialog();
            }
        });
    }

    private void loadCurrentTripReceipts() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getTripReceipts(AppDelegate.getInstance().getToken(),
                AppDelegate.getInstance().getCurrentTripId());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
//                    Log.d(TAG, "response.body() receipts : " + response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if (jsonObject.optInt("status") == 0) {
                            if (jsonObject.optJSONArray("data") != null && jsonObject.optJSONArray("data").length() > 0) {
                                parseReceipts(jsonObject.optJSONArray("data"));
                            } else {
                                receipts = new ArrayList<Receipt>();
                                AppDelegate.getInstance().setReceipsList(receipts);
                            }
                        }
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        if (jsonObject1.optString("message").length() > 2) {
                            myToast(jsonObject1.optString("message"));
                        }
                        Log.d(TAG, "response error code:" + response.code());
                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
                dismissDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
                if (getSupportFragmentManager().findFragmentById(R.id.rv_container) == null) {

                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.rv_container, homeFragment, ReceiptsFragment.TAG)
                            .commit();
                }

                dismissDialog();
            }
        });
    }

    @Override
    public void loadCurrentTripVatForms() {
//        showMyProgressDialog("loading...");

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getTripVatForms(AppDelegate.getInstance().getToken(),
                AppDelegate.getInstance().getCurrentTripId());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response.body() vat : " + response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if (jsonObject.optInt("status") == 0) {
                            if (jsonObject.optJSONArray("data") != null && jsonObject.optJSONArray("data").length() > 0) {
                                parseVatForms(jsonObject.optJSONArray("data"));
                            } else {
                                vatForms = new ArrayList<VatForm>();
                            }
                        }
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        if (jsonObject1.optString("message").length() > 2) {
                            myToast(jsonObject1.optString("message"));
                        }
                        Log.d(TAG, "response error code:" + response.code());
                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
                dismissDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
                if (getSupportFragmentManager().findFragmentById(R.id.rv_container) == null) {

                    getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.rv_container, homeFragment, ReceiptsFragment.TAG)
                            .commit();
                }

                dismissDialog();
            }
        });
    }

    private void parseReceipts(JSONArray data) {
        int size = data.length();
        receipts = new ArrayList<>();
        int notificationCounter = 0;

        for (int i = 0; i < size; i++) {
            Receipt receipt = new Receipt();
            try {
                JSONObject jsonObject = new JSONObject(data.get(i).toString());

//                Log.d(TAG, "receipts: " + jsonObject.toString() );

                receipt.id = jsonObject.optInt("id");
                receipt.editable = jsonObject.optBoolean("editable");
                receipt.create_time = jsonObject.optLong("create_time");
                receipt.file = jsonObject.optString("receipt_file");
                receipt.file_thumb = jsonObject.optString("receipt_file_thumb");
                receipt.client_refund = jsonObject.optString("client_refund");
                receipt.comment = jsonObject.optString("comment");

                if ( jsonObject.optString("currency") != null ) {
                    receipt.currency = jsonObject.optInt("currency");
                }

                if ( receipt.comment.length() > 5 ) {
                    notificationCounter += 1;
                }

                receipt.status = jsonObject.optString("status");

                if ( receipt.status.equals("client-response") || receipt.status.equals("updated")) {
                    receipt.status = "new";
                }

                if ( notificationCounter > 0 ) {
                    setNotifications(notificationCounter, 0);
                }

                receipts.add(receipt);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (homeFragment instanceof ReceiptsFragment && homeFragment.isResumed()) {
            ((ReceiptsFragment) homeFragment).doWork();
        }

        if ( subviewToOpen == 0 && homeFragment instanceof ReceiptsFragment ) {
            ((ReceiptsFragment) homeFragment).showImage(idToOpen);
            viewToOpen      = -1;
            subviewToOpen   = -1;
            idToOpen        = -1;
        }

        AppDelegate.getInstance().setReceipsList(receipts);
    }

    private void parseVatForms(JSONArray data) {
        int size = data.length();
        vatForms = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            VatForm vatForm = new VatForm();
            try {
                JSONObject jsonObject = new JSONObject(data.get(i).toString());

                vatForm.id              = jsonObject.optInt("id");
                vatForm.client_refund   = jsonObject.optString("client_refund");
                vatForm.file            = jsonObject.optString("file");
                vatForm.updated         = jsonObject.optLong("updated");
                vatForm.file_thumb      = jsonObject.optString("file_thumb");
                vatForm.status          = jsonObject.optString("status");
                vatForm.error           = jsonObject.optInt("error");
                vatForm.comment         = jsonObject.optString("comment");

                vatForms.add(vatForm);
                downloadPDF(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (vatFormsFragment.isResumed()) {
            ((FragmentInterface)vatFormsFragment).doWork();
        }

        if ( subviewToOpen == 2 ) {
            callVatFrom();
            subviewToOpen   = -1;
            viewToOpen      = -1;
            idToOpen        = -1;
        }


//        if ( homeFragment instanceof ReceiptsFragment) {
//            ((ReceiptsFragment) homeFragment).doWork();
//        }
    }

    private void parseCards(JSONArray data) {
        int size = data.length();
        cards = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            Card card = new Card();
            try {
                JSONObject jsonObject = new JSONObject(data.get(i).toString());

                card.id = jsonObject.optInt("id");
                card.expire_month = jsonObject.optString("expire_month");
                card.expire_year = jsonObject.optString("expire_year");
                card.is_default = jsonObject.optBoolean("is_default");
                card.number = jsonObject.optString("number");
                card.name = jsonObject.optString("name");

                cards.add(card);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        AppDelegate.getInstance().setCards(cards);

        if ( bottomNavigation.getCurrentItem() == 1 && AppDelegate.getInstance().isNeedUpdate() ) {
            AppDelegate.getInstance().setNeedUpdate(false);
            bottomNavigation.setCurrentItem(1);
        }
    }

    @Override
    public void callCountyFragment(String tag) {
        CountriesChoicerFragment fragment = CountriesChoicerFragment.newInstance(tag);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rv_container, fragment, CountriesChoicerFragment.TAG)
                .commit();
        fragmentStack.push(fragment);
    }

    @Override
    public void callPreviousFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rv_container, currentFragment, currentFragment.getTag())
                .commit();

    }

    @Override
    public void editTrip() {
        showMyProgressDialog("uploading data...");

        TripObj tripObj = AppDelegate.getInstance().getCurrentTrip();

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

//        Log.d(TAG, "tripObj.date_start: " + tripObj.date_start );

        Call<JsonObject> call = serviceApi.editTrip(Locale.getDefault().getLanguage(), AppDelegate.getInstance().getToken(), tripObj.date_start, tripObj.date_end,
                tripObj.departure_country, tripObj.destination_country, tripObj.flightNumber, tripObj.city);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response.body(): " + response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if (jsonObject.optInt("status") == 0) {
                            myToast(getString(R.string.edit_trip_success));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        myToast(jsonObject1.optString("message"));

                        Log.d(TAG, "response error code:" + response.code());
                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
                dismissDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());

                dismissDialog();
            }
        });

    }

    public void setListenerToRootView() {
        final View activityRootView = getWindow().getDecorView().findViewById(android.R.id.content);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > activityRootView.getHeight() / 3) {

                    if (!isOpened) {
//                        Toast.makeText(getApplicationContext(), "Gotcha!!! softKeyboardup", Toast.LENGTH_SHORT).show();
                        bottomNavigation.hideBottomNavigation(true);
                        if (chatFrg.isResumed()) {
                            Log.d(TAG, "hide menu1");
                            ((ChatFragmentInterface)chatFrg).hideBar(true);
                        }
                    }
                    isOpened = true;
                } else if (isOpened) {
//                    Toast.makeText(getApplicationContext(), "softkeyborad Down!!!", Toast.LENGTH_SHORT).show();
                    isOpened = false;
                    bottomNavigation.restoreBottomNavigation(true);

                    if (detailsFragment.isResumed()) {
                        ((DetailsFragment) detailsFragment).saveChanges();
                    } else if (profileFragment.isResumed()) {
                        profileFragment.saveChanges();
                    }

                    Log.d(TAG, "hide menu2");
                    if (chatFrg.isResumed()) {
                        ((ChatFragmentInterface)chatFrg).hideBar(false);
                    }
                }
            }
        });

    }

    private void getUser() {
        currentUser = new User();

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getUser(AppDelegate.getInstance().getToken());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response profile view:" + response.body());
                    try {

                        JSONObject status = new JSONObject(response.body().toString());
                        if (status.optInt("status") == 0) {

                            JSONObject jsonObject = new JSONObject(status.optString("data"));

                            currentUser.id = jsonObject.optString("identity");
                            AppDelegate.getInstance().setUserId(currentUser.id);

                            if ( jsonObject.optString("phone") != null) {
                                currentUser.phone = jsonObject.optString("phone");
                            }

                            if ( jsonObject.optString("identity") != null) {
                                currentUser.identity = jsonObject.optString("identity");
                            }

                            if (jsonObject.optString("phone_country") != null ) {
                                currentUser.phone_country = jsonObject.optLong("phone_country");
                            }
                            currentUser.country = jsonObject.optInt("country");
                            if (!jsonObject.optString("full_name").equals("null")) {
                                currentUser.full_name = jsonObject.optString("full_name");
                            }

                            currentUser.passport_file = jsonObject.optString("passport_file");

                            currentUser.email = jsonObject.optString("email");

                            if (!jsonObject.optString("passport_sex").equals("null")) {
                                currentUser.passport_sex = jsonObject.optString("passport_sex");
                            }

                            if (!jsonObject.optString("passport_number").equals("null")) {
                                currentUser.passport_number = jsonObject.optString("passport_number");

                            }
                            if (!jsonObject.optString("city").equals("null")) {
                                currentUser.city = jsonObject.optString("city");
                            }

                            if (!jsonObject.optString("address").equals("null")) {
                                currentUser.address = jsonObject.optString("address");
                            }

                            currentUser.passport_date = jsonObject.optLong("passport_date");
                            currentUser.passport_country = jsonObject.optInt("passport_country");
                            currentUser.passport_citizenship = jsonObject.optInt("passport_citizenship");

                            currentUser.passport_birthday = jsonObject.optLong("passport_birthday");

                            if ( !jsonObject.optString("post_code").equals("null") ) {
                                currentUser.post_code = jsonObject.optString("post_code");
                            }

                            if ( jsonObject.optString("status").equals("request")) {
                                currentUser.errorMessage = jsonObject.optString("reason");
                                bottomNavigation.setNotification(1, 3);
                            }

                            AppDelegate.getInstance().saveUser(currentUser);

                            if (profileFragment.isResumed()) {
                                profileFragment.doWork();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response body null: " + errorConvector(response));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    @Override
    public void saveUser() {
        currentUser = AppDelegate.getInstance().getUser();

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.updateUser(AppDelegate.getInstance().getToken(), prepareUserParams());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response profile view:" + response.body());
                    try {

                        JSONObject status = new JSONObject(response.body().toString());
                        if (status.optInt("status") == 0) {
                            myToast(getString(R.string.edit_user_success));
                        } else {
                            JSONObject error = new JSONObject(status.optString("error"));
                            myToast(error.optString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response body null: " + errorConvector(response));
                    myToast(errorConvector(response));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    private Map<String, String> prepareUserParams() {
        currentUser = AppDelegate.getInstance().getUser();
        Map<String, String> userParams = new HashMap<>();

        if ( currentUser.phone.length() >= 7) {
            userParams.put("phone", currentUser.phone.replace(" ", "").replace("+", ""));
        }
        if ( currentUser.full_name.length() >= 2) {
            userParams.put("full_name", currentUser.full_name);
        }
        if ( currentUser.email.length() >= 3) {
            userParams.put("email", currentUser.email);
        }
        if ( currentUser.country > 0) {
            userParams.put("country", String.valueOf(currentUser.country));
        }
        if ( currentUser.city.length() >= 2) {
            userParams.put("city", currentUser.city);
        }
        if ( currentUser.address.length() >= 3) {
            userParams.put("address", currentUser.address);
        }
        if ( currentUser.post_code.length() > 0) {
            userParams.put("post_code", String.valueOf(currentUser.post_code));
        }
        if ( currentUser.passport_country > 0) {
            userParams.put("passport_country", String.valueOf(currentUser.passport_country));
        }
        if ( currentUser.passport_citizenship > 0) {
            userParams.put("passport_citizenship", String.valueOf(currentUser.passport_citizenship));
        }
        if ( currentUser.passport_date > 0) {
            userParams.put("passport_date", String.valueOf(currentUser.passport_date));
        }
        if ( currentUser.passport_number.length() > 0) {
            userParams.put("passport_number", String.valueOf(currentUser.passport_number));
        }

        if ( currentUser.passport_birthday > 0) {
            userParams.put("passport_birthday", String.valueOf(currentUser.passport_birthday));
        }

        if ( currentUser.passport_sex.length() > 0) {
            userParams.put("passport_sex", String.valueOf(currentUser.passport_sex));
        }

        return userParams;
    }

    @Override
    public void loadCards() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getCards(AppDelegate.getInstance().getToken());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response.body() cards: " + response.body());
                    try {
                        JSONObject status = new JSONObject(response.body().toString());

                        if (status.optInt("status") == 0) {
                            if (status.optJSONArray("data") != null && status.optJSONArray("data").length() > 0) {
                                parseCards(status.optJSONArray("data"));
                            } else {
                                cards = new ArrayList<Card>();
                                AppDelegate.getInstance().setCards(cards);

                                if ( bottomNavigation.getCurrentItem() == 1 && AppDelegate.getInstance().isNeedUpdate() ) {
                                    AppDelegate.getInstance().setNeedUpdate(false);
                                    bottomNavigation.setCurrentItem(1);
                                }
                            }
                        }
//                            if (profileFragment.isResumed()) {
//                                profileFragment.doWork();
//                            }
//
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response body null: " + errorConvector(response));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    @Override
    public void rotateReceipt(boolean rotate) {
//        this.rotate = rotate;
    }

    @Override
    public void loadCardIsDefault(int id, boolean state) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.editCardDefault(AppDelegate.getInstance().getToken(), id, state);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response.body() cards: " + response.body());
                    try {
                        JSONObject status = new JSONObject(response.body().toString());

                        if (status.optInt("status") == 0) {
                            myToast(getString(R.string.card_set_default));
                        }
//                            if (profileFragment.isResumed()) {
//                                profileFragment.doWork();
//                            }
//
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response body null: " + errorConvector(response));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    @Override
    public void callCards() {
        bottomNavigation.setCurrentItem(1);
    }

    @Override
    public void retakeReceipt(int id) {
//        Bundle bundle = new Bundle();
//
//        bundle.putInt("receiptId", id);
//
//        cameraFragment.setArguments(bundle);
//
//        getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.rv_container, cameraFragment, CameraBasicFragment.TAG)
//                .commit();
//
//        fragmentStack.push(cameraFragment);
    }

    @Override
    public void updateCheckPhoto(int id) {
        if ( loadCheck > 0) {
            Log.d(TAG, "response.body() update receipt: updateCheckPhoto");

            File receipt_file = new File(MainActivity.this.getExternalFilesDir(null), "checkPhoto.jpg");

//            try {
//                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(receipt_file));
//                bos.write(receiptPhoto);
//
//                bos.flush();
//                bos.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), receipt_file);

            MultipartBody.Part addition =
                    MultipartBody.Part.createFormData("receipt", String.valueOf(id));

            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("receipt_file", "receipt_file", requestFile);

            Gson gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                    .create();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(40, TimeUnit.SECONDS)
                    .writeTimeout(200, TimeUnit.SECONDS)
                    .readTimeout(45, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getString(R.string.base_url))
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ServiceApi serviceApi = retrofit.create(ServiceApi.class);

            Call<JsonObject> call = serviceApi.updateCheckPhoto(AppDelegate.getInstance().getToken(), addition, body);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.body() != null) {
                        Log.d(TAG, "response.body() update receipt: " + response.body().toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());

                            if (jsonObject.optInt("status") == 0) {
                                myToast(getString(R.string.check_updated_success));

                                receiptPhoto = null;
                                loadCheck = -1;

                                if ( homeFragment instanceof ReceiptsFragment && homeFragment.isResumed() ) {
                                    Log.d(TAG, "DO WORK 2");
                                    loadCurrentTripReceipts();
                                }

                                Log.d(TAG, "DO WORK 3");
                            } else {
                                JSONObject error = new JSONObject(jsonObject.optString("error"));

                                myToast(error.optString("message"));
                                receiptPhoto = null;
                                loadCheck = -1;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(errorConvector(response));
                            JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                            myToast(jsonObject1.optString("message"));

                            Log.d(TAG, "response error code:" + response.code());
                            Log.d(TAG, "response error:" + jsonObject1.optString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "response error:" + errorConvector(response));
                        }
                    }
                    loadCheck = -1;
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d(TAG, "response error: " + t.getMessage());
                }
            });
        } else {
            loadCheck = id;
        }
    }

    @Override
    public void deleteDialog(final int id) {
        final Dialog delDialog = new Dialog(this, R.style.Base_Theme_AppCompat_Dialog);
        delDialog.setContentView(R.layout.dialog_delete);

        final TextView cancel    = (TextView)       delDialog.getWindow().findViewById(R.id.account_cancel);
        final TextView delete    = (TextView)       delDialog.getWindow().findViewById(R.id.account_yes);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteReceipt(id);
                delDialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delDialog.dismiss();
            }
        });

        delDialog.show();
    }

    @Override
    public void deleteReceipt(final int id) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.deleteReceipt(AppDelegate.getInstance().getToken(), id);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response delete receipt:" + response.body());
                    try {

                        JSONObject status = new JSONObject(response.body().toString());
                        if (status.optInt("status") == 0) {
                            myToast(getString(R.string.receipt_delete_from_server));

                            for ( int i = 0; i < receipts.size(); i++ ) {
                                if ( receipts.get(i).id == id){
                                    receipts.remove(i);
                                }
                            }
                            ((ReceiptsFragment)homeFragment).doWork();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response body null: " + errorConvector(response));
                    myToast(errorConvector(response));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    @Override
    public void setNotifications(int count, int position) {

        bottomNavigation.setNotification(count, position);
        Log.d(TAG, "profile errorMessage: notify3 " );
    }

    @Override
    public void retakePassportPhoto() {
        CameraPassportFragment fragment = CameraPassportFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rv_container, fragment, CameraPassportFragment.TAG)
                .commit();

        fragmentStack.push(fragment);
    }

    @Override
    public boolean isPassportPhoto() {
        return bytesOfPassport;
    }

    @Override
    public void sendPassportPhoto() {
//        bottomNavigation.setCurrentItem(3);

//        if ( loadPassport ) {
            File passportImage =  new File(this.getExternalFilesDir(null), "passportPhoto.jpg");

            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), passportImage);

            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("passport", "passport", requestFile);

            Gson gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                    .create();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(200, TimeUnit.SECONDS)
                    .readTimeout(25, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getString(R.string.base_url))
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ServiceApi serviceApi = retrofit.create(ServiceApi.class);

            Call<JsonObject> call = serviceApi.uploadPassportPhoto(AppDelegate.getInstance().getToken(), body);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (response.body() != null) {
                        Log.d(TAG, "12334: " + response.body().toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());

                            if ( jsonObject.optInt("status") == 0 ) {
                                myToast(getString(R.string.passport_upload_success));

                                AppDelegate.getInstance().setHasPassport(true);
                                bytesOfPassport = false;
                                getUser();
                            } else {
                                JSONObject error = new JSONObject(jsonObject.optString("error"));
                                myToast(error.optString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(errorConvector(response));
                            JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                            myToast(jsonObject1.optString("message"));

                            Log.d(TAG, "response error code:" + response.code());
                            Log.d(TAG, "response error12:" + jsonObject1.optString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "response error12:" + errorConvector(response));
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d(TAG, "response error: " + t.getMessage());
                }
            });

//            loadPassport = false;
//        } else {
//            loadPassport = true;
//        }
    }

    @Override
    public void savePhotoPassport(final Bitmap passportPhoto) {
        this.bytesOfPassport = true;
        if ( passportPhoto != null  ) {
            new Thread(new Runnable() {
                public void run() {
                    Bitmap bmp = passportPhoto;

                    bmp = scaleBmp(bmp);

                    if (getDeviceName().contains("LGE Nexus 5X")) {
                        bmp = rotateImage(270, bmp);
                    } else {
                        bmp = rotateImage(90, bmp);
                    }
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.JPEG, 70, stream);

                    try {
                        File mFile = new File(MainActivity.this.getExternalFilesDir(null), "passportPhoto.jpg");

                        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(mFile));
                        bos.write(stream.toByteArray());
                        bos.flush();
                        bos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//                    if ( loadPassport ) {
                        sendPassportPhoto();
//                        loadPassport = false;
//                    } else {
//                        loadPassport = true;
//                    }
                }
            }).start();
        }
    }

    @Override
    public void callProfile() {
        bottomNavigation.setCurrentItem(3);
    }

    @Override
    public void changePasswordDialog(boolean cancelable) {
        final Dialog dialog = new Dialog( this, R.style.Base_Theme_AppCompat_Dialog);
        dialog.setContentView(R.layout.dialog_change_password);

        TextView               change    = (TextView)       dialog.getWindow().findViewById(R.id.change_yes);
        final TextView               error     = (TextView)       dialog.getWindow().findViewById(R.id.dialog_change_password_error_tv);

        final MaterialEditText password  = (MaterialEditText)       dialog.getWindow().findViewById(R.id.dialog_change_password);
        final MaterialEditText confirm   = (MaterialEditText)       dialog.getWindow().findViewById(R.id.dialog_change_password_confirm);


        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( password.getText().toString().length() > 7 && password.getText().toString().equals(confirm.getText().toString()) ) {
                    loadChangePassword(password.getText().toString(), dialog);
                    showMyProgressDialog("Changing password...");
                } else if ( password.getText().toString().length() < 7 ) {
                    error.setText(getResources().getString(R.string.to_short_password));
                } else  if ( !(password.getText().toString().equals(confirm.getText().toString()))) {
                    error.setText(getResources().getString(R.string.password_doesnt_match));
                }
            }
        });

        dialog.setCancelable(cancelable);
        dialog.show();
    }

    private void loadChangePassword(String newPassword, final Dialog dialog) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.changePassword(AppDelegate.getInstance().getToken(), newPassword);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response change password:" + response.body());
                    try {
                        JSONObject status = new JSONObject(response.body().toString());
                        if (status.optInt("status") == 0) {
                            myToast(getString(R.string._password_change_success));
                            dialog.dismiss();
                            dismissDialog();
                            AppDelegate.getInstance().setFirstLogin(0);
                            hideKeyboard();
                        } else {
                            JSONObject error = new JSONObject(status.optString("error"));

                            myToast(error.optString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response body null: " + errorConvector(response));
                    myToast(errorConvector(response));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    @Override
    public ArrayList<Card> getCurrentCards() {
        return cards;
    }

    @Override
    public void callMenu() {
        showPopup(findViewById(R.id.popup));
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                int id = menuItem.getItemId();

                switch (id) {
                    case R.id.faq:
                        startActivity(new Intent(MainActivity.this, FaqActivity.class).putExtra("faq", 0));
                        return true;
                    case R.id.termsandconditions:
                        startActivity(new Intent(MainActivity.this, FaqActivity.class).putExtra("faq", 1));
                        return true;
                    case R.id.video_tutorial:
                        startActivity(new Intent(MainActivity.this, VideoActivity.class));
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private void downloadPDF(final int position) {
        File tempFile = new File(getExternalFilesDir(null), vatForms.get(position).updated + ".pdf");

        if (!tempFile.exists()) {
            Retrofit retrofit = new Retrofit.Builder().
                    baseUrl(getString(R.string.base_url)).
                    build();

            ServiceApi retrofitDownload = retrofit.create(ServiceApi.class);

            Call<ResponseBody> call = retrofitDownload.downloadFileWithDynamicUrlSync(
                    AppDelegate.getInstance().getToken(), vatForms.get(position).file);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    File file = new File(getExternalFilesDir(null), vatForms.get(position).updated + ".pdf");
                    try {
                        OutputStream output = new FileOutputStream(file);
                        try {
                            try {
                                byte[] buffer = new byte[4 * 1024]; // or other buffer size
                                int read;

                                while ((read = response.body().byteStream().read(buffer)) != -1) {
                                    output.write(buffer, 0, read);
                                }
                                output.flush();
                            } finally {
                                output.close();
                            }
                        } catch (Exception e) {
                            e.printStackTrace(); // handle exception, define IOException and others
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }

    @Override
    public void onBackPressed() {

        if ( fragmentStack.size() > 1 ) {
            fragmentStack.pop();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.rv_container, fragmentStack.lastElement()).commit();

            Log.d(TAG, "fragment: " + fragmentStack.lastElement().getTag() );
        } else {
            if ( doubleBackToExitPressedOnce ) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getString(R.string._back_pressed), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    private void getTotalRefundReceipt() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getTotalRefundReceipts(AppDelegate.getInstance().getToken());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response.body() cards: " + response.body());
                    try {
                        JSONObject status = new JSONObject(response.body().toString());

                        if ( status.optInt("status") == 0 ) {
                            JSONObject data = new JSONObject(status.optString("data"));
                            AppDelegate.getInstance().setTotalRefundReceipts(data.optString("total_refund"));
                        }
//                            if (profileFragment.isResumed()) {
//                                profileFragment.doWork();
//                            }
//
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response body null: " + errorConvector(response));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    @Override
    public void prepareFragmentStack() {
        for ( int i = 0; i < fragmentStack.size() - 1; i++ ) {
            fragmentStack.pop();
        }
    }

    @Override
    public void startCardIO() {
        Intent scanIntent = new Intent(this, CardIOActivity.class);
//
        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false); // default: true

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.

        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    public DatabaseReference getFireBaseRef() {
        return mFirebaseRef;
    }

    private void getToken() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .disableHtmlEscaping()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getToken(AppDelegate.getInstance().getToken());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response token firebase:" + response.body().toString());
                    try {
                        JSONObject status = new JSONObject(response.body().toString());
                        if (status.optInt("status") == 0) {
                            JSONObject jsonObject = new JSONObject(status.optString("data"));
//                            mCustomToken = jsonObject.optString("token");
                            AppDelegate.getInstance().saveFireBaseToken(jsonObject.optString("token"));
                            AppDelegate.getInstance().saveFireBaseRoom(jsonObject.optInt("room_id"));
                            startSignIn(jsonObject.optString("token"));

//                            getActivity().startActivity(new Intent(getActivity(), ChatActivity.class));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response countries error: " + t.getMessage());
            }
        });
    }

    private void startSignIn(String token) {
        mAuth.signInWithCustomToken(token)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        AppDelegate.getInstance().setNotificationCount(0);
                        Log.d(TAG, "signInWithCustomToken:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCustomToken", task.getException());
                        }
                    }
                });

        FirebaseDatabase base = FirebaseDatabase.getInstance();
        mFirebaseRef = base.getReference("firebase/chat/rooms/" + AppDelegate.getInstance().getFireBaseRoom());
        mFirebaseRef = mFirebaseRef.child("/messages/");


        mFirebaseRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                try {
//                Log.d(TAG, "setNotification + 1, seen: " + dataSnapshot.getValue().toString());
                Chat chat = dataSnapshot.getValue(Chat.class);

//                Log.d(TAG, "key: " + chat.key);

                if (!chatFrg.isResumed() && !chat.user_id.equals(userId) && chat.seen == 0) {
//                    AppDelegate.getInstance().setNotificationCount(1);
                    bottomNavigation.setNotification(AppDelegate.getInstance().getNotificationCounter(), 2);

                }
//                } catch (DatabaseException exception) {}
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        Log.d(TAG, "device_data:   " + AppDelegate.getInstance().getFireBaseToken());
        Log.d(TAG, "device_FCM token:   " + AppDelegate.getInstance().getFireBasePushToken());
    }

}
