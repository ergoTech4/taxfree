package eu.taxfree4u.client.interfaces;


import android.graphics.Bitmap;

import com.google.firebase.database.DatabaseReference;

import java.io.File;
import java.util.ArrayList;

import eu.taxfree4u.client.model.Card;
import eu.taxfree4u.client.model.Receipt;
import eu.taxfree4u.client.model.VatForm;

public interface AppInterface {

    void hideKeyboard();
    void logout();
    ArrayList<Receipt> getCurrentReceipts();
    ArrayList<VatForm> getCurrentVatForms();

    void callDetails();
    void callVatFrom();
    void callReceipts();
    void loadCheckPhoto();
    void addReceipt();

    boolean isCheckPhoto();

    void savePhotoCheck(Bitmap image);

    void hideBottomMenu(boolean hide);

    void loadCurrentTrip();
    void callCountyFragment(String tag);

    void callPreviousFragment();

    void editTrip();
    void saveUser();

    ArrayList<Card> getCurrentCards();

    void callMenu();
    void loadCards();
    void loadCurrentTripVatForms();

    void rotateReceipt(boolean rotate);

    void loadCardIsDefault(int id, boolean state);

    void callCards();

    void retakeReceipt(int id);

    void updateCheckPhoto(int id);

    void deleteDialog(int id);

    void setNotifications(int count, int position);

    void retakePassportPhoto();

    boolean isPassportPhoto();

    void sendPassportPhoto();

    void savePhotoPassport(Bitmap passportPhoto);

    void callProfile();

    void changePasswordDialog(boolean cancelable);
    void deleteReceipt(final int id);
    void prepareFragmentStack();

    void startCardIO();

    DatabaseReference getFireBaseRef();
//
//    void callOnSignInPhone(String status, String text);
//    void callOnSignUpPin(String status);
//    void callOnSignUpPhoto(String status);
//    void callOnSignUpPass(String status);
//    void callOnSignUpCardId(String status);
}
