package eu.taxfree4u.client.interfaces;

import java.util.ArrayList;

import eu.taxfree4u.client.model.Receipt;

/**
 * Created by ergo on 10/24/16.
 */

public interface ChatInterface {

    void setSendList(ArrayList<Receipt> sendList);
}
