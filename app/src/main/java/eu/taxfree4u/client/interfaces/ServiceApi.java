package eu.taxfree4u.client.interfaces;

import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ServiceApi {

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("/v2api/login")
    Call<JsonObject> login(
            @Header("locale")   String locale,
            @Field("email")     String email,
            @Field("password")  String password,
            @Field("device_id") String fireBaseToken,
            @Field("device_data")String device_data
    );

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @FormUrlEncoded
    @POST("/v2api/registration")
    Call<JsonObject> registration(
            @Header("locale")       String locale,
            @Field("email")         String email,
            @Field("password")      String password,
            @Field("device_id")     String device_id,
            @Field("device_data")   String device_data,
            @Field("promo_code")    String promo_code);

    @Multipart
    @POST("/v2api/profile/passport_load")
    Call<JsonObject> uploadPassportPhoto(
            @Header("access-token") String token,
            @Part MultipartBody.Part file);

    @Multipart
    @POST("/v2api/receipt/add")
    Call<JsonObject> uploadCheckPhoto(
            @Header("access-token") String token,
            @Part MultipartBody.Part file);

    @Multipart
    @POST("/v2api/receipt/edit")
    Call<JsonObject> updateCheckPhoto(
            @Header("access-token") String token,
            @Part MultipartBody.Part id,
            @Part MultipartBody.Part file);

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @GET("/v2api/trip/current")
    Call<JsonObject> getCurrentTrip(
            @Header("access-token") String token);

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @GET("/v2api/receipt/list")
    Call<JsonObject> getTripReceipts(
            @Header("access-token") String token, @Query("trip") int trip);

    @GET("/v2api/trip/countries")
    Call<JsonObject> getCountries(@Header("locale") String locale,
                                  @Header("access-token") String token,
                                  @Query("update_country_time") int update_country_time);

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @FormUrlEncoded
    @POST("/v2api/trip/add")
    Call<JsonObject> addTrip(
            @Header("locale")               String locale,
            @Header("access-token")         String token,
            @Field("date_start")            long date_start,
            @Field("date_end")              long date_end,
            @Field("departure_country")     int departure_country,
            @Field("destination_country")   int destination_country,
            @Field("flight_number")         String flight_number,
            @Field("city")                  String city);

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @FormUrlEncoded
    @PUT("/v2api/trip/edit")
    Call<JsonObject> editTrip(
            @Header("locale")               String locale,
            @Header("access-token")         String token,
            @Field("date_start")            long date_start,
            @Field("date_end")              long date_end,
            @Field("departure_country")     int departure_country,
            @Field("destination_country")   int destination_country,
            @Field("flight_number")         String transport_enter,
            @Field("city")                  String city);

    @GET("/v2api/profile/view")
    Call<JsonObject> getUser(@Header("access-token") String token);

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @FormUrlEncoded
    @PUT("/v2api/profile/edit")
    Call<JsonObject> saveUser(@Header("access-token")           String token,
                              @Field("phone")                   String phone,
                              @Field("phone_country")           long phone_country,
                              @Field("full_name")               String full_name,
                              @Field("email")                   String email,
                              @Field("country")                 long country,
                              @Field("city")                    String city,
                              @Field("address")                 String address,
                              @Field("post_code")               int post_code,
                              @Field("passport_country")        long passport_country,
                              @Field("passport_sex")            String passport_sex,
                              @Field("passport_number")         String passport_number,
                              @Field("passport_citizenship")    int passport_citizenship,
                              @Field("passport_date")           long passport_date);


    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @FormUrlEncoded
    @PUT("/v2api/profile/edit")
    Call<JsonObject> updateUser(@Header("access-token") String token,
                                @FieldMap Map<String, String> options);


    @GET("/v2api/declaration/list")
    Call<JsonObject> getTripVatForms(@Header("access-token")String token,
                                     @Query("trip") Integer trip);


    @GET("/v2api/card/list")
    Call<JsonObject> getCards(@Header("access-token")String token);

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @FormUrlEncoded
    @POST("/v2api/card/add")
    Call<JsonObject> addCard(@Header("locale")      String locale,
                             @Header("access-token")String token,
                             @Field("name")         String name,
                             @Field("number")       String number,
                             @Field("expire_month") String expire_month,
                             @Field("expire_year")  String expire_year,
                             @Field("is_default")   int is_default);

    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlSync(@Header("access-token")String token, @Url String fileUrl);


    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @FormUrlEncoded
    @POST("/v2api/forgot_password_request")
    Call<JsonObject> forgotPassword(@Field("email") String email);


    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @FormUrlEncoded
    @PUT("/v2api/card/edit")
    Call<JsonObject> editCardDefault(@Header("access-token") String token, @Field("id")int id, @Field("is_default")boolean state);

    @DELETE("/v2api/card/delete")
    Call<JsonObject> deleteCard(@Header("access-token") String token, @Query("id") int id);

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @FormUrlEncoded
    @PUT("/v2api/card/edit")
    Call<JsonObject> editCard(@Header("access-token")String token,
                              @Field("id")           int id,
                              @Field("name")         String name,
                              @Field("number")       String number,
                              @Field("expire_month") String expire_month,
                              @Field("expire_year")  String expire_year,
                              @Field("is_default")   boolean is_default);





    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @DELETE("/v2api/receipt/delete")
    Call<JsonObject> deleteReceipt(@Header("access-token")String token, @Query("id") int id);



    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @FormUrlEncoded
    @POST("/v2api/change_password")
    Call<JsonObject> changePassword(@Header("access-token")String token, @Field("new_password") String newPassword);


    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @GET("/v2api/receipt/total_refund")
    Call<JsonObject> getTotalRefundReceipts(
            @Header("access-token") String token);

    @Headers("Content-Type: application/x-www-form-urlencoded;charset=utf-8")
    @GET("/v2api/declaration/total_refund")
    Call<JsonObject> getTotalRefund(@Header("access-token")String token);


    @GET("/v2api/chat/room")
    Call<JsonObject> getToken(@Header("access-token")String token);

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("/v2api/logout")
    Call<JsonObject> logout(@Header("access-token")String token);

}


















