package eu.taxfree4u.client.interfaces;

public interface FragmentInterface {

    void doWork();
    void saveChanges();
    void showImage(int position);

    void retakeReceipt(int id);

    void deleteReceipt(int id);
}
