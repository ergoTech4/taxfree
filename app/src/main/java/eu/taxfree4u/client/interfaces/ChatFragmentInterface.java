package eu.taxfree4u.client.interfaces;

/**
 * Created by ergo on 10/12/16.
 */

public interface ChatFragmentInterface {

    void hideBar(boolean hide);
}
