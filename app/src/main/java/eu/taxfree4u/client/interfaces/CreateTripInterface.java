package eu.taxfree4u.client.interfaces;

import android.graphics.Bitmap;

import eu.taxfree4u.client.model.TripObj;

public interface CreateTripInterface {

//    void savePhotoCheck(byte[] bytesOfPhoto);
    void savePhotoPassport(final Bitmap imageBitmap);
    void callRegisterStep1();
    void callRegisterStep2();
    void callRegisterStep3();
    void callPassportPhoto();
    void callCards();
    void savePhotoCheck(final Bitmap imageBitmap);

    Bitmap getCheckPhoto();
    Bitmap getPassportPhoto();

    void startCardIO();
    boolean isPassportPhoto();
    boolean isCheckPhoto();

    void myToast(String msg);

    void createTrip();
    TripObj getTripObj();
    void setTripObj(TripObj tripObj);

    void callCountyFragment(String tag);

    void callPreviousFragment();
    void saveUser();

    void rotateReceipt(boolean rotate);

    void hideKeyboard();

    void loadCardIsDefault(int id, boolean is_default);

    void loadCards();
}

