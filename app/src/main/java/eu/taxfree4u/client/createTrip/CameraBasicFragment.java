package eu.taxfree4u.client.createTrip;

import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.activities.MainActivity;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.CreateTripInterface;
import eu.taxfree4u.client.tools.AppDelegate;


public class  CameraBasicFragment extends Fragment implements SurfaceHolder.Callback
        , Camera.PictureCallback, Camera.PreviewCallback, Camera.AutoFocusCallback {

    public static final String TAG = "CameraBasicFragment";

    private CreateTripInterface fragmentHolder;
    private AppInterface        mainHolder;

    private Camera          camera;
    private SurfaceHolder   surfaceHolder;
    private SurfaceView     preview;
    private ImageView       capturePhoto, retakePhoto, library;
    private LinearLayout    comments_ll;
    private TextView        retakeTv, libraryTv, skip;
    private CountDownTimer  demon;
    private boolean         capture;

    public CameraBasicFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if ( context instanceof CreateTripInterface ) {
                fragmentHolder  = (CreateTripInterface) context;
            } else {
                mainHolder      = (AppInterface) context;
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    public static CameraBasicFragment newInstance() {
        return new CameraBasicFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);

        initialize(view);

        capturePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragmentHolder != null) {
                    if (fragmentHolder.isCheckPhoto()) {
                        if (AppDelegate.getInstance().getHasPassport()) {
                            fragmentHolder.callRegisterStep3();
                        } else {
                            fragmentHolder.callRegisterStep1();
                        }
                    } else {
                        try {
                            capture = true;
                            camera.autoFocus(CameraBasicFragment.this);
                            demon.cancel();
                        } catch (Exception ignored) {
                        }
                    }
                } else {
                    if ( mainHolder.isCheckPhoto() ) {
                        int id = -1;
                        try {
                            id = getArguments().getInt("receiptId");
                        } catch (NullPointerException e) {}

                        Log.d(TAG, "receiptId: " + id );

                        if ( id > 0 ) {
                            mainHolder.updateCheckPhoto(id);
                        } else {
                            mainHolder.loadCheckPhoto();
                        }
                    } else {
                        try {
                            capture = true;
                            camera.autoFocus(CameraBasicFragment.this);
                        } catch (Exception ignored) {}
                    }
                }
            }
        });

        retakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( camera != null ) {
                    camera.startPreview();
                } else {
                    onResume();
                }
                capture = false;

                retakeTv.setVisibility(View.GONE);
                retakePhoto.setVisibility(View.GONE);
                capturePhoto.setImageResource(R.drawable.image_camera);


                library.setVisibility(View.VISIBLE);
                libraryTv.setVisibility(View.VISIBLE);

                if ( fragmentHolder != null ) {
                    fragmentHolder.savePhotoCheck(null);
                    skip.setVisibility(View.VISIBLE);
                } else {
                    mainHolder.savePhotoCheck(null);
                }
            }
        });

        library.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( camera != null ) {
                    camera.stopPreview();
                }

                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 1);
            }
        });


        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( fragmentHolder != null ) {
                    if (AppDelegate.getInstance().getHasPassport()) {
                        fragmentHolder.callRegisterStep3();
                    } else {
                        fragmentHolder.callRegisterStep1();
                    }
                } else {
                    mainHolder.callReceipts();
                }
            }
        });

        return view;
    }

    private void initialize(View view) {
        preview         = (SurfaceView) view.findViewById(R.id.surfaceView1);

        capturePhoto    = (ImageView) view.findViewById(R.id.capture_photo);
        retakePhoto     = (ImageView) view.findViewById(R.id.camera_retake);
        library         = (ImageView) view.findViewById(R.id.camera_library);

        retakeTv        = (TextView) view.findViewById(R.id.camera_retake_tv);
        libraryTv       = (TextView) view.findViewById(R.id.camera_library_tv);
        skip            = (TextView) view.findViewById(R.id.camera_skip);

        comments_ll     = (LinearLayout) view.findViewById(R.id.comment_ll);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && data != null ) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();


                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                Bitmap bmp = BitmapFactory.decodeFile(picturePath);

                Drawable d = new BitmapDrawable(getResources(), bmp);

                preview.setBackgroundDrawable(d);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 40, stream);

                if (fragmentHolder != null) {
                    fragmentHolder.rotateReceipt(false);
//                    fragmentHolder.savePhotoCheck(stream.toByteArray());
                } else {
                    mainHolder.rotateReceipt(false);
//                    mainHolder.savePhotoCheck(stream.toByteArray());
                }


                retakeTv.setVisibility(View.VISIBLE);
                retakePhoto.setVisibility(View.VISIBLE);
                capturePhoto.setImageResource(R.drawable.image_ok);
                skip.setVisibility(View.GONE);

                library.setVisibility(View.GONE);
                libraryTv.setVisibility(View.GONE);

            }

        } else {
            if ( camera != null ) {
                camera.startPreview();
            } else {
                onResume();
            }

            comments_ll.setVisibility(View.GONE);
//            file = null;
//            fragmentHolder.setFile(file);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if ( getActivity() instanceof CreateTripActivity && fragmentHolder == null ) {
            fragmentHolder = (CreateTripInterface) getActivity();

            fragmentHolder.hideKeyboard();
        } else if ( getActivity() instanceof MainActivity && mainHolder == null ) {
            mainHolder = (AppInterface) getActivity();
            mainHolder.hideKeyboard();
        }

        if ( mainHolder != null ) {
            mainHolder.hideBottomMenu(true);
            mainHolder.hideKeyboard();
            skip.setVisibility(View.GONE);
        } else if (fragmentHolder != null ) {
            fragmentHolder.hideKeyboard();
        }
//        fragmentHolder.hideKeyboard();

//        file = fragmentHolder.getFile();
//
//        if ( file == null ) {
            try {
                camera = Camera.open();

                surfaceHolder = preview.getHolder();

                surfaceHolder.addCallback(CameraBasicFragment.this);
                surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    camera.enableShutterSound(true);
                }

            } catch (RuntimeException e) {
//            }

//            if (gps != null) {
//                gps.startUsingGPS();
//            }
        }

        demon = new CountDownTimer(10000, 2000) {
            @Override
            public void onTick(long l) {
                try {
                    camera.autoFocus(CameraBasicFragment.this);
                } catch (RuntimeException e) {}

            }

            @Override
            public void onFinish() {
                demon.start();
            }
        };

        demon.start();

    }

    @Override
    public void onPause() {
        if (camera != null) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }

        if ( mainHolder != null ) {
            mainHolder.hideBottomMenu(false);
        }

        if ( fragmentHolder != null ) {
            fragmentHolder.hideKeyboard();
        }

//        if ( gps != null ) {
//            gps.stopUsingGPS();
//        }

        demon.cancel();

        super.onPause();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if ( getDeviceName().contains("Nexus 5X") ) {
                camera.setDisplayOrientation(270);
            } else {
                camera.setDisplayOrientation(90);
            }

            camera.setPreviewDisplay(holder);
            camera.setPreviewCallback(this);

            int previewSurfaceWidth = preview.getWidth();
            int previewSurfaceHeight = preview.getHeight();

            ViewGroup.LayoutParams lp = preview.getLayoutParams();

            lp.width = previewSurfaceWidth;
            lp.height = previewSurfaceHeight;

            preview.setLayoutParams(lp);
            camera.startPreview();
        } catch (IOException | RuntimeException e) {
            e.printStackTrace();

        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onPictureTaken(byte[] paramArrayOfByte, final Camera paramCamera) {
        camera.stopPreview();
        capture = false;

        if ( fragmentHolder != null ) {
            fragmentHolder.rotateReceipt(true);
//            fragmentHolder.savePhotoCheck(paramArrayOfByte);
        } else {
            mainHolder.rotateReceipt(true);
//            mainHolder.savePhotoCheck(paramArrayOfByte);
        }

        retakeTv.setVisibility(View.VISIBLE);
        retakePhoto.setVisibility(View.VISIBLE);
        capturePhoto.setImageResource(R.drawable.image_ok);
        skip.setVisibility(View.GONE);

        library.setVisibility(View.GONE);
        libraryTv.setVisibility(View.GONE);

    }

    @Override
    public void onAutoFocus(boolean paramBoolean, Camera paramCamera) {
        if (paramBoolean && capture ) {
            // если удалось сфокусироваться, делаем снимок
            try {
                paramCamera.takePicture(null, null, null, this);
            } catch (RuntimeException e) {}
        }
    }

    @Override
    public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera) {
        // здесь можно обрабатывать изображение, показываемое в preview
    }


    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

//        String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
//                phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
//            phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }

}