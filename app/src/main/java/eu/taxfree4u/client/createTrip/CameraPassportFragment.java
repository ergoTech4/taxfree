package eu.taxfree4u.client.createTrip;

import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.activities.MainActivity;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.CreateTripInterface;


public class CameraPassportFragment extends Fragment implements SurfaceHolder.Callback
        , Camera.PictureCallback, Camera.PreviewCallback, Camera.AutoFocusCallback {

    public static final String TAG = "CameraPassportFragment";

    private CreateTripInterface fragmentHolder;
    private AppInterface        mainHolder;

    private Camera          camera;
    private SurfaceHolder   surfaceHolder;
    private SurfaceView     preview;
    private ImageView       capturePhoto, retakePhoto, library;
    private LinearLayout    comments_ll;
    private TextView        retakeTv, libraryTv, skip;
    private Button          doneButton;
    private byte[]          bytesOfPhoto;
    private File            mFile;

    private CountDownTimer  demon;
    private boolean         capture;



    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    public CameraPassportFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if ( context instanceof CreateTripInterface ) {
                fragmentHolder  = (CreateTripInterface) context;
            } else {
                mainHolder      = (AppInterface) context;
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }


    public static CameraPassportFragment newInstance() {
        return new CameraPassportFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_passport_photo, container, false);

        initialize(view);

        if ( mainHolder != null ) {
            skip.setText(getString(R.string.close));
        }

        capturePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( fragmentHolder != null ) {
                    if (fragmentHolder.isPassportPhoto()) {
                        fragmentHolder.callRegisterStep1();
                    } else {
                        try {
                            demon.cancel();
                            capture = true;
                            camera.autoFocus(CameraPassportFragment.this);
                        } catch (Exception ignored) {}
                    }
                } else if ( mainHolder != null ) {
                    if (mainHolder.isPassportPhoto()) {
                        mainHolder.sendPassportPhoto();
                    } else {
                        try {
                            demon.cancel();
                            capture = true;
                            camera.autoFocus(CameraPassportFragment.this);
                        } catch (Exception ignored) {}
                    }
                }
            }
        });

//        client = fragmentHolder.getCurrentClient();

        retakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( camera != null ) {
                    camera.startPreview();
                } else {
                    onResume();
                }

                capture = false;
                demon.start();

                if ( fragmentHolder != null ) {
                    fragmentHolder.savePhotoPassport(null);
                } else if( mainHolder != null ) {
                    mainHolder.savePhotoPassport(null);
                }

                retakeTv.setVisibility(View.GONE);
                retakePhoto.setVisibility(View.GONE);
                capturePhoto.setImageResource(R.drawable.image_camera);

                skip.setVisibility(View.VISIBLE);
                library.setVisibility(View.VISIBLE);
                libraryTv.setVisibility(View.VISIBLE);

            }
        });

        library.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( camera != null ) {
                    camera.stopPreview();
                }

                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 1);
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ( fragmentHolder != null ) {

                    if (fragmentHolder.isPassportPhoto()) {
                        fragmentHolder.callRegisterStep2();
                    } else {
                        fragmentHolder.myToast("Upload passport photo please");
                    }
                } else if( mainHolder != null ) {
                    mainHolder.callProfile();
                }
            }
        });

        return view;
    }

    private void initialize(View view) {
        preview         = (SurfaceView) view.findViewById(R.id.surfaceView2);

        capturePhoto    = (ImageView) view.findViewById(R.id.capture_photo);
        retakePhoto     = (ImageView) view.findViewById(R.id.camera_retake);
        library         = (ImageView) view.findViewById(R.id.camera_library);

        retakeTv        = (TextView) view.findViewById(R.id.camera_retake_tv);
        libraryTv       = (TextView) view.findViewById(R.id.camera_library_tv);
        skip            = (TextView) view.findViewById(R.id.camera_skip);

        comments_ll     = (LinearLayout) view.findViewById(R.id.comment_ll);

        doneButton      = (Button) view.findViewById(R.id.photo_done);
    }

    private Bitmap scaleBmp(Bitmap bmp){
        final int maxSize = 1100;
        int outWidth;
        int outHeight;
        int inWidth = bmp.getWidth();
        int inHeight = bmp.getHeight();

        if(inWidth < inHeight){
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }

        return Bitmap.createScaledBitmap(bmp, outWidth, outHeight, false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1 && data != null ) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                Bitmap bmp =  BitmapFactory.decodeFile(picturePath);

                Drawable d = new BitmapDrawable(getResources(), bmp);

                preview.setBackgroundDrawable(d);

//                bmp = scaleBmp(bmp);


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 70, stream);

                retakeTv.setVisibility(View.VISIBLE);
                retakePhoto.setVisibility(View.VISIBLE);
                capturePhoto.setImageResource(R.drawable.image_ok);
                skip.setVisibility(View.GONE);

                library.setVisibility(View.GONE);
                libraryTv.setVisibility(View.GONE);

                if ( fragmentHolder != null ) {
                    fragmentHolder.rotateReceipt(false);
//                    fragmentHolder.savePhotoPassport(stream.toByteArray());
                } else if ( mainHolder != null) {
                    mainHolder.rotateReceipt(false);
//                    mainHolder.savePhotoPassport(stream.toByteArray());
                }
            }
        } else {
            if ( camera != null ) {
                camera.startPreview();
            } else {
                onResume();
            }

            comments_ll.setVisibility(View.GONE);
//            file = null;
//            fragmentHolder.setFile(file);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if ( getActivity() instanceof CreateTripActivity && fragmentHolder == null ) {
            fragmentHolder = (CreateTripInterface) getActivity();
        } else if ( getActivity() instanceof MainActivity && mainHolder == null ) {
            mainHolder = (AppInterface) getActivity();
        }

        if ( mainHolder != null ) {
            mainHolder.hideBottomMenu(true);
            mainHolder.hideKeyboard();
        } else if (fragmentHolder != null ) {
            fragmentHolder.hideKeyboard();
        }

//        fragmentHolder.hideKeyboard();

//        file = fragmentHolder.getFile();
//
//        if ( file == null ) {
            try {
                camera = Camera.open();

                surfaceHolder = preview.getHolder();

                surfaceHolder.addCallback(CameraPassportFragment.this);
                surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    camera.enableShutterSound(true);
                }

            } catch (RuntimeException e) {
//            }

//            if (gps != null) {
//                gps.startUsingGPS();
//            }
        }

        demon = new CountDownTimer(10000, 1600) {
            @Override
            public void onTick(long l) {
                try {
                    camera.autoFocus(CameraPassportFragment.this);
                } catch (RuntimeException e) {}

            }

            @Override
            public void onFinish() {
                demon.start();
            }
        };

        demon.start();
    }

    @Override
    public void onPause() {
        if (camera != null) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }

        if ( mainHolder != null ) {
            mainHolder.hideBottomMenu(false);
        }

//        if ( gps != null ) {
//            gps.stopUsingGPS();
//        }

        super.onPause();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {

//            mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
//            int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
//            ORIENTATIONS.get(rotation) +  270) % 360;
//            camera.setDisplayOrientation((ORIENTATIONS.get(rotation) + 180) % 360);

            if ( getDeviceName().contains("Nexus 5X") ) {
                camera.setDisplayOrientation(270);
//                camera.setDisplayOrientation(180);
                Log.d(TAG, "getDeviceName :" + getDeviceName());
            } else {
                Log.d(TAG, " else getDeviceName :" + getDeviceName());
                camera.setDisplayOrientation(90);
            }

            camera.setPreviewDisplay(holder);
            camera.setPreviewCallback(this);

            int previewSurfaceWidth = preview.getWidth();
            int previewSurfaceHeight = preview.getHeight();

            ViewGroup.LayoutParams lp = preview.getLayoutParams();

            lp.width = previewSurfaceWidth;
            lp.height = previewSurfaceHeight;

//            Log.d(TAG, "getDeviceName :" + getDeviceName());

            preview.setLayoutParams(lp);
            camera.startPreview();
        } catch (IOException | RuntimeException e) {
            e.printStackTrace();

        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    public Bitmap rotateImage(int angle, Bitmap bitmapSrc) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(bitmapSrc, 0, 0,
                bitmapSrc.getWidth(), bitmapSrc.getHeight(), matrix, true);
    }

    @Override
    public void onPictureTaken(byte[] paramArrayOfByte, final Camera paramCamera) {
        camera.stopPreview();

//        mFile = new File(getActivity().getExternalFilesDir(null), "passportPhoto.jpg");
////
//        try {
//            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(mFile));
//            Bitmap bmp = BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length);
//
//            bmp = scaleBmp(bmp);
//
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            bmp.compress(Bitmap.CompressFormat.JPEG, 40, stream);
//
//            bos.write(stream.toByteArray());
//            bos.flush();
//            bos.close();
////
////            Log.d(TAG, "1333 filepath: " + mFile.getCanonicalPath());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

//        fragmentHolder.savePhotoCheck(paramArrayOfByte);

//        Bitmap bmp = BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length);
        retakeTv.setVisibility(View.VISIBLE);
        retakePhoto.setVisibility(View.VISIBLE);
        capturePhoto.setImageResource(R.drawable.image_ok);
        skip.setVisibility(View.GONE);

        library.setVisibility(View.GONE);
        libraryTv.setVisibility(View.GONE);

        if ( fragmentHolder != null ) {
            fragmentHolder.rotateReceipt(true);
//            fragmentHolder.savePhotoPassport(paramArrayOfByte);
        } else if ( mainHolder != null ) {
            mainHolder.rotateReceipt(true);
//            mainHolder.savePhotoPassport(paramArrayOfByte);
        }
    }

//        new Thread(new Runnable() {
//            public void run() {
//                final String tempComments = edit_comment.getText().toString();
//                Bitmap bmp = BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length);
//
//                bmp = scaleBmp(bmp);
//
//                if ("Nexus 5X".contains(getDeviceName())) {
//                    bmp = rotateImage(270, bmp);
//                } else {
//                    bmp = rotateImage(90, bmp);
//                }
//
//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                flippedImageByteArray[0] = stream.toByteArray();
//
//                if ( done ) {
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                invoice = ParseObject.create("Invoice");
//
//                                file = new ParseFile("invoice.jpg", flippedImageByteArray[0]);
//                                final ParseObject cl = client;
//                                if (gps != null) {
//                                    ParseGeoPoint geoPoint = new ParseGeoPoint();
//                                    geoPoint.setLatitude(gps.getLatitude());
//                                    geoPoint.setLongitude(gps.getLongitude());
//
//                                    invoice.put("invoiceLocation", geoPoint);
//                                }
//
//                                invoice.put("comment", tempComments);
//                                invoice.put("client", cl);
//                                invoice.put("user", ParseUser.getCurrentUser());
//                                invoice.put("photo", file);
//                                invoice.saveInBackground(new SaveCallback() {
//                                    @Override
//                                    public void done(ParseException e) {
//                                        if (e == null) {
//                                            fragmentHolder.myToast(getString(R.string.add_invoice_success));
//
////                                            fragmentHolder.dismissDialog();
//
//                                            ParseRelation<ParseObject> relation = cl.getRelation("invoices");
//
//                                            relation.add(invoice);
//
//                                            cl.saveInBackground();
//                                            file = null;
//                                            fragmentHolder.setFile(file);
//
//                                            //createSuccessDialog("");
//                                        }
//                                    }
//                                });
//                            } catch (Exception ignored) {
//                            }
////                            edit_comment.setText("");
//                            done = false;
//                        }
//                    }).start();
//                } else {
//                    done = true;
//                }
//
//            }
//        }).start();
//
//        if ( fragmentHolder.getCurrentClient() != null ) {
//
//            doneButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    fragmentHolder.hideKeyboard();
//                    final String tempComments = edit_comment.getText().toString();
//                    edit_comment.setText("");
//
//                    if ( done ) {
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                try {
//                                    invoice = ParseObject.create("Invoice");
//
//                                    file = new ParseFile("invoice.jpg", flippedImageByteArray[0]);
//                                    final ParseObject cl = client;
//                                    if (gps != null) {
//                                        ParseGeoPoint geoPoint = new ParseGeoPoint();
//                                        geoPoint.setLatitude(gps.getLatitude());
//                                        geoPoint.setLongitude(gps.getLongitude());
//
//                                        invoice.put("invoiceLocation", geoPoint);
//                                    }
//
//                                    invoice.put("comment", tempComments);
//                                    invoice.put("client", cl);
//                                    invoice.put("user", ParseUser.getCurrentUser());
//                                    invoice.put("photo", file);
//                                    invoice.saveInBackground(new SaveCallback() {
//                                        @Override
//                                        public void done(ParseException e) {
//                                            if (e == null) {
//                                                fragmentHolder.myToast(getString(R.string.add_invoice_success));
//
////                                            fragmentHolder.dismissDialog();
//
//                                                ParseRelation<ParseObject> relation = cl.getRelation("invoices");
//
//                                                relation.add(invoice);
//
//                                                cl.saveInBackground();
//                                                file = null;
//                                                fragmentHolder.setFile(file);
//
//                                                //createSuccessDialog("");
//                                            }
//                                        }
//                                    });
//                                } catch (Exception ignored) {
//                                }
////                            edit_comment.setText("");
//                                done = false;
//                            }
//                        }).start();
//                    } else {
//                        done = true;
//                    }
////                    fragmentHolder.showMenu();
//                    camera.startPreview();
//
//                    comments_ll.postDelayed(new Runnable() {
//                        public void run() {
//                            comments_ll.setVisibility(View.GONE);
//                        }
//                    }, 350);
//                }
//            });
//        } else {
//            doneButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    fragmentHolder.hideKeyboard();
//                    fragmentHolder.setNavItem(2);
//                    final String tempComments = edit_comment.getText().toString();
//                    edit_comment.setText("");
//
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            try {
//                                invoice = ParseObject.create("Invoice");
//
//                                file = new ParseFile("invoice.jpg", flippedImageByteArray[0]);
//
//                                if ( gps != null ) {
//                                    ParseGeoPoint geoPoint = new ParseGeoPoint();
//                                    geoPoint.setLatitude(gps.getLatitude());
//                                    geoPoint.setLongitude(gps.getLongitude());
//
//                                    invoice.put("invoiceLocation", geoPoint);
//                                }
//
//                                invoice.put("comment", tempComments);
//                                invoice.put("user", ParseUser.getCurrentUser());
//                                invoice.put("photo", file);
////                        invoice.put("client", client);
////                        invoice.saveInBackground(new SaveCallback() {
////                            @Override
////                            public void done(ParseException e) {
////                                if (e == null) {
////                                    fragmentHolder.myToast("invoice is add success");
////
////                                    fragmentHolder.dismissDialog();
////
////
////                                }
////                            }
////                        });
//
//                                fragmentHolder.saveInvoice(invoice);
//                                fragmentHolder.setFile(file);
//
//                            } catch (Exception ignored) {}
//                        }
//                    }).start();
//                }
//            });
//        }
//    }

//    private void createSuccessDialog(String textMessage) {
//        if ( getActivity() != null ) {
//            final Dialog dialog = new Dialog(getActivity(), R.style.Base_Theme_AppCompat_Dialog);
//            dialog.setContentView(R.layout.dialog_approuve_user);
//
//            TextView retake   = (TextView) dialog.getWindow().findViewById(R.id.dialog_approuve_retake);
//            TextView ok       = (TextView) dialog.getWindow().findViewById(R.id.dialog_approuve_ok);
//            TextView message  = (TextView) dialog.getWindow().findViewById(R.id.dialog_approuve_tv);
//
//            message.setText(String.format("%s", getString(R.string.invoice_success_sent)));
//
//            retake.setVisibility(View.GONE);
//
//            ok.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    fragmentHolder.setNavItem(2);
//                    dialog.dismiss();
//                }
//            });
//
//            dialog.show();
//        }
//    }

    @Override
    public void onAutoFocus(boolean paramBoolean, Camera paramCamera) {
        if (paramBoolean && capture) {
            // если удалось сфокусироваться, делаем снимок
            paramCamera.takePicture(null, null, null, this);
        }
    }

    @Override
    public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera) {
        // здесь можно обрабатывать изображение, показываемое в preview
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

//        String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
//                phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
//            phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }



}