package eu.taxfree4u.client.createTrip;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.interfaces.CreateTripInterface;
import eu.taxfree4u.client.model.Country;
import eu.taxfree4u.client.model.TripObj;
import eu.taxfree4u.client.model.User;
import eu.taxfree4u.client.tools.AppDelegate;

public class RegistrationStep2Fragment extends Fragment {

    public static final String TAG = "RegistrationStep1Fragment";

    private CreateTripInterface fragmentHolder;
    private View                rootView;
    private TextView            nextButton;
    private EditText            phoneField, addressField, postcodeField, cityField, countryField;
    private ArrayList<Country>  countryArrayList;
    private User                user;

    public static RegistrationStep2Fragment newInstance() {
        return new RegistrationStep2Fragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentHolder = (CreateTripInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fr_reg_step2, container, false);

        countryArrayList = AppDelegate.getInstance().getCountries();
        initialize();
        user = AppDelegate.getInstance().getUser();

//        countryField.setInputType(InputType.TYPE_NULL);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( fillTrackObj() ) {
                    fragmentHolder.callRegisterStep3();
                }
            }
        });

        countryField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countryField.setText("");
                fragmentHolder.callCountyFragment(TAG);
            }
        });

        phoneField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = phoneField.getText().toString();
                int textLength = phoneField.getText().length();

                if(text.endsWith("+"))
                    return;

                if(text.endsWith(" "))
                    return;

                if( textLength == 1 ) {
                    phoneField.setText(new StringBuilder(text).insert(text.length()-1, "+").toString());
                    phoneField.setSelection(phoneField.getText().length());
                }

            }
        });

        return rootView;
    }

    private boolean fillTrackObj() {
        String phone = phoneField.getText().toString();

        phone = phone.replace(" " , "");
        phone = phone.replace("+" , "");
        phone = phone.replace("(" , "");
        phone = phone.replace(")" , "");

        if ( phone.length() >= 9 ) {
            user.phone = phone;
        } else {
            fragmentHolder.myToast(getString(R.string._phone_error));
            return false;
        }

        if ( countryField.getText().toString().length() < 1 ) {
            fragmentHolder.myToast(getString(R.string._country_error));
            return false;
        }

        if ( cityField.getText().toString().length() > 0 ) {
            user.city = cityField.getText().toString();
        } else {
            fragmentHolder.myToast(getString(R.string._city_error));
            return false;
        }

        user.post_code = postcodeField.getText().toString();

        if ( addressField.getText().toString().length() > 0 ) {
            user.address = addressField.getText().toString();
        } else {
            fragmentHolder.myToast(getString(R.string._address_error));
            return false;
        }

        AppDelegate.getInstance().saveUser(user);

        fragmentHolder.saveUser();

        return true;
    }

    private void initialize() {
        nextButton      = (TextView) rootView.findViewById(R.id.reg_step2_next);

        phoneField      = (EditText) rootView.findViewById(R.id.reg_step2_phone);
        countryField    = (EditText) rootView.findViewById(R.id.reg_step2_address_country);
        cityField       = (EditText) rootView.findViewById(R.id.reg_step2_address_city);
        postcodeField   = (EditText) rootView.findViewById(R.id.reg_step2_address_postcode);
        addressField    = (EditText) rootView.findViewById(R.id.reg_step2_address_);
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( fragmentHolder == null ) {
            fragmentHolder = (CreateTripInterface) getActivity();
        }

        if ( AppDelegate.getInstance().getUser().country != 0 ) {
            countryField.setText(getCountryName((int) AppDelegate.getInstance().getUser().country));
        }
    }

    @Override
    public void onPause() {
        AppDelegate.getInstance().setCurrentTrip(fragmentHolder.getTripObj());

        fragmentHolder.hideKeyboard();
        super.onPause();
    }

    private String getCountryName(int id) {
        int size = countryArrayList.size();

        for ( int i = 0; i < size; i++ ) {
            if ( countryArrayList.get(i).id == id) {
                return countryArrayList.get(i).name;
            }
        }
        return "";
    }

}
