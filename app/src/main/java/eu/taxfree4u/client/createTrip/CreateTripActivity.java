package eu.taxfree4u.client.createTrip;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.activities.AddNewCardActivity;
import eu.taxfree4u.client.fragments.CardsFragment;
import eu.taxfree4u.client.fragments.CountriesChoicerFragment;
import eu.taxfree4u.client.fragments.NewTripFragment;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.interfaces.CreateTripInterface;
import eu.taxfree4u.client.interfaces.ServiceApi;
import eu.taxfree4u.client.model.Card;
import eu.taxfree4u.client.model.TripObj;
import eu.taxfree4u.client.model.User;
import eu.taxfree4u.client.tools.AppDelegate;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateTripActivity extends AppCompatActivity implements CreateTripInterface {

    private static final String TAG = "CreateTripActivity";

    private static final int REQUEST_IMAGE_CAPTURE  = 1;
    private static final int REQUEST_PHOTOS         = 2;
    private static final int REQUEST_IMAGE_PASSPORT = 3;
    private static final int REQUEST_PASSPORT       = 4;
    private static final int MY_SCAN_REQUEST_CODE   = 16;

    private Fragment    regStep1, regStep2, regStep3, passportPhoto, currentFragment, cardFrg;
    private Bitmap      imageBill, imagePassport;
    private boolean     bytesOfCheck;
    private boolean     bytesOfPassport;
    private TripObj     tripObj;

    private ProgressDialog      progressDialog;
    private FragmentInterface   regStep1Holder;
    private boolean             doubleBackToExitPressedOnce = false;
    private boolean             rotate;
    private BottomSheetLayout   bottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_trip);
        bottomSheet     = (BottomSheetLayout) findViewById(R.id.bottomsheet);
        File mFile = new File(CreateTripActivity.this.getExternalFilesDir(null), "passportPhoto.jpg");

        if (mFile.exists()) {
            if (mFile.delete()) {
                System.out.println("file Deleted ");
            } else {
                System.out.println("file not Deleted ");
            }
        }

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            cameraFragment = new Camera2BasicFragment();
//        } else {

        tripObj         = new TripObj();
//        cameraFragment  = new CameraBasicFragment();
        regStep1        = new RegistrationStep1Fragment();
        regStep2        = new RegistrationStep2Fragment();
        regStep3        = new RegistrationStep3Fragment();
//        passportPhoto   = new CameraPassportFragment();

        cardFrg         = new CardsFragment();


        regStep1Holder  = (FragmentInterface) regStep1;


        if ( getIntent().getIntExtra("source", 1) == 2 ) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, REQUEST_PHOTOS);
        } else {
            dispatchTakePictureIntent();
        }

//        if ( getFragmentManager().findFragmentById(R.id.rv_container) == null ) {
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .add(R.id.rv_container, cameraFragment, CameraBasicFragment.TAG)
//                    .commit();
//        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File mFile = new File(this.getExternalFilesDir(null), "checkPhoto.jpg");
            Uri imageUri = Uri.fromFile(mFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "requestCode:" + requestCode);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            File mFile = new File(this.getExternalFilesDir(null), "checkPhoto.jpg");

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            Bitmap bitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath(), options);

            savePhotoCheck(bitmap);

            if ( AppDelegate.getInstance().getHasPassport()) {
                callRegisterStep3();
            } else {
                callRegisterStep1();
            }
        } else if ( requestCode == REQUEST_PHOTOS && resultCode == RESULT_OK ) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = this.getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                Bitmap bmp = BitmapFactory.decodeFile(picturePath);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                savePhotoCheck(bmp);
            }
            if ( AppDelegate.getInstance().getHasPassport()) {
                callRegisterStep3();
            } else {
                callRegisterStep1();
            }
        } else if (requestCode == REQUEST_IMAGE_PASSPORT && resultCode == RESULT_OK) {
            File mFile = new File(this.getExternalFilesDir(null), "passportPhoto.jpg");

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            Bitmap bitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath(), options);

            savePhotoPassport(bitmap);
        } else if ( requestCode == REQUEST_PASSPORT && resultCode == RESULT_OK ) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = this.getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                Bitmap bmp = BitmapFactory.decodeFile(picturePath);
//
//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                bmp.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                savePhotoPassport(bmp);
            }
        } else if ( requestCode == MY_SCAN_REQUEST_CODE ) {
            String resultDisplayStr;
            Log.d(TAG, "123card.number:  FAILL000" );
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                Log.d(TAG, "123card.number: " + resultDisplayStr);

                Intent intent = new Intent(this, AddNewCardActivity.class);

                intent.putExtra("cardNumber", scanResult.cardNumber);

                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";


                    Log.d(TAG, "123card.number: " + resultDisplayStr);
                    intent.putExtra("expiryMonth", scanResult.expiryMonth);
                    intent.putExtra("expiryYear", scanResult.expiryYear);
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }

                startActivity(intent);
            } else {
                resultDisplayStr = "Scan was canceled.";

            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
        }
    }

    @Override
    public void savePhotoCheck(final Bitmap imageBitmap) {
        this.bytesOfCheck = true;

        if ( imageBitmap != null ) {
            new Thread(new Runnable() {
                public void run() {

                    Bitmap bmp = imageBitmap;

                    bmp = scaleBmp(bmp);

                    if ( bmp.getHeight() < bmp.getWidth() ) {
                        bmp = rotateImage(90, bmp);
                    }

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.JPEG, 70, stream);

                    imageBill = bmp;

                    try {
                        File mFile = new File(CreateTripActivity.this.getExternalFilesDir(null), "checkPhoto.jpg");

                        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(mFile));
                        bos.write(stream.toByteArray());
                        bos.flush();
                        bos.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    @Override
    public void savePhotoPassport(final Bitmap imageBitmap) {
        this.bytesOfPassport = true;

        if ( imageBitmap != null  ) {
            new Thread(new Runnable() {
                public void run() {
                    Bitmap bmp = imageBitmap;

                    bmp = scaleBmp(bmp);

                    if ( bmp.getHeight() < bmp.getWidth() ) {
                        bmp = rotateImage(90, bmp);
                    }


                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.JPEG, 90, stream);

                    try {
                        File mFile = new File(CreateTripActivity.this.getExternalFilesDir(null), "passportPhoto.jpg");

                        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(mFile));
                        bos.write(stream.toByteArray());
                        bos.flush();
                        bos.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    imagePassport = bmp;
                }

                @Override
                protected void finalize() throws Throwable {


                    Log.d(TAG, "((FragmentInterface)regStep1).doWork();");
                    super.finalize();
                }
            }).start();
        }
    }

    private Bitmap scaleBmp(Bitmap bmp) {
        final int maxSize = 1400;
        int outWidth;
        int outHeight;
        int inWidth = bmp.getWidth();
        int inHeight = bmp.getHeight();

        if(inWidth < inHeight){
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }

        return Bitmap.createScaledBitmap(bmp, outWidth, outHeight, false);
    }

    public Bitmap rotateImage(int angle, Bitmap bitmapSrc) {
        Matrix matrix = new Matrix();

        if ( rotate ) {
            matrix.postRotate(angle);
        }

        return Bitmap.createBitmap(bitmapSrc, 0, 0,
                bitmapSrc.getWidth(), bitmapSrc.getHeight(), matrix, true);
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

//        String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
//                phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
//            phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }

    @Override
    public void callRegisterStep1() {
        getSupportFragmentManager()
                .beginTransaction()
//               .setCustomAnimations(R.animator.hyperspace_out, R.animator.hyperspace_in,
//                        R.animator.slide_in_left, R.animator.slide_in_right)
                .replace(R.id.rv_container, regStep1, RegistrationStep1Fragment.TAG)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    @Override
    public void callRegisterStep2() {
        getSupportFragmentManager()
                .beginTransaction()
// .setCustomAnimations(R.animator.hyperspace_out, R.animator.hyperspace_in,
//                        R.animator.slide_in_left, R.animator.slide_in_right)
                .replace(R.id.rv_container, regStep2, RegistrationStep2Fragment.TAG)
                .addToBackStack(null)
                .commit();
        currentFragment = regStep2;

        if ( new File(CreateTripActivity.this.getExternalFilesDir(null), "passportPhoto.jpg").exists()) {
            loadPassportPhoto();
        }
    }

    @Override
    public void callRegisterStep3() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rv_container, regStep3, RegistrationStep3Fragment.TAG)
                .addToBackStack(null)
                .commitAllowingStateLoss();

        currentFragment = regStep3;
    }

    @Override
    public void callPassportPhoto() {
//        getSupportFragmentManager()
//                .beginTransaction()
////                .setCustomAnimations(R.animator.hyperspace_out, R.animator.hyperspace_in,
////                        R.animator.slide_in_left, R.animator.slide_in_right)
//                .replace(R.id.rv_container, passportPhoto, CameraPassportFragment.TAG)
//                .addToBackStack(null)
//                .commit();
        bottomSheet.showWithSheetView(LayoutInflater.from(this).inflate(R.layout.my_sheet_layout, bottomSheet, false));
        TextView txtTakePhoto   = (TextView) bottomSheet.findViewById(R.id.txtTakePhoto);
        TextView txtGallery     = (TextView) bottomSheet.findViewById(R.id.txtGallery);

        txtTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

                    File mFile = new File(CreateTripActivity.this.getExternalFilesDir(null), "passportPhoto.jpg");
                    Uri imageUri = Uri.fromFile(mFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_PASSPORT);
                }
                bottomSheet.dismissSheet();
            }
        });

        txtGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, REQUEST_PASSPORT);
                bottomSheet.dismissSheet();
            }
        });


    }

    @Override
    public void callCards() {
        try {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.rv_container, cardFrg, CardsFragment.TAG)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        } catch ( IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Bitmap getCheckPhoto() {
        return imageBill;
    }

    @Override
    public Bitmap getPassportPhoto() {
        return imagePassport;
    }

    @Override
    public void startCardIO() {

        Intent scanIntent = new Intent(this, CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false); // default: true

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.

        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    public boolean isPassportPhoto() {
        if ( bytesOfPassport ) {
            return true;
        } else {
            File passportImage =  new File(CreateTripActivity.this.getExternalFilesDir(null), "passportPhoto.jpg");
            if ( passportImage.exists() ) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean isCheckPhoto() {
        return bytesOfCheck;
    }

    @Override
    public void myToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void createTrip() {
        showMyProgressDialog("loading...");

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.addTrip(Locale.getDefault().getLanguage(), AppDelegate.getInstance().getToken(),
                tripObj.date_start, tripObj.date_end,
                tripObj.departure_country, tripObj.destination_country,
                tripObj.flightNumber, tripObj.city);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG,"response.body(): " + response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if ( jsonObject.optInt("status") == 0 ) {
                            JSONObject jsonObject1 = new JSONObject(jsonObject.optString("data"));

                            AppDelegate.getInstance().setCurrentTrip(tripObj);
                            AppDelegate.getInstance().setCurrentTripId(jsonObject1.optInt("trip_id"));

                            if ( new File(CreateTripActivity.this.getExternalFilesDir(null), "checkPhoto.jpg").exists()) {
                                loadCheckPhoto();
                            }

                            finish();
                        } else {
                            JSONObject jsonData = new JSONObject(jsonObject.optString("error"));
                            myToast(jsonData.optString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        myToast(jsonObject1.optString("message"));

                        Log.d(TAG, "response error code:" + response.code());
                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
                dismissDialog();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());

                dismissDialog();
            }
        });

    }

    @Override
    public TripObj getTripObj() {
        return tripObj;
    }

    @Override
    public void setTripObj(TripObj tripObj) {
        this.tripObj = tripObj;

    }

    @Override
    public void callCountyFragment(String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rv_container, CountriesChoicerFragment.newInstance(tag), CountriesChoicerFragment.TAG)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    @Override
    public void callPreviousFragment() {
        try {
            getSupportFragmentManager()
                    .beginTransaction()
//                    .setCustomAnimations(R.animator.hyperspace_out, R.animator.hyperspace_in,
//                            R.animator.slide_in_left, R.animator.slide_in_right)
                    .replace(R.id.rv_container, currentFragment, currentFragment.getTag())
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        } catch (NullPointerException e) {
            e.printStackTrace();
            getSupportFragmentManager()
                    .beginTransaction()
//                    .setCustomAnimations(R.animator.hyperspace_out, R.animator.hyperspace_in,
//                            R.animator.slide_in_left, R.animator.slide_in_right)
                    .replace(R.id.rv_container, regStep1, RegistrationStep1Fragment.TAG)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }
    }

    private void loadPassportPhoto() {
        File passportImage =  new File(CreateTripActivity.this.getExternalFilesDir(null), "passportPhoto.jpg");

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), passportImage);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("passport", "passport", requestFile);

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(200, TimeUnit.SECONDS)
                .readTimeout(25, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.uploadPassportPhoto(AppDelegate.getInstance().getToken(), body);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "12334: " + response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if ( jsonObject.optInt("status") == 0 ) {
                            myToast(getString(R.string.passport_upload_success));

                            AppDelegate.getInstance().setHasPassport(true);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        myToast(jsonObject1.optString("message"));

                        Log.d(TAG, "response error code:" + response.code());
                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    private void loadCheckPhoto() {
        File checkImage =  new File(CreateTripActivity.this.getExternalFilesDir(null), "checkPhoto.jpg");

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), checkImage);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("receipt_file", "receipt_file", requestFile);

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(200, TimeUnit.SECONDS)
                .readTimeout(25, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.uploadCheckPhoto(AppDelegate.getInstance().getToken(), body);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "12334: " + response.body().toString());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if ( jsonObject.optInt("status") == 0 ) {
                            myToast(getString(R.string.check_add_success));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(errorConvector(response));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.optString("error"));

                        myToast(jsonObject1.optString("message"));

                        Log.d(TAG, "response error code:" + response.code());
                        Log.d(TAG, "response error:" + jsonObject1.optString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error:" + errorConvector(response));
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    private String errorConvector(Response<JsonObject> response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    protected void showMyProgressDialog(String msg) {
        if ( progressDialog == null || !progressDialog.isShowing() ) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(msg);
            try {
                progressDialog.show();
            } catch (Exception e) {
            }
        }
    }

    protected void dismissDialog() {
        if ( progressDialog != null && progressDialog.isShowing() ) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {}
        }
    }

    @Override
    public void saveUser() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.updateUser(AppDelegate.getInstance().getToken(),
                prepareUserParams());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response profile view:" + response.body());
                    try {

                        JSONObject status = new JSONObject(response.body().toString());
                        if ( status.optInt("status") == 0 ) {
                            myToast(getString(R.string.edit_user_success));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response body null: " + errorConvector(response));
                    myToast(errorConvector(response));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    @Override
    public void rotateReceipt(boolean rotate) {
        this.rotate = rotate;
    }

    private Map<String, String> prepareUserParams() {
       User currentUser = AppDelegate.getInstance().getUser();
        Map<String, String> userParams = new HashMap<>();

        if ( currentUser.phone.length() >= 7) {
            userParams.put("phone", currentUser.phone);
        }
        if ( currentUser.country > 0) {
            userParams.put("country", String.valueOf(currentUser.country));
        }
        if ( currentUser.city.length() >= 2) {
            userParams.put("city", currentUser.city);
        }
        if ( currentUser.address.length() >= 3) {
            userParams.put("address", currentUser.address);
        }
        if ( currentUser.post_code.length() > 0) {
            userParams.put("post_code", String.valueOf(currentUser.post_code));
        }

        return userParams;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if ( getSupportFragmentManager().getBackStackEntryCount() == 0 ) {
            this.finish();
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException ignored) {
        }
    }

    @Override
    public void loadCards() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.getCards(AppDelegate.getInstance().getToken());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response.body() cards: " + response.body());
                    try {
                        JSONObject status = new JSONObject(response.body().toString());

                        if (status.optInt("status") == 0) {
                            if (status.optJSONArray("data") != null) {
                                parseCards(status.optJSONArray("data"));
                            }
                        }
//                            if (profileFragment.isResumed()) {
//                                profileFragment.doWork();
//                            }
//
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response body null: " + errorConvector(response));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    private void parseCards(JSONArray data) {
        int size = data.length();
        ArrayList<Card> cards = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            Card card = new Card();
            try {
                JSONObject jsonObject = new JSONObject(data.get(i).toString());

                card.id = jsonObject.optInt("id");
                card.expire_month = jsonObject.optString("expire_month");
                card.expire_year = jsonObject.optString("expire_year");
                card.is_default = jsonObject.optBoolean("is_default");
                card.number = jsonObject.optString("number");
                card.name = jsonObject.optString("name");

                cards.add(card);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        AppDelegate.getInstance().setCards(cards);
        ((FragmentInterface)cardFrg).saveChanges();

        if ( AppDelegate.getInstance().isNeedUpdate()) {
            AppDelegate.getInstance().setNeedUpdate(false);
        }
    }

    @Override
    public void loadCardIsDefault(int id, boolean state) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceApi serviceApi = retrofit.create(ServiceApi.class);

        Call<JsonObject> call = serviceApi.editCardDefault(AppDelegate.getInstance().getToken(), id, state);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.d(TAG, "response.body() cards: " + response.body());
                    try {
                        JSONObject status = new JSONObject(response.body().toString());

                        if (status.optInt("status") == 0) {
                            myToast("card set as default");
                        }
//                            if (profileFragment.isResumed()) {
//                                profileFragment.doWork();
//                            }
//
                    } catch (JSONException | NullPointerException e) {
                        e.printStackTrace();
                        Log.d(TAG, "response error: " + e.getMessage());
                    }
                } else {
                    Log.d(TAG, "response body null: " + errorConvector(response));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "response error: " + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

//        if ( AppDelegate.getInstance().isNeedUpdate()){
//            callCards();
//        }
//
// else if ( bytesOfCheck ) {
//            if ( AppDelegate.getInstance().getHasPassport()) {
//                callRegisterStep3();
//            } else {
//                callRegisterStep1();
//            }
//        }

//        if ( getSupportFragmentManager().getBackStackEntryCount() == 0 ) {
//            super.onBackPressed();
//        }
    }
}
