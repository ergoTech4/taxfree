package eu.taxfree4u.client.createTrip;

import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.Locale;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.interfaces.CreateTripInterface;

public class RegistrationStep1Fragment  extends Fragment implements FragmentInterface {

    public static final String TAG = "RegistrationStep1Fragment";

    private View                    rootView;
    private ImageView               capturePhoto, showPhotoImage;
    private TextView                nextButton;

    private ImageLoader             imageLoader;
    private DisplayImageOptions     options;
    private CreateTripInterface     fragmentHolder;

    public static RegistrationStep1Fragment newInstance() {
        return new RegistrationStep1Fragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentHolder = (CreateTripInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fr_reg_step1, container, false);

        initialize();

        capturePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callPassportPhoto();
            }
        });

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                //.showImageForEmptyUri(R.drawable.ic_empty_img)
                //.showImageOnFail(R.drawable.ic_empty_img)
                .resetViewBeforeLoading(true)
                .cacheOnDisc(false)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( fragmentHolder.isPassportPhoto() ) {
                    fragmentHolder.callRegisterStep2();
                } else {
                    fragmentHolder.myToast(getString(R.string.upload_passport));
                }
            }
        });

        return rootView;
    }


    private void initialize() {
        showPhotoImage      = (ImageView) rootView.findViewById(R.id.reg_step1_image_photo);
        capturePhoto        = (ImageView) rootView.findViewById(R.id.reg_step1_image);
        nextButton          = (TextView) rootView.findViewById(R.id.reg_step1_next);
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( fragmentHolder == null ) {
            fragmentHolder = (CreateTripInterface) getActivity();
        }

//        fragmentHolder.savePhotoPassport(null);

        new CountDownTimer(2000, 2000 ){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                showPhotoImage.setVisibility(View.VISIBLE);
                imageLoader.displayImage("file:///"  + getActivity().getExternalFilesDir(null) + "/passportPhoto.jpg" , showPhotoImage, options);
            }
        }.start();

    }

    @Override
    public void onPause() {
        fragmentHolder.hideKeyboard();
        super.onPause();
    }



    @Override
    public void doWork() {
        showPhotoImage.setVisibility(View.VISIBLE);

        imageLoader.displayImage("file:///"  + getActivity().getExternalFilesDir(null) + "/passportPhoto.jpg" , showPhotoImage, options);
    }

    @Override
    public void saveChanges() {

    }

    @Override
    public void showImage(int position) {

    }

    @Override
    public void retakeReceipt(int id) {

    }

    @Override
    public void deleteReceipt(int id) {

    }
}
