package eu.taxfree4u.client.createTrip;

import android.app.DatePickerDialog;
import android.support.v4.app.Fragment;
import android.content.Context;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.interfaces.CreateTripInterface;
import eu.taxfree4u.client.model.Card;
import eu.taxfree4u.client.model.Country;
import eu.taxfree4u.client.model.TripObj;
import eu.taxfree4u.client.tools.AppDelegate;

public class RegistrationStep3Fragment extends Fragment {

    public static final String TAG          = "RegistrationStep1Fragment";
    public static final String TAG_DESTINATION = "RegistrationStep1Fragment_enter";
    public static final String TAG_DEPARTURE = "RegistrationStep1Fragment_leave";
    public static final long   SECOND_DIVIDER = 1000;

    private CreateTripInterface fragmentHolder;
    private View                rootView;
    private TextView            create, step3;
    private EditText            from, to, enterCountry, city, flightNumber,
                                leavingCountry, paymentCard;

    private static final long SECONDS_IN_YEAR = 31536000;

    private ArrayList<Card>     cards = new ArrayList<>();

    private TripObj             currentTrip;
    private ArrayList<Country>  countryArrayList;

    private Calendar    cal;
    private int         day;
    private int         month;
    private int         year;


    public static RegistrationStep3Fragment newInstance() {
        return new RegistrationStep3Fragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentHolder = (CreateTripInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fr_reg_step3, container, false);

        initialize();

//        currentTrip = fragmentHolder.getTripObj();

        if ( AppDelegate.getInstance().getHasPassport()) {
            step3.setText(getString(R.string._create_trip));
        }

        cal     = Calendar.getInstance();
        day     = cal.get(Calendar.DAY_OF_MONTH);
        month   = cal.get(Calendar.MONTH);
        year    = cal.get(Calendar.YEAR);

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( currentTrip.date_start - currentTrip.date_end  <= 86000 ) {
                    if (fillTrackObj()) {
                        fragmentHolder.createTrip();
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.date_error), Toast.LENGTH_SHORT).show();

                }
            }
        });

        from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateDialogFrom(from);
            }
        });

        to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateDialogTo(to);
            }
        });

        enterCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callCountyFragment(TAG_DESTINATION);
            }
        });


        leavingCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callCountyFragment(TAG_DEPARTURE);
            }
        });

        paymentCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callCards();
            }
        });

        return rootView;
    }

    public void dateDialogTo(final EditText editText) {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                editText.setText(String.format("%s / %s / %s", String.format("%02d", dayOfMonth),
                                String.format("%02d", monthOfYear + 1), String.format("%02d", year)));

                Calendar calTo =  Calendar.getInstance();
                calTo.set(year, monthOfYear, dayOfMonth);

                currentTrip.date_end = calTo.getTimeInMillis() / SECOND_DIVIDER;

                AppDelegate.getInstance().setCurrentTrip(currentTrip);

                Log.d(TAG, "date end: " + calTo.getTimeInMillis());
            }};

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), listener, year, month, day);

        if ( currentTrip.date_start == 0 ) {
            dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + SECONDS_IN_YEAR * 1000);
            dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - SECONDS_IN_YEAR * 1000);
        } else {
            dpDialog.getDatePicker().setMaxDate((currentTrip.date_start * 1000) + SECONDS_IN_YEAR * 1000);
            dpDialog.getDatePicker().setMinDate((currentTrip.date_start * 1000) - SECONDS_IN_YEAR * 1000);
        }

        dpDialog.show();
    }

    public void dateDialogFrom(final EditText editText) {
        DatePickerDialog.OnDateSetListener listener=new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                editText.setText(String.format("%s / %s / %s", String.format("%02d", dayOfMonth),
                        String.format("%02d", monthOfYear + 1), String.format("%02d", year)));

                Calendar calFrom = Calendar.getInstance();
                calFrom.set(year, monthOfYear, dayOfMonth);

                currentTrip.date_start = calFrom.getTimeInMillis() / SECOND_DIVIDER;

                AppDelegate.getInstance().setCurrentTrip(currentTrip);
            }};

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), listener, year, month, day);

        if ( currentTrip.date_end == 0 ) {
            dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + SECONDS_IN_YEAR * 1000);
            dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - SECONDS_IN_YEAR * 1000);
        } else {
            dpDialog.getDatePicker().setMaxDate(currentTrip.date_end * 1000 + SECONDS_IN_YEAR * 1000);
            dpDialog.getDatePicker().setMinDate(currentTrip.date_end * 1000 - SECONDS_IN_YEAR * 1000);

        }

        dpDialog.show();
    }


    private void initialize() {
        create              = (TextView) rootView.findViewById(R.id.reg_step3_create);
        step3               = (TextView) rootView.findViewById(R.id.reg_step3_tv);

        from                = (EditText) rootView.findViewById(R.id.reg_step3_from);
        to                  = (EditText) rootView.findViewById(R.id.reg_step3_to);
        enterCountry        = (EditText) rootView.findViewById(R.id.reg_step3_final_destination_country);
        city                = (EditText) rootView.findViewById(R.id.reg_step3_city);
        flightNumber        = (EditText) rootView.findViewById(R.id.reg_step3_flight_number);
        leavingCountry      = (EditText) rootView.findViewById(R.id.reg_step3_departure_country);
        paymentCard         = (EditText) rootView.findViewById(R.id.reg_step3_payment);
    }

    private boolean fillTrackObj() {
        if ( from.getText().length() <= 0 ) {
            fragmentHolder.myToast(getString(R.string._from_error));
            return false;
        }

        if ( to.getText().length() <= 0 ) {
            fragmentHolder.myToast(getString(R.string._country_error));
            return false;
        }

        if ( enterCountry.getText().length() <= 0) {
            fragmentHolder.myToast(getString(R.string.desitaion_country_error));
            return false;
        }

        if ( city.getText().length() > 0 ) {

            currentTrip.city = city.getText().toString();

        } else {
            fragmentHolder.myToast(getString(R.string._city_error));
            return false;
        }

        if (leavingCountry.getText().length() <= 0) {
            fragmentHolder.myToast(getString(R.string.departure_country_incorrect));
            return false;
        }

        currentTrip.flightNumber = flightNumber.getText().toString();

        fragmentHolder.setTripObj(currentTrip);
        AppDelegate.getInstance().setCurrentTrip(currentTrip);

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( fragmentHolder == null ) {
            fragmentHolder = (CreateTripInterface) getActivity();
        }

        countryArrayList    = AppDelegate.getInstance().getCountries();
        currentTrip         = AppDelegate.getInstance().getCurrentTrip();

        if ( currentTrip.departure_country !=  0 ) {
            leavingCountry.setText(getCountryName(currentTrip.departure_country));
        }

        if ( currentTrip.destination_country !=  0 ) {
            enterCountry.setText(getCountryName(currentTrip.destination_country));
        } else if ( AppDelegate.getInstance().getUser().country != 0 ) {
            enterCountry.setText(getCountryName((int) (long)AppDelegate.getInstance().getUser().country));
            currentTrip.destination_country = (int) (long)AppDelegate.getInstance().getUser().country;
            AppDelegate.getInstance().setCurrentTrip(currentTrip);
        }

        cards = AppDelegate.getInstance().getCards();

        for ( int i = 0; i < cards.size(); i++ ) {
            if ( cards.get(i).is_default ) {
                String cardNumberTemp = cards.get(i).number.substring(0, 4) + "-" + "****"
                        + "-" + "****" + "-" + cards.get(i).number.substring(12, 16);

                paymentCard.setText(cardNumberTemp);
            }
        }

    }

    @Override
    public void onPause() {
        fragmentHolder.hideKeyboard();

        AppDelegate.getInstance().setCurrentTrip(currentTrip);
        super.onPause();
    }

    private String getCountryName(int id) {
        int size = countryArrayList.size();

        for ( int i = 0; i < size; i++ ) {
            if ( countryArrayList.get(i).id == id) {
                return countryArrayList.get(i).name;
            }
        }
        return "";
    }

}
