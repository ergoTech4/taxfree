package eu.taxfree4u.client.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.concurrent.RejectedExecutionException;

import eu.taxfree4u.client.model.Card;
import eu.taxfree4u.client.model.Country;
import eu.taxfree4u.client.model.Receipt;
import eu.taxfree4u.client.model.TripObj;
import eu.taxfree4u.client.model.User;

public class AppDelegate {
    private static final String TAG                     = "Tools.AppDelegate";
    private static final String PREFERENCES_FILENAME    = "TaxFree4u";
    private static final String TOKEN                   = "access_token";
    private static final String TRIP_ID                 = "trip_id";
    private static final String COUNTRIES               = "countries";

    private static final String ID                      = "user_id";
    private static final String EMAIL                   = "email";
    private static final String PHONE_COUNTRY           = "phone_country";
    private static final String PHONE                   = "phone";
    private static final String FULL_NAME               = "full_name";
    private static final String COUNTRY                 = "country";
    private static final String CITIZEN                 = "citizen";
    private static final String PASSPORT_DATE           = "passport_date";
    private static final String PASSPORT_NUMBER         = "passport_number";
    private static final String PASSPORT_SEX            = "passport_sex";
    private static final String PASSPORT_COUNTRY        = "passport_country";
    private static final String CITY                    = "city";
    private static final String ADDRESS                 = "address";
    private static final String HAS_PASSPORT            = "has_passport";
    private static final String BIRTH_DATE              = "passport_birth";
    private static final String POST_CODE               = "post_code";
    private static final String IDENTITY                = "identity";

    private static AppDelegate  instance;
    private static Context      context;
    private static User         currentUser             = new User();
    private TripObj             currentTrip;
    private ArrayList<Country>  countries;
    private Object              countriesJson;
    private boolean             hasPassport;
    private boolean             needUpdate;
    private ArrayList<Card>     cards;
    private String              totalRefund = "0";
    private int                 notificationCounter = 0;
    private int                 firstLogin = 0;
    private String              totalRefundReceipts;
    private String              userId;
    private boolean             firsLogin;
    private int                 activity = 0;
    private ArrayList<Receipt> receipsList;


    private AppDelegate() {}

    public static void create(Context context) {
        if ( AppDelegate.instance == null ) {
            AppDelegate.instance    = new AppDelegate();
            AppDelegate.context     = context;
        } else {
            throw new RejectedExecutionException(AppDelegate.class.getName() + " cannot be created again.");
        }
    }

    public static AppDelegate getInstance() {
        if ( AppDelegate.instance == null ) {
            throw new RejectedExecutionException(AppDelegate.class.getName() + " not init.");
        }

        return AppDelegate.instance;
    }

    public static SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(PREFERENCES_FILENAME, Context.MODE_PRIVATE);
    }

    public static Context getContext() {
        return context;
    }

    public String getToken(){
        return getSharedPreferences().getString(TOKEN, "");
    }

    public void setToken(String token) {
        getSharedPreferences().edit().putString(TOKEN, token).apply();
    }

    public Integer getCurrentTripId(){
        return getSharedPreferences().getInt(TRIP_ID, -1);
    }

    public void setCurrentTripId(int id) {
        getSharedPreferences().edit().putInt(TRIP_ID, id).apply();
    }

    public void saveUser(User user) {
        currentUser = user;

        getSharedPreferences().edit().putString(ID, user.id).apply();
        getSharedPreferences().edit().putString(EMAIL, user.email).apply();
        getSharedPreferences().edit().putLong(PHONE_COUNTRY, user.phone_country).apply();
        getSharedPreferences().edit().putString(PHONE, user.phone).apply();
        getSharedPreferences().edit().putString(FULL_NAME, user.full_name).apply();
        getSharedPreferences().edit().putLong(COUNTRY, user.country).apply();
        getSharedPreferences().edit().putInt(CITIZEN, user.passport_citizenship).apply();
        getSharedPreferences().edit().putLong(PASSPORT_DATE, user.passport_date).apply();
        getSharedPreferences().edit().putString(PASSPORT_NUMBER, user.passport_number).apply();
        getSharedPreferences().edit().putString(PASSPORT_SEX, user.passport_sex).apply();
        getSharedPreferences().edit().putInt(PASSPORT_COUNTRY, user.passport_country).apply();
        getSharedPreferences().edit().putString(CITY, user.city).apply();
        getSharedPreferences().edit().putString(ADDRESS, user.address).apply();
        getSharedPreferences().edit().putLong(BIRTH_DATE, user.passport_birthday).apply();
        getSharedPreferences().edit().putString(POST_CODE, user.post_code).apply();
        getSharedPreferences().edit().putString(IDENTITY, user.identity).apply();

    }

    public User getUser() {
        if ( currentUser == null ) {
            currentUser = new User();

            currentUser.id                  = getSharedPreferences().getString(ID, "");
            currentUser.email               = getSharedPreferences().getString(EMAIL, "");
            currentUser.phone_country       = getSharedPreferences().getLong(PHONE_COUNTRY, 1);
            currentUser.phone               = getSharedPreferences().getString(PHONE, "");
            currentUser.country             = getSharedPreferences().getInt(COUNTRY, 1);
            currentUser.full_name           = getSharedPreferences().getString(FULL_NAME, "");
            currentUser.passport_citizenship = getSharedPreferences().getInt(CITIZEN, 1);
            currentUser.passport_date       = getSharedPreferences().getLong(PASSPORT_DATE, 0);
            currentUser.passport_number     = getSharedPreferences().getString(PASSPORT_NUMBER, "");
            currentUser.passport_sex        = getSharedPreferences().getString(PASSPORT_SEX, "");
            currentUser.city                = getSharedPreferences().getString(CITY, "");
            currentUser.address             = getSharedPreferences().getString(ADDRESS, "");
            currentUser.passport_country    = getSharedPreferences().getInt(PASSPORT_COUNTRY, 1);
            currentUser.passport_birthday   = getSharedPreferences().getInt(BIRTH_DATE, 0);
            currentUser.post_code           = getSharedPreferences().getString(POST_CODE, "");
            currentUser.identity            = getSharedPreferences().getString(IDENTITY, "");
        }

        return currentUser;
    }

    public void setCurrentTrip(TripObj currentTrip) {
        this.currentTrip = currentTrip;
    }

    public TripObj getCurrentTrip() {
        if ( currentTrip == null ) {
            return new TripObj();
        } else {
            return currentTrip;
        }
    }

    public ArrayList<Country> getCountries() {
        if ( countries != null ) {
            ArrayList<Country> arrayList = new ArrayList<>();
            arrayList.addAll(countries);
            return arrayList;
        } else {
        return getCountriesJson();
        }
    }

    public void saveCountries(String countries){
        getSharedPreferences().edit().putString(COUNTRIES, countries).apply();
    }

    private ArrayList<Country> getCountriesJson() {
        countries = new ArrayList<>();

        try {
            JSONArray jsonCountries = new JSONArray(getSharedPreferences().getString(COUNTRIES, ""));

            int size = jsonCountries.length();

            for ( int i = 0; i < size; i++ ) {
                Country country = new Country();
                try {
                    JSONObject object = new JSONObject(jsonCountries.get(i).toString());

                    country.id          = object.optInt("id");
                    country.name        = object.optString("name");
                    country.iso_name    = object.optString("iso_name");
                    country.phone_code  = object.optString("phone_code");
                    country.is_europe   = object.optBoolean("is_europe");

                    countries.add(country);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return countries;
    }

    public void setHasPassport(boolean hasPassport) {
        getSharedPreferences().edit().putBoolean(HAS_PASSPORT, hasPassport).apply();
    }

    public boolean getHasPassport() {

        return getSharedPreferences().getBoolean(HAS_PASSPORT, false);
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public ArrayList<Card> getCards() {
        if ( cards == null ) {
            cards = new ArrayList<>();
        }

        return cards;
    }

    public boolean isNeedUpdate() {
        return needUpdate;
    }

    public void setNeedUpdate(boolean needUpdate) {
        this.needUpdate = needUpdate;
    }

    public void setTotalRefundVAT(String totalRefund) {
        getSharedPreferences().edit().putString("total_vat_refund", totalRefund).apply();
    }

    public String getTotalRefundVAT() {
        return getSharedPreferences().getString("total_vat_refund", "");
    }

    public void setNotificationCount(int count) {
        if ( count != 0 ) {
            this.notificationCounter += 1;
        } else {
            this.notificationCounter = 0;
        }
    }

    public int getNotificationCounter() {
        return notificationCounter;
    }

    public void setFirstLogin(int firstLogin) {
        this.firstLogin = firstLogin;
    }

    public int isFirstLogin() {
        return firstLogin;
    }

    public void saveFireBaseToken(String token) {
        getSharedPreferences().edit().putString("firebase_token", token).apply();
    }

    public String getFireBaseToken() {
        return getSharedPreferences().getString("firebase_token", "");
    }

    public void saveFireBasePushToken(String token) {
        getSharedPreferences().edit().putString("firebase_push_token", token).apply();
    }

    public String getFireBasePushToken() {
        return getSharedPreferences().getString("firebase_push_token", "");
    }


    public void setTotalRefundReceipts(String totalRefundReceipts) {
        getSharedPreferences().edit().putString("total_receipt_refund", totalRefundReceipts).apply();
    }

    public String getTotalRefundReceipts() {
        return getSharedPreferences().getString("total_receipt_refund", "");
    }

    public void logout() {
        currentUser = null;
        setToken("");
        getSharedPreferences().edit().clear().apply();
    }

    public void saveFireBaseRoom(int room_id) {
        getSharedPreferences().edit().putInt("room_id", room_id).apply();

    }

    public int getFireBaseRoom() {
        return getSharedPreferences().getInt("room_id", -1);
    }


    public String getUserId() {
        return getSharedPreferences().getString("firebase_user_id", "");
    }

    public void setUserId(String userId) {
        getSharedPreferences().edit().putString("firebase_user_id", replaceZero(userId)).apply();
    }

    private String replaceZero(String userId) {
        int temp = Integer.valueOf(userId);
        return String.valueOf(temp);
    }

    public boolean getNeedToLogin() {
        return getSharedPreferences().getBoolean("need_to_login", false);
    }

    public void setNeedToLogin(boolean firsLogin) {
        getSharedPreferences().edit().putBoolean("need_to_login", firsLogin).apply();
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public int isActivity() {
        return activity;
    }

    public void setReceipsList(ArrayList<Receipt> receipsList) {
        this.receipsList = receipsList;
    }

    public ArrayList<Receipt> getReceipsList() {
        if ( receipsList == null ) {
            receipsList = new ArrayList<>();
        }
        return receipsList;
    }
}

