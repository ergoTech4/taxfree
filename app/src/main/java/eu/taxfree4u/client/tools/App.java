package eu.taxfree4u.client.tools;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.firebase.client.Firebase;
import com.google.firebase.FirebaseApp;
import com.helpshift.Core;
import com.helpshift.support.Support;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;

import eu.taxfree4u.client.database.ConstDb;
//import eu.taxfree4u.client.services.gcm.RegistrationIntentService;
import io.fabric.sdk.android.Fabric;


public class App extends MultiDexApplication {
    private static final String TAG = "App";
    private SQLiteDatabase db;

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Firebase.setAndroidContext(this);
//        FirebaseApp.initializeApp(this);

        AppDelegate.create(this);
        Fabric.with(this, new Crashlytics());

        Core.init(Support.getInstance());

        Core.install(this, "91db69dff038aa189ede8175f1eb1086", "bank4you.helpshift.com",
                "bank4you_platform_20160729130327905-53756950e919e03");

//        Intent intent = new Intent(this, RegistrationIntentService.class);
//        startService(intent);

        File cacheDir = StorageUtils.getCacheDirectory(this);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)

                .threadPoolSize(5) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(20 * 1024 * 1024))
                .memoryCacheSize(20 * 1024 * 1024)
                .memoryCacheSizePercentage(18) // default
                .diskCache(new UnlimitedDiskCache(cacheDir)) // default
                .diskCacheSize(150 * 1024 * 1024)
                .diskCacheFileCount(200)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new CustomImageDownloader(this, 10 * 1000, 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .build();

        ImageLoader.getInstance().init(config);
    }

    public SQLiteDatabase getDb() {
        if ( db == null || !db.isOpen() ) {
            try {
                ConstDb.DatabaseHelper helper = new ConstDb.DatabaseHelper(this);
                db = helper.getWritableDatabase();

                Log.d(TAG, "BD open success");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return db;
    }

    public class CustomImageDownloader extends BaseImageDownloader {

        public CustomImageDownloader(Context context) {
            super(context);
        }

        public CustomImageDownloader(Context context, int connectTimeout, int readTimeout) {
            super(context, connectTimeout, readTimeout);
        }

        @Override
        protected HttpURLConnection createConnection(String url, Object extra) throws IOException {
            HttpURLConnection conn = super.createConnection(url, extra);
            Map<String, String> headers = (Map<String, String>) extra;
            if (headers != null) {
                for (Map.Entry<String, String> header : headers.entrySet()) {
                    conn.setRequestProperty(header.getKey(), header.getValue());
                }
            }
            return conn;
        }
    }
}
