package eu.taxfree4u.client.model;

public class VatForm {

    public String   client_refund;
    public int      id;
    public long     updated;
    public String   file;
    public String   file_thumb;
    public String   status;
    public int      error;
    public String   comment;

}
