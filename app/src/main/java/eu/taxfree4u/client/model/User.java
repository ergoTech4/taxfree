package eu.taxfree4u.client.model;

public class User {
    public String id;
    public String phone;
    public String full_name;
    public String email = "";
    public String passport_sex = "m";
    public String passport_number = "";
    public int    passport_citizenship;
    public long   phone_country;
    public long   country;
    public int    passport_country;
    public long   passport_date;
    public String post_code = "";
    public String city = "";
    public String address = "";
    public String passport_file;
    public long   passport_birthday;
    public String errorMessage = "";
    public String identity = "0";
}
