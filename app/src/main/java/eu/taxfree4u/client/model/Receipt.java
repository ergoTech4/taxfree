package eu.taxfree4u.client.model;

public class Receipt {
    public int id;
    public String name;
    public String file;
    public long create_time;
    public boolean editable;

    public String file_thumb;
    public String client_refund;
    public String comment;
    public String status;
    public int    country;
    public int    currency = 0;
}


//"status":"new","client_refund":"0.00","comment":null,"id":549,"trip":729,"editable":false,
// "create_time":1473239857,"country":1,"currency":null,"shop_date":null,"shop_name":null,
// "shop_city":null,"shop_address":null,"receipt_file":"https://taxfree4u.eu/uploads/check/20160907/8fb74ea0956b2a93fc7033196a0078f9837ab8d1.jpeg",
// "receipt_file_thumb":"https://taxfree4u.eu/uploads/check/20160907/8fb74ea0956b2a93fc7033196a0078f9837ab8d1_thumb.jpeg"