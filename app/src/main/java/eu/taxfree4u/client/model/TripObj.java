package eu.taxfree4u.client.model;

public class TripObj {

    public int      id;
    public long     date_start = 0;
    public long     date_end = 0;
    public int      destination_country;
    public int      departure_country;
    public int      transport_leaving = 1;
    public long     country;
    public String   race_number_enter = "1";
    public String   race_number_leaving = "1";
    public String   address;
    public String   phone;
    public String   city;
    public String   postCode;
    public String   from;
    public String   to;
    public String   flightNumber;
    public String   paymentCard;

}
