package eu.taxfree4u.client.model;

public class Country {
    public int      id;
    public String   name;
    public String   iso_name;
    public String   phone_code;
    public boolean  is_europe;

}
