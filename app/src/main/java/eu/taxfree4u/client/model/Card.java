package eu.taxfree4u.client.model;

public class Card {
    public int id               = 0;
    public String number        = "XXXX XXXX XXXX XXXX";
    public String expire_month  = "XX";
    public String expire_year   = "XX";

    public boolean is_default;
    public String name          = "noname";
}
