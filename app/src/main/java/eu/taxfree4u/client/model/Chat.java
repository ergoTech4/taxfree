package eu.taxfree4u.client.model;

import java.util.HashMap;

public class Chat {

    public int                      dataType = 1;
    public HashMap<String, String>  metaData = new HashMap<>();
    public int                      seen = 0;
    public String                   seenBy;
    public long                     seenTime;
    public long                     time;
    public String                   user_id;
//    public FileModel                uri;

    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    public Chat() {
    }
}