package eu.taxfree4u.client.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.interfaces.ClickListenerChatFirebase;
import eu.taxfree4u.client.model.Chat;
import eu.taxfree4u.client.model.Receipt;
import eu.taxfree4u.client.tools.AppDelegate;
import eu.taxfree4u.client.views.CircleTransform;

public class ChatFirebaseAdapter extends FirebaseRecyclerAdapter<Chat, ChatFirebaseAdapter.MyChatViewHolder> {

    private static final int RIGHT_MSG = 0;
    private static final int LEFT_MSG = 1;
    private static final int RIGHT_MSG_IMG = 2;
    private static final int LEFT_MSG_IMG = 3;
    private static final String TAG = "ChatFirebaseAdapter";

    private ClickListenerChatFirebase mClickListenerChatFirebase;

    private String              nameUser;
    private DatabaseReference   mReference;
    private FirebaseStorage     storage = FirebaseStorage.getInstance();
    private ImageLoader         imageLoader;
    private DisplayImageOptions options;



    public ChatFirebaseAdapter(DatabaseReference ref, String nameUser, ClickListenerChatFirebase mClickListenerChatFirebase) {
        super(Chat.class, R.layout.item_message_left, ChatFirebaseAdapter.MyChatViewHolder.class, ref);
        this.nameUser = nameUser;
        this.mClickListenerChatFirebase = mClickListenerChatFirebase;
        this.mReference     = ref;

        Map<String, String> headers = new HashMap<>();

        headers.put("access-token", AppDelegate.getInstance().getToken());


        imageLoader = ImageLoader.getInstance();

        options     = new DisplayImageOptions.Builder()
//                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .delayBeforeLoading(0)
                .extraForDownloader(headers)
                .showImageOnLoading(R.drawable.receipt_placeholder)
                .showImageForEmptyUri(R.drawable.receipt_placeholder)
                .build();
    }

    @Override
    public MyChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if ( viewType == RIGHT_MSG ) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_right, parent, false);
            return new MyChatViewHolder(view);
        } else if ( viewType == LEFT_MSG ) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_left, parent, false);
            return new MyChatViewHolder(view);
        } else if ( viewType == RIGHT_MSG_IMG ) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_right_img, parent, false);
            return new MyChatViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_left_img, parent, false);
            return new MyChatViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        try {
            Chat model = getItem(position);

            if (model.user_id.equals(nameUser)) {

                if ( model.dataType == 1) {
                    return RIGHT_MSG;
                } else if ( model.dataType == 2 ){
                    return RIGHT_MSG_IMG;
                } else if ( model.dataType == 3 ){
                    return RIGHT_MSG_IMG;
                } else {
                    return RIGHT_MSG;
                }
            } else {
                model.seen      = 1;
                model.seenBy    = AppDelegate.getInstance().getUserId();
                if ( model.dataType == 1) {
                    return LEFT_MSG;
                } else if ( model.dataType == 2 ){
                    return LEFT_MSG_IMG;
                } else if ( model.dataType == 3 ){
                    return LEFT_MSG_IMG;
                } else {
                    return LEFT_MSG;
                }
            }
        } catch (DatabaseException exception) {
            return RIGHT_MSG;
        }
//        } else if (model.getFile() != null){
//            if ( model.getFile().getType().equals("img") && model.getUserModel().getName().equals(nameUser)){
//                return RIGHT_MSG_IMG;
//            }else{
//                return LEFT_MSG_IMG;
//            }
//        } else if (model.getUserModel().getName().equals(nameUser)){
//            return RIGHT_MSG;
//        }else{
//            return LEFT_MSG;
//        }
    }

    @Override
    protected void populateViewHolder(final MyChatViewHolder viewHolder, Chat model, int position) {
//        viewHolder.setIvUser(model.getUserModel().getPhoto_profile());
        viewHolder.setTxtMessage(model.metaData.get("text"));
        viewHolder.setTvTimestamp(model.seen);
        viewHolder.tvIsLocation(View.GONE);

        if ( model.metaData.get("uri") != null) {
//            if ( viewHolder.ivChatPhoto != null ) {
//                viewHolder.ivChatPhoto.setImageResource(R.drawable.icon_about);
//            }

            try {
//                Log.d(TAG, "model.metaData.get(\"uri\"): " + model.metaData.get("uri"));
                StorageReference gsReference = storage.getReferenceFromUrl(model.metaData.get("uri"));

                gsReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        viewHolder.setIvChatPhoto(uri.toString());
                    }

                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ( model.metaData.get("id") != null ) {
            Receipt receipt = getReceiptById(Integer.valueOf(model.metaData.get("id")));

            if ( receipt != null ) {
                viewHolder.setIvChatPhoto(receipt.file_thumb);
            }
        }
    }

    private Receipt getReceiptById(int id) {
        ArrayList<Receipt> receipts = AppDelegate.getInstance().getReceipsList();
        for ( int i = 0; i < receipts.size(); i++ ) {
            if ( id == receipts.get(i).id ) {
                return receipts.get(i);
            }
        }
        return null;
    }

    public class MyChatViewHolder extends RecyclerView.ViewHolder {

        TextView tvTimestamp,tvLocation;
        TextView txtMessage;
        ImageView ivUser,ivChatPhoto;

        public MyChatViewHolder(View itemView) {
            super(itemView);
            tvTimestamp = (TextView)itemView.findViewById(R.id.timestamp);
            txtMessage = (TextView)itemView.findViewById(R.id.txtMessage);
            tvLocation = (TextView)itemView.findViewById(R.id.tvLocation);
            ivChatPhoto = (ImageView)itemView.findViewById(R.id.img_chat);
            ivUser = (ImageView)itemView.findViewById(R.id.ivUserChat);
        }

//        @Override
//        public void onClick(View view) {
//            int position = getAdapterPosition();
//            Chat model = getItem(position);
//
////            if (model.getMapModel() != null) {
////                mClickListenerChatFirebase.clickImageMapChat(view,position,model.getMapModel().getLatitude(),model.getMapModel().getLongitude());
////            } else {
////                mClickListenerChatFirebase.clickImageChat(view,position,model.getUserModel().getName(),model.getUserModel().getPhoto_profile(),model.getFile().getUrl_file());
////            }
//        }

        public void setTxtMessage(String message){
            if (txtMessage == null)return;
            txtMessage.setText(message);
        }

        public void setIvUser(String urlPhotoUser){
            if (ivUser == null)return;
            Glide.with(ivUser.getContext()).load(urlPhotoUser).centerCrop().transform(new CircleTransform(ivUser.getContext())).override(40,40).into(ivUser);
        }

        public void setTvTimestamp(int seen){
            if (tvTimestamp == null)return;
            if ( seen == 0 ) {
                tvTimestamp.setText("not seen");
            } else {
                tvTimestamp.setText("seen");
            }
        }

        public void setIvChatPhoto(String url){
            if (ivChatPhoto == null) return;
            imageLoader.displayImage(url, ivChatPhoto, options);
        }

        public void tvIsLocation(int visible){
            if (tvLocation == null)return;
            tvLocation.setVisibility(visible);
        }

    }
//
//    private CharSequence converteTimestamp(String mileSegundos){
//        return DateUtils.getRelativeTimeSpanString(Long.parseLong(mileSegundos), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
//    }

}
