package eu.taxfree4u.client.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.interfaces.ChatInterface;
import eu.taxfree4u.client.model.Receipt;
import eu.taxfree4u.client.tools.AppDelegate;

public class ReceiptsAdapter extends BaseAdapter {

    private static final String TAG = "InvoiceAdapter";
    private ArrayList<Receipt>   invoices;
    private ArrayList<Receipt>   sendList = new ArrayList<>();
    private Context         context;
    private LayoutInflater  inflater;

    private ImageLoader         imageLoader;
    private DisplayImageOptions options;

    public ReceiptsAdapter(ArrayList<Receipt> invoices, Context context) {
        inflater        = LayoutInflater.from(context);

        this.invoices   = invoices;
        this.context    = context;

        Map<String, String> headers = new HashMap<>();

        headers.put("access-token", AppDelegate.getInstance().getToken());

        imageLoader = ImageLoader.getInstance();

        options     = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .extraForDownloader(headers)
                .delayBeforeLoading(200)
                .showImageForEmptyUri(R.drawable.receipt_placeholder)
                .build();

    }

    @Override
    public int getCount() {
        return invoices.size();
    }

    @Override
    public Object getItem(int position) {
        return invoices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view;
        final Holder        holder;
        final Receipt   receipt = invoices.get(position);

        if ( convertView == null ) {
            view            = inflater.inflate(R.layout.item_invoice, parent, false);
            holder          = new Holder();

            holder.date         = (TextView) view.findViewById(R.id.item_invoice_date);
            holder.receiptImage = (ImageView) view.findViewById(R.id.item_invoice_image);
            holder.flag         = (ImageView) view.findViewById(R.id.item_invoice_flag);
            holder.reject       = (ImageView) view.findViewById(R.id.item_invoice_rejected);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (Holder) view.getTag();
        }

        try {


            DateFormat formatter = new SimpleDateFormat("dd/MM/yy', 'HH:mm");

//            Date date = receipt.getUpdatedAt();
//            holder.date.setText(formatter.format(date));

            imageLoader.displayImage(receipt.file_thumb, holder.receiptImage, options);

//            if ( receipt.get("status") != null && receipt.get("status").equals("1")) {
//                holder.reject.setVisibility(View.VISIBLE);
//                holder.reject.setImageResource(R.drawable.icon_success);
//            } else if ( receipt.get("status") != null && receipt.get("status").equals("2")) {
//                holder.reject.setVisibility(View.VISIBLE);
//                holder.reject.setImageResource(R.drawable.icon_reject);
//            } else {
//                holder.reject.setVisibility(View.VISIBLE);
//                holder.reject.setImageResource(R.drawable.icon_onreview);
//            }

        } catch (NullPointerException | IllegalStateException e) {
            Log.d(TAG, "parse NullPointerException " + e.getMessage() );
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (sendList.contains(receipt)) {
                        sendList.remove(receipt);
                        holder.flag.setVisibility(View.GONE);
                    } else {
                        sendList.add(receipt);
                        holder.flag.setVisibility(View.VISIBLE);
                    }

                    ((ChatInterface) context).setSendList(sendList);
            }
        });

        if ( sendList.contains(receipt) ) {
            holder.flag.setVisibility(View.VISIBLE);
        } else {
            holder.flag.setVisibility(View.GONE);
        }

        return view;
    }

    private class Holder {
        TextView date;
        ImageView receiptImage;
        ImageView flag;
        ImageView reject;
    }

}
