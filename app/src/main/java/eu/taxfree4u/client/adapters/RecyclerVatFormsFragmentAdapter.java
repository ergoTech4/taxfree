package eu.taxfree4u.client.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.fragments.VatFormsFragment;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.model.TripObj;
import eu.taxfree4u.client.model.VatForm;
import eu.taxfree4u.client.tools.AppDelegate;

public class RecyclerVatFormsFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final String TAG = "RecyclerVatFormsFragmentAdapter";

    private ImageLoader         imageLoader;
    private DisplayImageOptions options;
    private FragmentInterface   fragmentInterface;
    private TripObj             currentTrip;
    private ArrayList<VatForm>  vatForms;
    private Activity            activity;
    private Map<String, String> headers = new HashMap<>();

    public RecyclerVatFormsFragmentAdapter(VatFormsFragment fragment, ArrayList<VatForm> vatForms, Activity activity) {
        fragmentInterface   = fragment;
        this.currentTrip    = AppDelegate.getInstance().getCurrentTrip();
        this.vatForms       = vatForms;
        this.activity       = activity;

        headers.put("access-token", AppDelegate.getInstance().getToken());

        imageLoader = ImageLoader.getInstance();

        options     = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .extraForDownloader(headers)
                .delayBeforeLoading(200)
                .showImageForEmptyUri(R.drawable.vatform_placeholder)
                .build();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_row_vatform, parent, false);
            return new VHItem(view);
        } else if (viewType == TYPE_HEADER) {
            View header = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_row_header, parent, false);
            return new VHHeader(header);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if ( holder instanceof VHItem ) {
            final VatForm dataItem = getItem(position);

            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm' | 'dd.MM.yyyy");
            String dateString = formatter.format(new Date(dataItem.updated * 1000));

            ((VHItem) holder).date.setText(dateString);
            ((VHItem) holder).status.setText(String.format("%s", dataItem.status));

            if ( dataItem.comment.length() > 5 ) {
                ((VHItem) holder).comment.setText(dataItem.comment);
            } else {
                ((VHItem) holder).comment.setText("");
            }

            try {
                ((VHItem) holder).refund.setText(String.format("%s: %s", activity.getResources().getString(R.string.refund), dataItem.client_refund));
            } catch (NullPointerException e) {
                e.printStackTrace();
                ((VHItem) holder).refund.setText(String.format("%s: %s", "Refund: ", dataItem.client_refund));
            }

//            ((VHItem) holder).webView.getSettings().setJavaScriptEnabled(true);
//
//
//            ((VHItem) holder).webView.loadUrl("http://www.tex.uniyar.ac.ru/doc/href_in_LaTeX.pdf");

            imageLoader.displayImage(dataItem.file_thumb, ((VHItem) holder).imageCheck, options);

            ((VHItem) holder).imageCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragmentInterface.showImage(vatForms.get(position - 1).id);
                }
            });

            if ( dataItem.error == 1 ) {
                ((VHItem) holder).back.setBackgroundColor(activity.getResources().getColor(R.color.error_red));
            } else {
                ((VHItem) holder).back.setBackgroundColor(activity.getResources().getColor(R.color.white));
            }

            ((VHItem) holder).share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("image/*");
                    Uri fileUri = FileProvider.getUriForFile(activity, "eu.taxfree4u.client", new File(activity.getExternalFilesDir(null) + "/" + dataItem.updated + ".pdf"));
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
                    activity.startActivity(Intent.createChooser(shareIntent, "Share VAT forms using"));

//
//                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//
//                    Uri screenshotUri = Uri.parse(activity.getExternalFilesDir(null) + "/" + dataItem.updated + ".pdf");
//
//                    sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//
//                    sharingIntent.setType("*/*");
//
//                    sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
//
//                    activity.startActivity(Intent.createChooser(sharingIntent, "Share VAT forms using"));
                }
            });


        } else if ( holder instanceof VHHeader ) {
            ((VHHeader) holder).refund.setText(AppDelegate.getInstance().getTotalRefundVAT());
        }
    }

    @Override
    public int getItemCount() {
        return vatForms.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if ( position == 0 ) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private VatForm getItem(int position) {
        return vatForms.get(position - 1);
    }

    class VHItem extends RecyclerView.ViewHolder {
        TextView    date, refund, status, comment;
        ImageView   retake;
        ImageView   imageCheck;
        LinearLayout back;
        ImageView   share;

        public VHItem(View itemView) {
            super(itemView);
            date        = (TextView) itemView.findViewById(R.id.current_trip_item_date);
            status      = (TextView) itemView.findViewById(R.id.current_trip_item_status);
            comment     = (TextView) itemView.findViewById(R.id.current_trip_item_comment);
            refund      = (TextView) itemView.findViewById(R.id.current_trip_item_refund);

            retake      = (ImageView) itemView.findViewById(R.id.current_trip_item_retake);
            imageCheck  = (ImageView) itemView.findViewById(R.id.current_trip_item_webview);
            share       = (ImageView) itemView.findViewById(R.id.current_trip_item_share);

            back        = (LinearLayout) itemView.findViewById(R.id.vat_form_back);
        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        TextView refund;
        public VHHeader(View itemView) {
            super(itemView);
            refund       = (TextView) itemView.findViewById(R.id.current_trip_item_refund);
        }
    }

}
