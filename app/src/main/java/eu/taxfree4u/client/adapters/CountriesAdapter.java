package eu.taxfree4u.client.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.createTrip.RegistrationStep2Fragment;
import eu.taxfree4u.client.createTrip.RegistrationStep3Fragment;
import eu.taxfree4u.client.fragments.DetailsFragment;
import eu.taxfree4u.client.fragments.ProfileFragment;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.CreateTripInterface;
import eu.taxfree4u.client.model.Country;
import eu.taxfree4u.client.model.TripObj;
import eu.taxfree4u.client.model.User;
import eu.taxfree4u.client.tools.AppDelegate;

public class CountriesAdapter extends BaseAdapter {

    private static final String TAG = "CountriesAdapter";
    private LayoutInflater      inflater;
    private ArrayList<Country>  countries, filtred;
    private String              tag;

    private CreateTripInterface fragmentHolder;
    private AppInterface        appInterface;

    public CountriesAdapter(ArrayList<Country> countries, Context context, String tag, CreateTripInterface fragmentHolder, AppInterface appInterface){
        this.countries      = countries;
        this.inflater       = LayoutInflater.from(context);
        this.filtred        = new ArrayList<>();
        this.tag            = tag;

        this.filtred.addAll(countries);

        if ( fragmentHolder == null ) {
            this.appInterface = appInterface;
        } else {
            this.fragmentHolder = fragmentHolder;
        }
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Object getItem(int position) {
        return countries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return countries.get(position).id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View      view;
        final Holder    holder;

        if ( convertView == null ) {
            view                    = inflater.inflate(R.layout.item_country, parent, false);
            holder                  = new Holder();
            holder.countryName      = (TextView) view.findViewById(R.id.item_country_name);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (Holder) view.getTag();
        }

        holder.countryName.setText(countries.get(position).name);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TripObj tripObj = AppDelegate.getInstance().getCurrentTrip();

                if ( RegistrationStep2Fragment.TAG.equals(tag)) {
                    AppDelegate.getInstance().getUser().country = getItemId(position);
                } else if (RegistrationStep3Fragment.TAG_DESTINATION.equals(tag)) {
                    tripObj.destination_country = (int)getItemId(position);
                } else if (RegistrationStep3Fragment.TAG_DEPARTURE.equals(tag)) {
                    tripObj.departure_country = (int)getItemId(position);
                } else if (DetailsFragment.TAG_DEP.equals(tag)){
                    tripObj.departure_country = (int)getItemId(position);
                } else if (DetailsFragment.TAG_DES.equals(tag)){
                    tripObj.destination_country = (int)getItemId(position);

                } else if (ProfileFragment.TAG_CITIZEN.equals(tag)){
                    User user = AppDelegate.getInstance().getUser();
                    user.passport_citizenship = (int)getItemId(position);

                    AppDelegate.getInstance().saveUser(user);
                } else if (ProfileFragment.TAG_COUNTRY.equals(tag)){
                    User user = AppDelegate.getInstance().getUser();
                    user.country = (int)getItemId(position);

                    AppDelegate.getInstance().saveUser(user);
                } else if (ProfileFragment.TAG_PASS_COUNTRY.equals(tag)){
                    User user = AppDelegate.getInstance().getUser();
                    user.passport_country= (int)getItemId(position);

                    AppDelegate.getInstance().saveUser(user);
                }

                if ( fragmentHolder != null ) {
                    fragmentHolder.callPreviousFragment();
                } else if ( appInterface != null ) {
                    appInterface.callPreviousFragment();
                }

                AppDelegate.getInstance().setCurrentTrip(tripObj);
            }
        });

        return view;
    }

    private static class Holder {
        TextView    countryName;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());

        countries.clear();

        if (charText.length() == 0) {
            countries.addAll(filtred);
        } else {
            for ( Country county : filtred) {
                if (county.name.toLowerCase(Locale.getDefault()).contains(charText)) {
                    countries.add(county);
                }
            }
        }
        notifyDataSetChanged();
    }
}
