package eu.taxfree4u.client.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.fragments.ReceiptsFragment;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.model.Receipt;
import eu.taxfree4u.client.model.TripObj;
import eu.taxfree4u.client.tools.AppDelegate;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;


public class RecyclerReceiptFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final String TAG = "RecyclerReceiptFragmentAdapter";

    private ImageLoader         imageLoader;
    private DisplayImageOptions options;

    private FragmentInterface   fragmentInterface;
    private TripObj             currentTrip;
    private ArrayList<Receipt>  receiptsTemp, receipts;
    private Activity            activity;
    private String              totalRefund = "0";
    private ViewBinderHelper    binderHelper = new ViewBinderHelper();



    public RecyclerReceiptFragmentAdapter(ReceiptsFragment fragment, ArrayList<Receipt> receipts, Activity activity) {
        this.fragmentInterface   = fragment;

        this.currentTrip    = AppDelegate.getInstance().getCurrentTrip();
        this.receiptsTemp   = receipts;
        this.activity       = activity;
        Map<String, String> headers = new HashMap<>();

        headers.put("access-token", AppDelegate.getInstance().getToken());

        imageLoader = ImageLoader.getInstance();

        options     = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .extraForDownloader(headers)
                .delayBeforeLoading(200)
                .showImageForEmptyUri(R.drawable.receipt_placeholder)
                .build();

        sortReceipts();

    }

    private void sortReceipts() {
        receipts = new ArrayList<>();

        for ( int i = receiptsTemp.size()-1; i >= 0; i -- ) {
            if ( receiptsTemp.get(i).status.equals("request")) {
                receipts.add(receiptsTemp.get(i));
            }
        }

        for ( int i = receiptsTemp.size()-1; i >= 0; i -- ) {
            if ( receiptsTemp.get(i).status.equals("new") || receiptsTemp.get(i).status.equals("client-response") || receiptsTemp.get(i).status.equals("updated") ) {
                receipts.add(receiptsTemp.get(i));
            }
        }

        for ( int i = receiptsTemp.size()-1; i >= 0; i -- ) {
            if ( receiptsTemp.get(i).status.equals("signed")) {
                receipts.add(receiptsTemp.get(i));
            }
        }

        for ( int i = receiptsTemp.size()-1; i >= 0; i -- ) {
            if ( receiptsTemp.get(i).status.equals("refused")) {
                receipts.add(receiptsTemp.get(i));
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_row_receipt, parent, false);
            return new VHItem(view);
        } else if (viewType == TYPE_HEADER) {
            View header = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_row_header, parent, false);
            return new VHHeader(header);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if ( holder instanceof VHItem ) {
            final Receipt dataItem = getItem(position);

            binderHelper.bind(((VHItem) holder).swipeLayout, String.valueOf(position));
            ((VHItem) holder).bind(dataItem);

        } else if ( holder instanceof VHHeader ) {
            ((VHHeader) holder).refund.setText(AppDelegate.getInstance().getTotalRefundReceipts());
        }
    }

    @Override
    public int getItemCount() {
        return receipts.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if ( position == 0 ) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private Receipt getItem(int position) {
        return receipts.get(position - 1);
    }

    class VHItem extends RecyclerView.ViewHolder {
        SwipeRevealLayout swipeLayout;
        TextView        date, refund, headerTv;
        ImageView       retake;
        ImageView       imageCheck;
        LinearLayout    header;
        View            deleteLayout;
        RelativeLayout  main;


        public VHItem(View itemView) {
            super(itemView);
            swipeLayout = (SwipeRevealLayout) itemView.findViewById(R.id.swipe_layout);
            date        = (TextView) itemView.findViewById(R.id.current_trip_item_date);
            refund      = (TextView) itemView.findViewById(R.id.current_trip_item_refund);
            retake      = (ImageView) itemView.findViewById(R.id.current_trip_item_retake);
            headerTv    = (TextView) itemView.findViewById(R.id.current_trip_item_header_text);

            imageCheck  = (ImageView) itemView.findViewById(R.id.current_trip_item_check);

            header      = (LinearLayout) itemView.findViewById(R.id.current_trip_item_header);

            deleteLayout = itemView.findViewById(R.id.delete_layout);

            main         = (RelativeLayout) itemView.findViewById(R.id.current_trip_item_rl);

        }

        public void bind(final Receipt dataItem) {
            deleteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int pos = getAdapterPosition();

                    fragmentInterface.deleteReceipt(receipts.get(getAdapterPosition() - 1).id);
//                    receipts.remove(getAdapterPosition() - 1);
//                    notifyItemRemoved(getAdapterPosition());
                    swipeLayout.close(true);

                    if ( header.getVisibility() == View.VISIBLE ) {

                        Handler handler = new Handler();

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                notifyItemChanged(pos);

                                Log.d(TAG, "handler");
                            }
                        }, 300);
                    }
                }
            });

            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm' | 'dd.MM.yyyy");

            String dateString = formatter.format(new Date(dataItem.create_time * 1000));

            date.setText(dateString);

            refund.setText(String.format("%s: %s", activity.getResources().getString(R.string.refund), dataItem.client_refund));

            Log.d(TAG, "onBindViewHolder: " + dataItem.file_thumb);

            imageLoader.displayImage(dataItem.file_thumb, imageCheck, options);

            totalRefund = dataItem.client_refund;

            imageCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragmentInterface.showImage(dataItem.id);
                }
            });

            retake.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragmentInterface.retakeReceipt(dataItem.id);
                }
            });

            switch (dataItem.status) {
                case "refused":
//                    header.setVisibility(View.VISIBLE);
                    headerTv.setText(activity.getString(R.string.rejected));
                    if (dataItem.comment.length() > 5) {
                        main.setBackgroundColor(activity.getResources().getColor(R.color.error_red));
                    }
                    retake.setVisibility(View.GONE);
                    break;
                case "new":
//                    header.setVisibility(View.VISIBLE);
                    headerTv.setText(activity.getString(R.string.in_progress));
                    retake.setVisibility(View.GONE);
                    main.setBackgroundColor(activity.getResources().getColor(R.color.white));
                    break;
                case "client-response":
//                    header.setVisibility(View.VISIBLE);
                    headerTv.setText(activity.getString(R.string.in_progress));
                    retake.setVisibility(View.GONE);
                    main.setBackgroundColor(activity.getResources().getColor(R.color.white));
                    break;
                case "signed":
//                    header.setVisibility(View.VISIBLE);
                    headerTv.setText(activity.getString(R.string.done));
                    retake.setVisibility(View.GONE);
                    main.setBackgroundColor(activity.getResources().getColor(R.color.white));
                    break;
                case "request":
//                    header.setVisibility(View.VISIBLE);
                    headerTv.setText(activity.getString(R.string.error));
                    retake.setVisibility(View.VISIBLE);
                    main.setBackgroundColor(activity.getResources().getColor(R.color.error_red));

                    break;
                default:
                    header.setVisibility(View.GONE);
                    retake.setVisibility(View.GONE);
                    main.setBackgroundColor(activity.getResources().getColor(R.color.white));
                    break;
            }

            if ( getAdapterPosition() > 1 && receipts.get(getAdapterPosition() - 1).status.equals(receipts.get(getAdapterPosition() - 2).status) ) {
                header.setVisibility(View.GONE);
            } else {
                header.setVisibility(View.VISIBLE);
            }
        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        TextView refund;
        public VHHeader(View itemView) {
            super(itemView);
            refund       = (TextView) itemView.findViewById(R.id.current_trip_item_refund);
        }
    }
}
