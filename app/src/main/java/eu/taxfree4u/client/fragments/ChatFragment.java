package eu.taxfree4u.client.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.activities.MainActivity;
import eu.taxfree4u.client.activities.ReceiptsGridActivity;
import eu.taxfree4u.client.adapters.ChatFirebaseAdapter;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.ChatFragmentInterface;
import eu.taxfree4u.client.interfaces.ClickListenerChatFirebase;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.model.Chat;
import eu.taxfree4u.client.model.FileModel;
import eu.taxfree4u.client.model.Receipt;
import eu.taxfree4u.client.tools.AppDelegate;
import eu.taxfree4u.client.views.TouchImageView;
import okhttp3.internal.Util;

import static android.app.Activity.RESULT_OK;


public class ChatFragment extends Fragment implements ClickListenerChatFirebase, ChatFragmentInterface {

    private static final String TAG = "ChatFragment";
    private static final int REQUEST_TAKE_PHOTO = 22;
    private static final int REQUEST_PHOTOS     = 23;
    private String                  mCustomToken;

    private View                    rootView;
    private AppInterface            fragmentHolder;
    private Context                 context;
    private RecyclerView            mMessageRecyclerView;
    private LinearLayoutManager     mLinearLayoutManager;
    private DatabaseReference       mFirebaseRef;
    private ProgressBar             mProgressBar;
    private RelativeLayout          chatFieldLL;
    private ImageView               sendAttachment;
    private BottomSheetLayout       bottomSheet;
    private FirebaseStorage         storage = FirebaseStorage.getInstance();

    @Override
    public void clickImageChat(View view, int position, String nameUser, String urlPhotoUser, String urlPhotoClick) {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);

//        final Receipt receipt = ;

        dialog.setContentView(R.layout.dialog_image_layout);

        Map<String, String> headers = new HashMap<>();

        headers.put("access-token", AppDelegate.getInstance().getToken());

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .extraForDownloader(headers)
                .delayBeforeLoading(200)
                .showImageForEmptyUri(R.drawable.receipt_placeholder)
                .build();


        TouchImageView image      = (TouchImageView)  dialog.getWindow().findViewById(R.id.dialog_image);
        ImageView cancel          = (ImageView)  dialog.getWindow().findViewById(R.id.dialog_image_cancel);
        ImageView delete          = (ImageView) dialog.getWindow().findViewById(R.id.dialog_image_delete);
        TextView  reject          = (TextView)  dialog.getWindow().findViewById(R.id.dialog_image_reject);


        ImageLoader.getInstance().displayImage(urlPhotoClick, image, options);

//        if ( receipt.comment.length() > 5 ) {
//            reject.setText(receipt.comment);
//            dialog.getWindow().findViewById(R.id.dialog_main).setBackgroundResource(R.color.error_red);
//            image.setBackgroundResource(R.color.error_red);
//        } else {
//            image.setBackgroundResource(R.color.white);
//        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

//        if ( receipt.status.equals("signed") ) {
//            reject.setText(receipt.client_refund);
//        }
//
//        delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                fragmentHolder.deleteDialog(receipt.id);
//                dialog.dismiss();
//            }
//        });

        delete.setVisibility(View.GONE);

        dialog.show();
    }

    @Override
    public void clickImageMapChat(View view, int position, String latitude, String longitude) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if ( context instanceof MainActivity) {
                fragmentHolder = (AppInterface) context;
            }
            this.context = context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView        = inflater.inflate(R.layout.fr_chat, container, false);

        mProgressBar    = (ProgressBar) rootView.findViewById(R.id.progressBar);
        chatFieldLL     = (RelativeLayout) rootView.findViewById(R.id.listFooter);
        sendAttachment  = (ImageView) rootView.findViewById(R.id.send_attachment);
        bottomSheet     = (BottomSheetLayout) rootView.findViewById(R.id.bottomsheet);
        auth();

        return rootView;
    }

    private void auth() {
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setStackFromEnd(true);

        mFirebaseRef = fragmentHolder.getFireBaseRef();

        FirebaseDatabase base = FirebaseDatabase.getInstance();
        mFirebaseRef = base.getReference("firebase/chat/rooms/" + AppDelegate.getInstance().getFireBaseRoom());
        mFirebaseRef = mFirebaseRef.child("/messages/");
        lerMessagensFirebase();

        EditText inputText = (EditText) rootView.findViewById(R.id.messageInput);
        inputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    sendMessage();
                }
                return true;
            }
        });

        rootView.findViewById(R.id.sendButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

        sendAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.hideKeyboard();

                bottomSheet.showWithSheetView(LayoutInflater.from(getActivity()).inflate(R.layout.chat_sheet_layout, bottomSheet, false));
                TextView txtTakePhoto   = (TextView) bottomSheet.findViewById(R.id.txtTakePhoto);
                TextView txtGallery     = (TextView) bottomSheet.findViewById(R.id.txtGallery);
                TextView txtReceipts    = (TextView) bottomSheet.findViewById(R.id.sheet_receipts);
                TextView title          = (TextView) bottomSheet.findViewById(R.id.textView17);

                title.setText(getString(R.string.add_attachment));

                txtTakePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            File mFile = new File(getActivity().getExternalFilesDir(null), "image.jpg");
                            Uri imageUri = Uri.fromFile(mFile);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                        }
                    }
                });

                txtGallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, REQUEST_PHOTOS);
                    }
                });

                txtReceipts.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( fragmentHolder.getCurrentReceipts().size() > 0 ) {
                            AppDelegate.getInstance().setReceipsList(fragmentHolder.getCurrentReceipts());
                            startActivity(new Intent(context, ReceiptsGridActivity.class));
                        }
                    }
                });

                if ( fragmentHolder.getCurrentReceipts().size() > 0 ) {
                    txtReceipts.setTextColor(getResources().getColor(R.color.black));
                } else {
                    txtReceipts.setTextColor(getResources().getColor(R.color.gray_text));
                }
            }
        });
    }

    private void sendMessage() {
        EditText inputText = (EditText) rootView.findViewById(R.id.messageInput);
        String input = inputText.getText().toString();
        if (!input.equals("")) {

            Chat chat = new Chat();

            chat.dataType       = 1;
            chat.time           = System.currentTimeMillis();
            chat.seen           = 0;
            chat.user_id        = AppDelegate.getInstance().getUserId();

            chat.metaData.put("text", input);
            mFirebaseRef.push().setValue(chat);
            inputText.setText("");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        StorageReference storageRef = storage.getReferenceFromUrl(getString(R.string.base_firebase));

        if ( requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK ) {

            File mFile = new File(getActivity().getExternalFilesDir(null), "image.jpg");

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            Bitmap bitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath(), options);


            ByteArrayOutputStream stream = new ByteArrayOutputStream();

            bitmap = scaleBmp(bitmap);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);

//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//            Bitmap bitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath(), options);

            sendFileFirebase(storageRef, stream.toByteArray());

//            sendFileFirebase(storageRef, mFile.toURI());
//            fragmentHolder.savePhotoCheck(bitmap);
//            fragmentHolder.loadCheckPhoto();
//            fragmentHolder.updateCheckPhoto(retakeId);
//            showImageTemp();
        } else if ( requestCode == REQUEST_PHOTOS && resultCode == RESULT_OK ) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

                bitmap = scaleBmp(bitmap);

                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);

                byte[] buteData = baos.toByteArray();
                final String name =  "/" + System.currentTimeMillis() + "/image.jpg";

                StorageReference mountainsRef = storageRef.child(AppDelegate.getInstance().getUserId() + name);



                UploadTask uploadTask = mountainsRef.putBytes(buteData);

                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();

                        Log.d(TAG, "downloadUrl:  " + downloadUrl.toString());

                        Chat chat = new Chat();

//                    TODO userModel,"",Calendar.getInstance().getTime().getTime()+"",fileModel
                        chat.dataType       = 2;
                        chat.time           = System.currentTimeMillis();
                        chat.seen           = 0;
                        chat.user_id        = AppDelegate.getInstance().getUserId();

                        chat.metaData.put("uri", getString(R.string.base_firebase) + AppDelegate.getInstance().getUserId() + name);

                        mFirebaseRef.push().setValue(chat);
                    }
                });
            }
        }
    }

    private void sendFileFirebase(StorageReference storageReference, byte[] bytesOfPhoto) {
        if (storageReference != null) {
            final String name =  "/" + System.currentTimeMillis() + "/image.jpg";

            StorageReference imageGalleryRef = storageReference.child(AppDelegate.getInstance().getUserId() + name);

            UploadTask uploadTask = imageGalleryRef.putBytes(bytesOfPhoto);

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG,"onFailure sendFileFirebase "+e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Chat chat = new Chat();
//                    TODO userModel,"",Calendar.getInstance().getTime().getTime()+"",fileModel
                    chat.dataType       = 2;
                    chat.time           = System.currentTimeMillis();
                    chat.seen           = 0;
                    chat.user_id        = AppDelegate.getInstance().getUserId();

                    chat.metaData.put("uri", getString(R.string.base_firebase) + AppDelegate.getInstance().getUserId() + name);

                    mFirebaseRef.push().setValue(chat);
                }
            });
        }
    }

    private void lerMessagensFirebase(){

        if ( mFirebaseRef == null  ) {
            FirebaseDatabase base = FirebaseDatabase.getInstance();

            mFirebaseRef = base.getReference("firebase/chat/rooms/" + AppDelegate.getInstance().getFireBaseRoom());
            mFirebaseRef = mFirebaseRef.child("/messages/");
        }

        initAdapter();
    }

    private void initAdapter() {
        mMessageRecyclerView = (RecyclerView) rootView.findViewById(R.id.messageRecyclerView);

        final ChatFirebaseAdapter firebaseAdapter = new ChatFirebaseAdapter(mFirebaseRef, AppDelegate.getInstance().getUserId(), this);
        firebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = firebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mMessageRecyclerView.scrollToPosition(positionStart);
                }
                AppDelegate.getInstance().setNotificationCount(0);

            }
        });

        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
        mMessageRecyclerView.setAdapter(firebaseAdapter);
        mProgressBar.setVisibility(ProgressBar.INVISIBLE);
    }

    private Bitmap scaleBmp(Bitmap bmp) {
        final int maxSize = 1500;
        int outWidth;
        int outHeight;
        int inWidth = bmp.getWidth();
        int inHeight = bmp.getHeight();

        if(inWidth < inHeight){
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }

        return Bitmap.createScaledBitmap(bmp, outWidth, outHeight, false);
    }

    @Override
    public void hideBar(boolean hide) {
        RelativeLayout.LayoutParams layoutParams ;

        layoutParams = (RelativeLayout.LayoutParams) chatFieldLL.getLayoutParams();

        int dpValue = 60; // margin in dips
        float d = context.getResources().getDisplayMetrics().density;
        int margin = (int)(dpValue * d); // margin in pixels

        if ( hide ) {
            layoutParams.setMargins(0, 0, 0, 0);
        } else {
            layoutParams.setMargins(0, 0, 0, margin);
        }

        chatFieldLL.setLayoutParams(layoutParams);
    }

    @Override
    public void onResume() {
        super.onResume();
        bottomSheet.dismissSheet();
        AppDelegate.getInstance().setActivity(2);
    }

    @Override
    public void onPause() {
        AppDelegate.getInstance().setActivity(1);
        super.onPause();
    }
}
