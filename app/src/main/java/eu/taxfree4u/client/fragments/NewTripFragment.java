package eu.taxfree4u.client.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.helpshift.support.D;

import java.io.File;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.activities.VideoActivity;
import eu.taxfree4u.client.createTrip.CreateTripActivity;
import eu.taxfree4u.client.interfaces.AppInterface;

public class NewTripFragment extends Fragment {

    public static final String TAG = "NewTripFragment";

    private AppInterface    fragmentHolder;
    private RelativeLayout  newTripButton;
    private View            rootView;
    private LinearLayout    videoTutorial;
    private ImageView       menu;
    private BottomSheetLayout bottomSheet;

    public static NewTripFragment newInstance() {
        return new NewTripFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentHolder = (AppInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fr_new_trip, container, false);

        initialize();

        newTripButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File checkImage =  new File(getActivity().getExternalFilesDir(null), "checkPhoto.jpg");
                if(checkImage.exists()) {
                    checkImage.delete();
                }

                File passportImage =  new File(getActivity().getExternalFilesDir(null), "passportPhoto.jpg");
                if(passportImage.exists()) {
                    passportImage.delete();
                }

                bottomSheet.showWithSheetView(LayoutInflater.from(getActivity()).inflate(R.layout.my_sheet_layout, bottomSheet, false));
                TextView txtTakePhoto   = (TextView) bottomSheet.findViewById(R.id.txtTakePhoto);
                TextView txtGallery     = (TextView) bottomSheet.findViewById(R.id.txtGallery);

                txtTakePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(NewTripFragment.this.getActivity(), CreateTripActivity.class);

                        intent.putExtra("source", 1);

                        startActivity(intent);
                    }
                });

                txtGallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(NewTripFragment.this.getActivity(), CreateTripActivity.class);

                        intent.putExtra("source", 2);

                        startActivity(intent);
                    }
                });
            }
        });

        videoTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), VideoActivity.class));
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callMenu();
            }
        });

        return rootView;
    }

    private void initialize() {
        newTripButton   = (RelativeLayout) rootView.findViewById(R.id.new_trip_button);
        menu            = (ImageView) rootView.findViewById(R.id.menu_button);
        videoTutorial   = (LinearLayout) rootView.findViewById(R.id.video_tutorial);

        bottomSheet     = (BottomSheetLayout) rootView.findViewById(R.id.bottomsheet);
    }

    public void myToast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        bottomSheet.dismissSheet();
    }
}
