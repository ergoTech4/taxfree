package eu.taxfree4u.client.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.activities.VideoActivity;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.model.Card;
import eu.taxfree4u.client.model.Country;
import eu.taxfree4u.client.model.TripObj;
import eu.taxfree4u.client.tools.AppDelegate;

public class DetailsFragment extends Fragment implements FragmentInterface {
    public static final String TAG = "DetailsFragment";
    public static final String TAG_DEP = "detail_departure";
    public static final String TAG_DES = "detail_destination";

    public static final long   SECOND_DIVIDER = 1000;

    private TripObj             currentTrip;
    private ArrayList<Country>  countries;
    private ArrayList<Card>     cards;

    private AppInterface    fragmentHolder;
    private View            rootView;
    private TextView        receiptMenu, detailsMenu, vatFormMenu;
    private EditText        from, to, destinationCountry, city, flightNumber,
                            departureCountry, paymentCard;
    private ImageView       editArrow, menu;
    private LinearLayout    videoTutorial;

    private static final long SECONDS_IN_YEAR = 31536000;

    private Calendar    cal;
    private int         day;
    private int         month;
    private int         year;
    private boolean     wasEditTrip;

    public static DetailsFragment newInstance() {
        return new DetailsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentHolder = (AppInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fr_details, container, false);

        countries   = AppDelegate.getInstance().getCountries();


        initialize();

        receiptMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callReceipts();
            }
        });

        vatFormMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callVatFrom();
            }
        });

        from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateDialogFrom(from);
            }
        });

        to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateDialogTo(to);
            }
        });

        destinationCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wasEditTrip = true;
                fragmentHolder.callCountyFragment(TAG_DES);

            }
        });

        departureCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wasEditTrip = true;
                fragmentHolder.callCountyFragment(TAG_DEP);
            }
        });

        editArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( currentTrip.date_start - currentTrip.date_end  <= 86000 ) {
                    saveTrip();

                    fragmentHolder.editTrip();

                    wasEditTrip = false;
                    editArrow.setVisibility(View.GONE);
                    menu.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.date_error), Toast.LENGTH_SHORT).show();
                }
            }
        });

        paymentCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callCards();
            }
        });

        videoTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), VideoActivity.class));
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callMenu();
            }
        });

        return rootView;
    }

    private void saveTrip() {
        currentTrip.flightNumber    = flightNumber.getText().toString();
        currentTrip.city            = city.getText().toString();
//        currentTrip.paymentCard     = paymentCard.getText().toString();

        AppDelegate.getInstance().setCurrentTrip(currentTrip);
    }

    private void initFields() {
        if ( currentTrip != null ) {
            Calendar calFrom = Calendar.getInstance();
            calFrom.setTimeInMillis(currentTrip.date_start * SECOND_DIVIDER);
            from.setText(String.format("%s / %s / %s", String.format("%02d", calFrom.get(Calendar.DAY_OF_MONTH)),
                    String.format("%02d", calFrom.get(Calendar.MONTH) + 1), String.format("%02d", calFrom.get(Calendar.YEAR))));

            Calendar calTo = Calendar.getInstance();
            calTo.setTimeInMillis(currentTrip.date_end * SECOND_DIVIDER);
            to.setText(String.format("%s / %s / %s", String.format("%02d", calTo.get(Calendar.DAY_OF_MONTH)),
                    String.format("%02d", calTo.get(Calendar.MONTH) + 1), String.format("%02d", calTo.get(Calendar.YEAR))));

            destinationCountry.setText(setCountryName(currentTrip.destination_country));

            departureCountry.setText(setCountryName(currentTrip.departure_country));

            city.setText(currentTrip.city);

            if (!currentTrip.flightNumber.equals("null") ) {
                flightNumber.setText(currentTrip.flightNumber);
            }

//            paymentCard.setText(currentTrip.paymentCard);

        } else {
            Log.d(TAG, "trip is NULL");

            fragmentHolder.loadCurrentTrip();
        }
    }

    private String setCountryName(int country) {
        for ( int i = 0; i < countries.size(); i++ ) {
            if ( countries.get(i).id == country ) {
                return countries.get(i).name;
            }
        }

        return "";
    }

    private void initialize() {
        from            = (EditText) rootView.findViewById(R.id.details_from);
        to              = (EditText) rootView.findViewById(R.id.details_to);
        destinationCountry = (EditText) rootView.findViewById(R.id.details_final_destination_country);
        city            = (EditText) rootView.findViewById(R.id.details_city);
        flightNumber    = (EditText) rootView.findViewById(R.id.details_flight_number);
        departureCountry = (EditText) rootView.findViewById(R.id.details_departure_country);
        paymentCard     = (EditText) rootView.findViewById(R.id.details_payment);

        receiptMenu     = (TextView) rootView.findViewById(R.id.current_trip_receipts);
        detailsMenu     = (TextView) rootView.findViewById(R.id.current_trip_details);
        vatFormMenu     = (TextView) rootView.findViewById(R.id.current_trip_vat_forms);

        editArrow       = (ImageView) rootView.findViewById(R.id.details_save_edit);
        menu            = (ImageView) rootView.findViewById(R.id.menu_button);

        videoTutorial   = (LinearLayout) rootView.findViewById(R.id.video_tutorial);

    }

    @Override
    public void onResume() {
        super.onResume();
        if ( fragmentHolder == null ) {
            fragmentHolder = (AppInterface) getActivity();
        }
        currentTrip = AppDelegate.getInstance().getCurrentTrip();
        cards       = fragmentHolder.getCurrentCards();

        initFields();

        detailsMenu.setSelected(true);

        if ( wasEditTrip ) {
            editArrow.setVisibility(View.VISIBLE);
            menu.setVisibility(View.GONE);
        } else {
            editArrow.setVisibility(View.GONE);
            menu.setVisibility(View.VISIBLE);
        }

        for ( int i = 0; i < cards.size(); i++ ) {
            if ( cards.get(i).is_default ) {
                String cardNumberTemp = cards.get(i).number.substring(0,4) + "-" + "****"
                        + "-" + "****" + "-" + cards.get(i).number.substring(12,16);

                paymentCard.setText(cardNumberTemp);
            }
        }
    }

    @Override
    public void doWork() {
//        receipts = fragmentHolder.getCurrentReceipts();
//        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void saveChanges() {
        wasEditTrip = true;

        editArrow.setVisibility(View.VISIBLE);
        menu.setVisibility(View.GONE);
    }

    @Override
    public void showImage(int position) {

    }

    @Override
    public void retakeReceipt(int id) {

    }

    @Override
    public void deleteReceipt(int id) {

    }

    public void dateDialogTo(final EditText editText) {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                editText.setText(String.format("%s / %s / %s", String.format("%02d", dayOfMonth),
                        String.format("%02d", monthOfYear + 1), String.format("%02d", year)));

                Calendar calTo =  Calendar.getInstance();
                calTo.set(year, monthOfYear, dayOfMonth);

                currentTrip.date_end = calTo.getTimeInMillis() / SECOND_DIVIDER;

                wasEditTrip = true;
                editArrow.setVisibility(View.VISIBLE);
                menu.setVisibility(View.GONE);
            }};

        Calendar calTo = Calendar.getInstance();
        calTo.setTimeInMillis(currentTrip.date_end * SECOND_DIVIDER);

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), listener, calTo.get(Calendar.YEAR), calTo.get(Calendar.MONTH), calTo.get(Calendar.DAY_OF_MONTH));

        dpDialog.getDatePicker().setMaxDate((currentTrip.date_start * 1000) + SECONDS_IN_YEAR * 1000);
        dpDialog.getDatePicker().setMinDate((currentTrip.date_start * 1000) - SECONDS_IN_YEAR * 1000);

        dpDialog.show();
    }


    public void dateDialogFrom(final EditText editText) {
        DatePickerDialog.OnDateSetListener listener=new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                editText.setText(String.format("%s / %s / %s", String.format("%02d", dayOfMonth),
                        String.format("%02d", monthOfYear + 1), String.format("%02d", year)));

                Calendar calFrom = Calendar.getInstance();
                calFrom.set(year, monthOfYear, dayOfMonth);

                currentTrip.date_start = calFrom.getTimeInMillis() / SECOND_DIVIDER;

                wasEditTrip = true;
                editArrow.setVisibility(View.VISIBLE);
                menu.setVisibility(View.GONE);
            }};

        Calendar calFrom = Calendar.getInstance();
        calFrom.setTimeInMillis(currentTrip.date_start * SECOND_DIVIDER);

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), listener, calFrom.get(Calendar.YEAR), calFrom.get(Calendar.MONTH), calFrom.get(Calendar.DAY_OF_MONTH));

        dpDialog.getDatePicker().setMaxDate(currentTrip.date_end * 1000 + SECONDS_IN_YEAR * 1000);
        dpDialog.getDatePicker().setMinDate(currentTrip.date_end * 1000 - SECONDS_IN_YEAR * 1000);

        dpDialog.show();
    }

}
