package eu.taxfree4u.client.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.model.Card;
import eu.taxfree4u.client.services.ConstEX;
import eu.taxfree4u.client.tools.AppDelegate;


public class FrCabinetCard extends Fragment {
    private static final String TAG = "FrCabinetCard";

    private int         position;
    private TextView    txtAmmount;
    private TextView    txtCurrency;
    private TextView    cardNumber;
    private TextView    cardDate;
    private TextView    txtName;
    private ImageView   imgLogo;
    private Card        card;

    public static FrCabinetCard newInstance(int orderPosition) {
        FrCabinetCard fragment = new FrCabinetCard();

        final Bundle args = new Bundle();
        args.putInt(ConstEX.EX_CARD_POSITION, orderPosition);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ConstEX.EX_CARD_POSITION);

        try {
            card = AppDelegate.getInstance().getCards().get(position);
        } catch (IndexOutOfBoundsException e) {
            card = null;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_cabinet_card, container, false);
//        app = ((App) getActivity().getApplication());

        initialize(view);

        return view;
    }

//    private void initData(){
//        card = utilDbGet.getCardDataByOrderNumber(position);
//        Log.d(TAG, "initData : " + card.availableBalance);
//
//        if (card.cardCurrency.equals("USD")) {
//            txtAmmount.setText(String.format("%s $", card.availableBalance));
//        } else if (card.cardCurrency.equals("GBP")) {
//            txtAmmount.setText(String.format("£ %s", card.availableBalance));
//        } else if (card.cardCurrency.equals("EUR")) {
//            txtAmmount.setText(String.format("%s €", card.availableBalance));
//        }
//
//        try {
//            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//            Calendar c = Calendar.getInstance();
//            c.setTime(formatter.parse(card.expiryDate));
//            c.add(Calendar.DATE, 1);
//
//            DateFormat dateFormat = new SimpleDateFormat("MM");
//            String month = dateFormat.format(c.getTime());
//
//            DateFormat dateFormat1 = new SimpleDateFormat("yy");
//            String year = dateFormat1.format(c.getTime());
//
//            cardDate.setText(String.format("%s/%s", month, year));
//        } catch (ParseException e) {
//            utilLog.myLogExeption(e);
//        }
//
//        txtName.setText(card.cardEmbossLine.toUpperCase());
//
//        String cardNo = card.cardNo;
//        String cardNo2 = cardNo.substring(0, 4) + " ****" + " **** " + cardNo.substring(12, 16);
//
//        cardNumber.setText(cardNo2);
//        int cardSystem = utilCreditCard.getCardNameId(cardNo);
//
//        if (cardSystem == 0) {
//            imgLogo.setImageResource(R.drawable.ic_visa);
//        } else if (cardSystem == 1) {
//            imgLogo.setImageResource(R.drawable.ic_master);
//        }
//
//    }

    private void initialize(View v) {
        cardNumber  = (TextView) v.findViewById(R.id.add_card_number_field);
        cardDate    = (TextView) v.findViewById(R.id.add_card_exripe_field);
//        txtAmmount  = (TextView) v.findViewById(R.id.txtAmmount);
//        txtCurrency = (TextView) v.findViewById(R.id.txtCurrency);
//        txtName     = (TextView) v.findViewById(R.id.txtName);
//
//        imgLogo     = (ImageView) v.findViewById(R.id.imgLogo);
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    private void initData() {
        if ( card != null ) {
            String tempCardNumber = card.number.substring(0, 4);

            tempCardNumber += " **** **** ";

            tempCardNumber += card.number.substring(12, 16);

            cardNumber.setText(tempCardNumber);

            cardDate.setText(String.format("%s / %s", card.expire_month, card.expire_year.substring(2, 4)));
        }

    }
}