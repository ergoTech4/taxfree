package eu.taxfree4u.client.fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.io.File;
import java.util.ArrayList;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.adapters.RecyclerVatFormsFragmentAdapter;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.model.VatForm;
import eu.taxfree4u.client.views.ConfigurableRecyclerView;
import eu.taxfree4u.client.views.RecyclerLayoutManager;

public class VatFormsFragment extends Fragment implements FragmentInterface {
    public static final String TAG = "ReceiptsFragment";

    private AppInterface    fragmentHolder;
    private View            rootView;
    private TextView        receiptMenu, detailsMenu, vatFormMenu;

    private SwipyRefreshLayout mSwipeRefreshLayout;
    private ArrayList<VatForm> vatForms;
    private ImageView                   menu;


    private ConfigurableRecyclerView        mRecyclerView;
    private RecyclerVatFormsFragmentAdapter mAdapter;
    private RecyclerLayoutManager           mLayoutManager;


    public static VatFormsFragment newInstance() {
        return new VatFormsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentHolder = (AppInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fr_vat_forms, container, false);

        initialize();

        vatForms = fragmentHolder.getCurrentVatForms();

        mLayoutManager  = new RecyclerLayoutManager(getActivity());
        mAdapter        = new RecyclerVatFormsFragmentAdapter(this, vatForms, getActivity());

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);

        receiptMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callReceipts();
            }
        });

        mSwipeRefreshLayout.setDistanceToTriggerSync(160);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                fragmentHolder.loadCurrentTripVatForms();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }, 10000);
            }
        });


        detailsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callDetails();
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callMenu();
            }
        });

//        vatFormMenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                fragmentHolder.callVatFrom();
//            }
//        });

        return rootView;
    }



    private void initialize() {
        mRecyclerView   = (ConfigurableRecyclerView) rootView.findViewById(R.id.current_trip_recycler_view);

        menu            = (ImageView) rootView.findViewById(R.id.menu_button);

        receiptMenu     = (TextView) rootView.findViewById(R.id.current_trip_receipts);
        detailsMenu     = (TextView) rootView.findViewById(R.id.current_trip_details);
        vatFormMenu     = (TextView) rootView.findViewById(R.id.current_trip_vat_forms);

        mSwipeRefreshLayout = (SwipyRefreshLayout) rootView.findViewById(R.id.swipeContainer);

    }

    @Override
    public void onResume() {
        super.onResume();
        if ( fragmentHolder == null ) {
            fragmentHolder = (AppInterface) getActivity();
        }

        vatFormMenu.setSelected(true);

        fragmentHolder.hideKeyboard();
    }

    @Override
    public void doWork() {
        if ( fragmentHolder == null ) {
            fragmentHolder = (AppInterface) getActivity();
        }

        mSwipeRefreshLayout.setRefreshing(false);
        try {

            vatForms = fragmentHolder.getCurrentVatForms();

            mAdapter        = new RecyclerVatFormsFragmentAdapter(this, vatForms, getActivity());

            mRecyclerView.setAdapter(mAdapter);

            if ( vatForms.size() > 0 ) {
//                placeHolder.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
            } else {
//                placeHolder.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveChanges() {

    }

    @Override
    public void showImage(int id) {
        final VatForm vatForm = getVatFormById(id);

        File file = new File(getActivity().getExternalFilesDir(null), vatForm.updated + ".pdf");

        if (file.exists()) {
            Uri path = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try {
                startActivity(intent);
            }
            catch (ActivityNotFoundException e) {
                Toast.makeText(getActivity(),
                        "No Application Available to View PDF", Toast.LENGTH_SHORT).show();
            }
        }

//        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//
//        dialog.setContentView(R.layout.dialog_image_layout);
//
//        Map<String, String> headers = new HashMap<>();
//
//        headers.put("access-token", AppDelegate.getInstance().getToken());
//
//        DisplayImageOptions options = new DisplayImageOptions.Builder()
//                .resetViewBeforeLoading(true)
//                .cacheInMemory(true)
//                .cacheOnDisc(true)
//                .extraForDownloader(headers)
//                .delayBeforeLoading(200)
//                .showImageForEmptyUri(R.drawable.receipt_placeholder)
//                .build();
//
//
//        TouchImageView image      = (TouchImageView)  dialog.getWindow().findViewById(R.id.dialog_image);
//        ImageView cancel          = (ImageView)  dialog.getWindow().findViewById(R.id.dialog_image_cancel);
////        TextView  reject          = (TextView)  dialog.getWindow().findViewById(R.id.dialog_image_reject);
//
//        ImageLoader.getInstance().displayImage(vatForms.get(position).file_thumb, image, options);
//
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
    }

    private VatForm getVatFormById(int id) {
        for ( int i = 0; i < vatForms.size(); i++ ) {
            if ( id == vatForms.get(i).id ) {
                return vatForms.get(i);
            }
        }

        return vatForms.get(0);
    }


    @Override
    public void retakeReceipt(int id) {

    }

    @Override
    public void deleteReceipt(int id) {

    }
}
