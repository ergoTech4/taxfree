package eu.taxfree4u.client.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.melnykov.fab.FloatingActionButton;
import com.rey.material.widget.CheckBox;

import java.util.ArrayList;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.activities.AddNewCardActivity;
import eu.taxfree4u.client.activities.FaqActivity;
import eu.taxfree4u.client.activities.MainActivity;
import eu.taxfree4u.client.createTrip.CreateTripActivity;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.model.Card;
import eu.taxfree4u.client.tools.AppDelegate;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;


public class CardsFragment extends Fragment implements FragmentInterface {
    public static final String TAG = "CardsFragment";
    private static final int MY_SCAN_REQUEST_CODE = 16;

    private View            rootView;
    private AppInterface    fragmentHolder;
    private ViewPager       pager;
    private PagerAdapter    pagerAdapter;
    private TextView        cardName;
    private ImageView       menu, placeHolder;
    private CheckBox        useDefault;
    private RelativeLayout  editCard;
    private Context         context;
    private Button          buyButton;
    private TextView        details, editCardTv;

    private FloatingActionButton fab;
    private ArrayList<Fragment> frCabinet = new ArrayList<>();
    private ArrayList<Card> cards;


    public static CardsFragment newInstance() {
        return new CardsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if ( context instanceof MainActivity ) {
                fragmentHolder = (AppInterface) context;
            }
            this.context = context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fr_cards, container, false);
        initialize();

        cards = AppDelegate.getInstance().getCards();

        if ( cards.size() > 0 ) {
            useDefault.setChecked(cards.get(0).is_default);
            if ( cards.get(0).name.equals("") ) {
                cardName.setText(getString(R.string.no_name));
            } else {
                cardName.setText(cards.get(0).name);
            }
            useDefault.setTextColor(getResources().getColor(R.color.black));
            editCardTv.setTextColor(getResources().getColor(R.color.black));
            useDefault.setEnabled(true);
        } else {
            cardName.setText(getString(R.string.no_card));
            useDefault.setEnabled(false);
            useDefault.setTextColor(getResources().getColor(R.color.gray_text));
            editCardTv.setTextColor(getResources().getColor(R.color.gray_text));

        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), AddNewCardActivity.class));

                startCardIO();

            }
        });

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if ( cards.get(position).name.equals("") ) {
                    cardName.setText(getString(R.string.no_name));
                } else {
                    cardName.setText(cards.get(position).name);
                }

                useDefault.setChecked(cards.get(position).is_default);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if ( fragmentHolder == null ) {
            menu.setVisibility(View.GONE);
        } else {
            menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragmentHolder.callMenu();
                }
            });
        }

        editCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ( cards.size() > 0 ) {
                    Intent intent = new Intent(getActivity(), AddNewCardActivity.class);

                    intent.putExtra("cardId", cards.get(pager.getCurrentItem()).id);

                    startActivity(intent);
                }
            }
        });

        useDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( cards.size() > 0 ) {
                    for (int i = 0; i < cards.size(); i++) {
                        cards.get(i).is_default = false;
                    }

                    cards.get(pager.getCurrentItem()).is_default = !cards.get(pager.getCurrentItem()).is_default;
                    if (fragmentHolder != null) {
                        fragmentHolder.loadCardIsDefault(cards.get(pager.getCurrentItem()).id, cards.get(pager.getCurrentItem()).is_default);
                    }
                    if (context instanceof CreateTripActivity) {
                        ((CreateTripActivity) context).loadCardIsDefault(cards.get(pager.getCurrentItem()).id, cards.get(pager.getCurrentItem()).is_default);
                    }

                    AppDelegate.getInstance().setCards(cards);
                }
            }
        });

        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), FaqActivity.class).putExtra("faq", 2));
            }
        });

        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), FaqActivity.class).putExtra("faq", 3));
            }
        });

        return rootView;
    }

    private void startCardIO() {
        if ( context instanceof CreateTripActivity) {
            ((CreateTripActivity)context).startCardIO();
        } else if ( fragmentHolder != null ) {

            Intent scanIntent = new Intent(context, CardIOActivity.class);

            // customize these values to suit your needs.
            scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
            scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
            scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
            scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true);
            scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false); // default: true

            // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.

            startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "requestCode:" + requestCode);

        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;

            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

                Log.d(TAG, "123card.number: " + resultDisplayStr);

                Intent intent = new Intent(context, AddNewCardActivity.class);

                intent.putExtra("cardNumber", scanResult.cardNumber);

                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";


                    Log.d(TAG, "123card.number: " + resultDisplayStr);
                    intent.putExtra("expiryMonth", scanResult.expiryMonth);
                    intent.putExtra("expiryYear", scanResult.expiryYear);
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }

                context.startActivity(intent);
            }
            else {
                resultDisplayStr = "Scan was canceled.";
            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
        }
        // else handle other activity results
    }

    private void initialize() {
        pager           = (ViewPager) rootView.findViewById(R.id.pager);
        cardName        = (TextView) rootView.findViewById(R.id.card_name);
        menu            = (ImageView) rootView.findViewById(R.id.menu_button);
        placeHolder     = (ImageView) rootView.findViewById(R.id.card_place_holder);
        useDefault      = (CheckBox) rootView.findViewById(R.id.card_default);
        editCard        = (RelativeLayout) rootView.findViewById(R.id.edit_card);
        details         = (TextView) rootView.findViewById(R.id.card_details);
        buyButton       = (Button) rootView.findViewById(R.id.card_buy);

        editCardTv      = (TextView) rootView.findViewById(R.id.edit_card_text);

        fab             = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setType(FloatingActionButton.TYPE_NORMAL);
        fab.setColorNormal(getResources().getColor(R.color.orange));
        fab.setColorPressed(getResources().getColor(R.color.orange_pressed));
    }

    @Override
    public void doWork() {
        cardName.setText(cards.get(pager.getCurrentItem()).id);
    }

    @Override
    public void saveChanges() {
        cards = AppDelegate.getInstance().getCards();

        pagerAdapter    = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        if ( cards.size() == 0 ) {
            placeHolder.setVisibility(View.VISIBLE);
        } else {
            placeHolder.setVisibility(View.GONE);
        }
    }

    @Override
    public void showImage(int position) {

    }

    @Override
    public void retakeReceipt(int id) {

    }

    @Override
    public void deleteReceipt(int id) {

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            frCabinet.add(position, FrCabinetCard.newInstance(position));
            return frCabinet.get(position);
        }

        @Override
        public int getCount() {
            return cards.size();
        }

        public float getPageWidth(int position) {
            if ( getCount() > 1 ) {
                return 0.88f;
            } else {
                return 1f;
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if (AppDelegate.getInstance().isNeedUpdate()) {
            if ( fragmentHolder != null ) {
                fragmentHolder.loadCards();
            } else if ( context instanceof CreateTripActivity ) {
                ((CreateTripActivity)context).loadCards();
            }
        }

        cards = AppDelegate.getInstance().getCards();

        pagerAdapter    = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        if ( cards.size() == 0 ) {
            placeHolder.setVisibility(View.VISIBLE);
        } else {
            placeHolder.setVisibility(View.GONE);
        }
    }
}
