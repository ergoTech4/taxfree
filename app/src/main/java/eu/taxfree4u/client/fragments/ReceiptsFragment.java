package eu.taxfree4u.client.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.melnykov.fab.FloatingActionButton;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.activities.VideoActivity;
import eu.taxfree4u.client.adapters.RecyclerReceiptFragmentAdapter;
import eu.taxfree4u.client.createTrip.CreateTripActivity;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.model.Receipt;
import eu.taxfree4u.client.tools.AppDelegate;
import eu.taxfree4u.client.views.RecyclerLayoutManager;
import eu.taxfree4u.client.views.TouchImageView;

import static android.app.Activity.RESULT_OK;


public class ReceiptsFragment extends Fragment implements FragmentInterface {
    public static final String TAG = "ReceiptsFragment";
    static final int REQUEST_IMAGE_CAPTURE  = 1;
    static final int REQUEST_PHOTOS         = 2;
    static final int REQUEST_IMAGE_RETAKE   = 3;
    static final int REQUEST_PHOTOS_RETAKE  = 4;

    private AppInterface    fragmentHolder;
    private View            rootView;
    private TextView        receiptMenu, detailsMenu, vatFormMenu;

    private ArrayList<Receipt>          receipts = new ArrayList<>();
    private RecyclerView                mRecyclerView;
    private RecyclerReceiptFragmentAdapter mAdapter;
    private RecyclerLayoutManager       mLayoutManager;
    private FloatingActionButton        fab;
    private SwipyRefreshLayout          mSwipeRefreshLayout;
    private ImageView                   menu;
    private LinearLayout                placeHolder, videoTutorial;
    private BottomSheetLayout           bottomSheet;
    private int                         retakeId = -1;


    public static ReceiptsFragment newInstance() {
        return new ReceiptsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentHolder = (AppInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fr_receipts, container, false);

        initialize();

        receipts = fragmentHolder.getCurrentReceipts();

        mLayoutManager  = new RecyclerLayoutManager(getActivity());
        mAdapter        = new RecyclerReceiptFragmentAdapter(this, receipts, getActivity());

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mSwipeRefreshLayout.setDistanceToTriggerSync(160);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                fragmentHolder.loadCurrentTrip();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }, 10000);
            }
        });

        detailsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callDetails();
            }
        });

        vatFormMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callVatFrom();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheet.showWithSheetView(LayoutInflater.from(getActivity()).inflate(R.layout.my_sheet_layout, bottomSheet, false));
                TextView txtTakePhoto   = (TextView) bottomSheet.findViewById(R.id.txtTakePhoto);
                TextView txtGallery     = (TextView) bottomSheet.findViewById(R.id.txtGallery);

                txtTakePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        retakeId = 0;
                        dispatchTakePictureIntent();
                    }
                });

                txtGallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        retakeId = 0;
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, REQUEST_PHOTOS);
                    }
                });
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.callMenu();
            }
        });

//        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
//                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
//                    @Override
//                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
//                        Log.d(TAG, "DELETE2222");
//                        return false;
//                    }
//
//                    @Override
//                    public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
//
//                    }
//        };
//
//
//
//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
//
//        itemTouchHelper.attachToRecyclerView(mRecyclerView);
//
//

        videoTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), VideoActivity.class));
            }
        });

        return rootView;
    }

    private void initialize() {
        mRecyclerView   = (RecyclerView) rootView.findViewById(R.id.current_trip_recycler_view);

        receiptMenu     = (TextView) rootView.findViewById(R.id.current_trip_receipts);
        detailsMenu     = (TextView) rootView.findViewById(R.id.current_trip_details);
        vatFormMenu     = (TextView) rootView.findViewById(R.id.current_trip_vat_forms);

        menu            = (ImageView) rootView.findViewById(R.id.menu_button);

        mSwipeRefreshLayout = (SwipyRefreshLayout) rootView.findViewById(R.id.swipeContainer);

        placeHolder     = (LinearLayout) rootView.findViewById(R.id.receipts_placeholder);
        videoTutorial   = (LinearLayout) rootView.findViewById(R.id.video_tutorial);

        fab             = (FloatingActionButton) rootView.findViewById(R.id.fab);

        fab.attachToRecyclerView(mRecyclerView);
        fab.setType(FloatingActionButton.TYPE_NORMAL);
        fab.setColorNormal(getResources().getColor(R.color.orange));
        fab.setColorPressed(getResources().getColor(R.color.orange_pressed));

        bottomSheet     = (BottomSheetLayout) rootView.findViewById(R.id.bottomsheet);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "refresh:  onResume!!");
        if ( fragmentHolder == null ) {
            fragmentHolder = (AppInterface) getActivity();
        }
        receiptMenu.setSelected(true);

        if ( receipts.size() > 0 ) {
            placeHolder.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            placeHolder.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }

        bottomSheet.dismissSheet();

        if ( retakeId >= 0 ) {
            fragmentHolder.updateCheckPhoto(retakeId);
            retakeId = -1;
        }
    }

    @Override
    public void doWork() {
        if ( fragmentHolder == null ) {
            Log.d(TAG, "NULL!!");
            fragmentHolder = (AppInterface) getActivity();
        }

        mSwipeRefreshLayout.setRefreshing(false);

        try {
            receipts = fragmentHolder.getCurrentReceipts();

            mAdapter = new RecyclerReceiptFragmentAdapter(this, receipts, getActivity());

            mRecyclerView.setAdapter(mAdapter);

            if ( receipts.size() > 0 ) {
                placeHolder.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
            } else {
                placeHolder.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveChanges() {

    }

    @Override
    public void showImage(int id) {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);

        final Receipt receipt = getReceiptById(id);

        dialog.setContentView(R.layout.dialog_image_layout);

        Map<String, String> headers = new HashMap<>();

        headers.put("access-token", AppDelegate.getInstance().getToken());

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .extraForDownloader(headers)
                .delayBeforeLoading(200)
                .showImageForEmptyUri(R.drawable.receipt_placeholder)
                .build();


        TouchImageView image      = (TouchImageView)  dialog.getWindow().findViewById(R.id.dialog_image);
        ImageView cancel          = (ImageView)  dialog.getWindow().findViewById(R.id.dialog_image_cancel);
        ImageView delete          = (ImageView) dialog.getWindow().findViewById(R.id.dialog_image_delete);
        TextView  reject          = (TextView)  dialog.getWindow().findViewById(R.id.dialog_image_reject);


        ImageLoader.getInstance().displayImage(receipt.file, image, options);

        if ( receipt.comment.length() > 5 ) {
            reject.setText(receipt.comment);
            dialog.getWindow().findViewById(R.id.dialog_main).setBackgroundResource(R.color.error_red);
            image.setBackgroundResource(R.color.error_red);
        } else {
            image.setBackgroundResource(R.color.white);
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if ( receipt.status.equals("signed") ) {
            reject.setText(receipt.client_refund);
        }

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.deleteDialog(receipt.id);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private Receipt getReceiptById(int id) {
        for ( int i = 0; i < receipts.size(); i++ ) {
            if ( id == receipts.get(i).id ) {
                return receipts.get(i);
            }
        }

        return receipts.get(0);
    }

    @Override
    public void retakeReceipt(int id) {
        retakeId = id;

        bottomSheet.showWithSheetView(LayoutInflater.from(getActivity()).inflate(R.layout.my_sheet_layout, bottomSheet, false));
        TextView txtTakePhoto   = (TextView) bottomSheet.findViewById(R.id.txtTakePhoto);
        TextView txtGallery     = (TextView) bottomSheet.findViewById(R.id.txtGallery);

        txtTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    File mFile = new File(getActivity().getExternalFilesDir(null), "checkPhoto.jpg");
                    Uri imageUri = Uri.fromFile(mFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_RETAKE);
                }

            }
        });

        txtGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, REQUEST_PHOTOS_RETAKE);
            }
        });
    }

    @Override
    public void deleteReceipt(int id) {
        fragmentHolder.deleteDialog(id);
        fab.show(true);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File mFile = new File(getActivity().getExternalFilesDir(null), "checkPhoto.jpg");
            Uri imageUri = Uri.fromFile(mFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            File mFile = new File(getActivity().getExternalFilesDir(null), "checkPhoto.jpg");

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            Bitmap bitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath(), options);

            fragmentHolder.savePhotoCheck(bitmap);
//            fragmentHolder.loadCheckPhoto();
//            fragmentHolder.updateCheckPhoto(retakeId);
//            showImageTemp();
        } else if ( requestCode == REQUEST_PHOTOS && resultCode == RESULT_OK ) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                Bitmap bmp = BitmapFactory.decodeFile(picturePath);

//                Drawable d = new BitmapDrawable(getResources(), bmp);
//                preview.setBackgroundDrawable(d);

//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                bmp.compress(Bitmap.CompressFormat.JPEG, 90, stream);
//
                fragmentHolder.savePhotoCheck(bmp);
                fragmentHolder.loadCheckPhoto();
            }
        } else if (requestCode == REQUEST_IMAGE_RETAKE && resultCode == RESULT_OK) {
            File mFile = new File(getActivity().getExternalFilesDir(null), "checkPhoto.jpg");

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            Bitmap bitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath(), options);

            fragmentHolder.savePhotoCheck(bitmap);
            fragmentHolder.updateCheckPhoto(retakeId);
        } else if ( requestCode == REQUEST_PHOTOS_RETAKE && resultCode == RESULT_OK ) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                Bitmap bmp = BitmapFactory.decodeFile(picturePath);

//                Drawable d = new BitmapDrawable(getResources(), bmp);
//                preview.setBackgroundDrawable(d);

//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                bmp.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                fragmentHolder.savePhotoCheck(bmp);
                fragmentHolder.updateCheckPhoto(retakeId);
            }
        }
    }


    public void showImageTemp() {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);

        dialog.setContentView(R.layout.dialog_image_layout);
        File mFile = new File(getActivity().getExternalFilesDir(null), "checkPhoto.jpg");

        Map<String, String> headers = new HashMap<>();

        headers.put("access-token", AppDelegate.getInstance().getToken());

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .extraForDownloader(headers)
                .delayBeforeLoading(200)
                .showImageForEmptyUri(R.drawable.receipt_placeholder)
                .build();


        TouchImageView image      = (TouchImageView)  dialog.getWindow().findViewById(R.id.dialog_image);
        ImageView cancel          = (ImageView)  dialog.getWindow().findViewById(R.id.dialog_image_cancel);
        ImageView delete          = (ImageView) dialog.getWindow().findViewById(R.id.dialog_image_delete);
        TextView  reject          = (TextView)  dialog.getWindow().findViewById(R.id.dialog_image_reject);


//        image.setImageBitmap(bmp);
//
        ImageLoader.getInstance().displayImage("file://" + mFile, image, options);
//
//        if ( receipt.comment.length() > 5 ) {
//            reject.setText(receipt.comment);
//            dialog.getWindow().findViewById(R.id.dialog_main).setBackgroundResource(R.color.error_red);
//        }
//
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        if ( receipt.status.equals("signed") ) {
//            reject.setText(receipt.client_refund);
//        }
//
//        delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                fragmentHolder.deleteDialog(receipt.id);
//                dialog.dismiss();
//            }
//        });

        dialog.show();
    }



}
