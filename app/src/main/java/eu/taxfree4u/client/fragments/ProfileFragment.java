package eu.taxfree4u.client.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.createTrip.CreateTripActivity;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.FragmentInterface;
import eu.taxfree4u.client.model.Country;
import eu.taxfree4u.client.model.User;
import eu.taxfree4u.client.tools.AppDelegate;
import eu.taxfree4u.client.views.TouchImageView;

import static android.app.Activity.RESULT_OK;
import static eu.taxfree4u.client.fragments.ReceiptsFragment.REQUEST_IMAGE_CAPTURE;


public class ProfileFragment extends Fragment implements FragmentInterface {
    public static final String TAG      = "ProfileFragment";

    public static final String TAG_CITIZEN      = "Profile_Citizen";
    public static final String TAG_COUNTRY      = "Profile_Country";
    public static final String TAG_PASS_COUNTRY = "Profile_Pass_Country";

    private static final long SECOND_DIVIDER = 1000;
    private static final int REQUEST_IMAGE_PASSPORT = 3;
    private static final int REQUEST_PASSPORT = 4;

    private MaterialEditText    fullName, phone, email, passport_citizenship, passport_date, address, passport_photo;
    private MaterialEditText    passport_country, country, passport_number, city, passport_birthday, profile_postcode, changePassword;
    private Spinner             passport_sex;

    private TextView            saveButton, identity;
    private Button              logoutButton;
    private User                user;
    private View                rootView;
    private ArrayList<Country>  countries;
    private ImageLoader             imageLoader;
    private DisplayImageOptions     options;

    private boolean             wasEditTrip;
    private AppInterface        fragmentHolder;
    private ImageView           editArrow, passportImage, what;
    private ImageView           imageBarCode;
    private BottomSheetLayout   bottomSheet;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            fragmentHolder = (AppInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fr_profile, container, false);

        initialize();

        Map<String, String> headers = new HashMap<>();

        headers.put("access-token", AppDelegate.getInstance().getToken());

        imageLoader = ImageLoader.getInstance();

        options     = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .extraForDownloader(headers)
                .delayBeforeLoading(200)
                .showImageForEmptyUri(R.drawable.receipt_placeholder)
                .build();

        countries = AppDelegate.getInstance().getCountries();
        user      = AppDelegate.getInstance().getUser();

        initFields();

//        saveButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                createSaveDialog();
//            }
//        });
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createLogoutDialog();
            }
        });

        editArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user.full_name          = fullName.getText().toString();
                user.phone              = phone.getText().toString();
                user.email              = email.getText().toString();

                user.passport_number    = passport_number.getText().toString();
                user.city               = city.getText().toString();
                user.address            = address.getText().toString();
                user.post_code          = profile_postcode.getText().toString();

                AppDelegate.getInstance().saveUser(user);

                fragmentHolder.saveUser();

                editArrow.setVisibility(View.GONE);
                wasEditTrip = false;

                fragmentHolder.hideKeyboard();
            }
        });

        passport_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateDialogExpire(passport_date);
            }
        });

        passport_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wasEditTrip = true;
                editArrow.setVisibility(View.VISIBLE);
                fragmentHolder.callCountyFragment(TAG_PASS_COUNTRY);
            }
        });

        country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wasEditTrip = true;
                editArrow.setVisibility(View.VISIBLE);
                fragmentHolder.callCountyFragment(TAG_COUNTRY);
            }
        });

        passport_citizenship.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wasEditTrip = true;
                editArrow.setVisibility(View.VISIBLE);
                fragmentHolder.callCountyFragment(TAG_CITIZEN);
            }
        });

        passport_birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateDialogBirth(passport_birthday);
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHolder.changePasswordDialog(true);
            }
        });

//        BarCodeView barCodeView = new BarCodeView(getActivity(), user.identity);

//        EAN13 barcode = new EAN13();
//
//        barcode.setSupData("00");
//        barcode.setData("0000000000474");
//        // for EAN13 with supplement data (2 or 5 digits)
//        /*
//        barcode.setSupData("12");
//        // supplement bar height vs bar height ratio
//        barcode.setSupHeight(0.8f);
//        // space between barcode and supplement barcode (in pixel)
//        barcode.setSupSpace(15);
//        */
//
//        // Unit of Measure, pixel, cm, or inch
////        barcode.setUom(IBarcode.UOM_PIXEL);
//
//
//        // barcode bar module width (X) in pixel
//        barcode.setX(3.7f);
//        // barcode bar module height (Y) in pixel
//        barcode.setY(200f);
//
//        // barcode image margins
//        barcode.setLeftMargin(10f);
//        barcode.setRightMargin(10f);
//        barcode.setTopMargin(10f);
//        barcode.setBottomMargin(10f);
//
//        // barcode image resolution in dpi
//        barcode.setResolution(72);
//
//        // disply barcode encoding data below the barcode
//        barcode.setShowText(true);
//        // barcode encoding data font style
//        barcode.setTextFont(new AndroidFont("Arial", Typeface.NORMAL, 10));
//        // space between barcode and barcode encoding data
//        barcode.setTextMargin(6);
//        barcode.setTextColor(AndroidColor.black);
//
//        // barcode bar color and background color in Android device
//        barcode.setForeColor(AndroidColor.black);
//        barcode.setBackColor(AndroidColor.white);
//
//        /*
//        specify your barcode drawing area
//	    */
//        RectF bounds = new RectF(30, 30, 0, 0);
//        barcode.drawBarcode(canvas, bounds);


        Code128Writer writer = new Code128Writer();

        try {
            BitMatrix bm = writer.encode(user.identity, BarcodeFormat.CODE_128, 150, 150);

            Bitmap ImageBitmap = Bitmap.createBitmap(150, 60, Bitmap.Config.ARGB_8888);

            for (int i = 0; i < 150; i++) {//width
                for (int j = 0; j < 60; j++) {//height
                    ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK: Color.WHITE);
                }
            }

            imageBarCode.setImageBitmap(ImageBitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }

//        imageBarCode.addView(barCodeView);

        identity.setText(user.identity);



        what.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createSaveDialog();
            }
        });


        return rootView;
    }

    public void dateDialogBirth(final EditText editText) {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                editText.setText(String.format("%s / %s / %s", String.format("%02d", dayOfMonth),
                        String.format("%02d", monthOfYear + 1), String.format("%02d", year)));

                Calendar calFrom = Calendar.getInstance();

                calFrom.set(year, monthOfYear, dayOfMonth);

                user.passport_birthday = calFrom.getTimeInMillis() / SECOND_DIVIDER;

                wasEditTrip = true;
                editArrow.setVisibility(View.VISIBLE);
            }
        };

        Calendar calFrom = Calendar.getInstance();
        calFrom.setTimeInMillis(user.passport_birthday * SECOND_DIVIDER);

        if ( user.passport_birthday == 0 ) {
            DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), listener, 2000, calFrom.get(Calendar.MONTH), calFrom.get(Calendar.DAY_OF_MONTH));
            dpDialog.show();
        } else {
            DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), listener, calFrom.get(Calendar.YEAR), calFrom.get(Calendar.MONTH), calFrom.get(Calendar.DAY_OF_MONTH));
            dpDialog.show();
        }
    }

    public void dateDialogExpire(final EditText editText) {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                editText.setText(String.format("%s / %s / %s", String.format("%02d", dayOfMonth),
                        String.format("%02d", monthOfYear + 1), String.format("%02d", year)));

                Calendar calFrom = Calendar.getInstance();

                calFrom.set(year, monthOfYear, dayOfMonth);

                user.passport_date = calFrom.getTimeInMillis() / SECOND_DIVIDER;

                wasEditTrip = true;
                editArrow.setVisibility(View.VISIBLE);
            }
        };

        Calendar calFrom = Calendar.getInstance();
        calFrom.setTimeInMillis(user.passport_date * SECOND_DIVIDER);

        if ( user.passport_date == 0 ) {
            DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), listener, 2016, calFrom.get(Calendar.MONTH), calFrom.get(Calendar.DAY_OF_MONTH));
            dpDialog.show();
        } else {
            DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), listener, calFrom.get(Calendar.YEAR), calFrom.get(Calendar.MONTH), calFrom.get(Calendar.DAY_OF_MONTH));
            dpDialog.show();
        }
    }

    private void createSaveDialog(){
        final Dialog dialog = new Dialog( getContext(), R.style.Base_Theme_AppCompat_Dialog);
        dialog.setContentView(R.layout.dialog_what);

        final TextView ok      = (TextView)       dialog.getWindow().findViewById(R.id.call_support_call);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void createLogoutDialog(){
        final Dialog dialog = new Dialog( getContext(), R.style.Base_Theme_AppCompat_Dialog);
        dialog.setContentView(R.layout.dialog_logout);

        final TextView cancel    = (TextView)       dialog.getWindow().findViewById(R.id.account_cancel);
        final TextView logout    = (TextView)       dialog.getWindow().findViewById(R.id.account_yes);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO logout
                fragmentHolder.logout();
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void initialize() {
        fullName                = (MaterialEditText) rootView.findViewById(R.id.profile_fullname);
        phone                   = (MaterialEditText) rootView.findViewById(R.id.profile_phone);
        email                   = (MaterialEditText) rootView.findViewById(R.id.profile_email);
        country                 = (MaterialEditText) rootView.findViewById(R.id.profile_country);
        passport_citizenship    = (MaterialEditText) rootView.findViewById(R.id.profile_citizenship);
        passport_number         = (MaterialEditText) rootView.findViewById(R.id.profile_passport_number);
        passport_country        = (MaterialEditText) rootView.findViewById(R.id.profile_passport_country);
        passport_date           = (MaterialEditText) rootView.findViewById(R.id.profile_expiry_date);
        city                    = (MaterialEditText) rootView.findViewById(R.id.profile_city);
        address                 = (MaterialEditText) rootView.findViewById(R.id.profile_address);
        passport_birthday       = (MaterialEditText) rootView.findViewById(R.id.passport_birthday);
        passport_photo          = (MaterialEditText) rootView.findViewById(R.id.profile_passport_photo);
        profile_postcode        = (MaterialEditText) rootView.findViewById(R.id.profile_postcode);
        changePassword          = (MaterialEditText) rootView.findViewById(R.id.profile_change_password);

        passportImage           = (ImageView) rootView.findViewById(R.id.profile_image_photo);
        editArrow               = (ImageView) rootView.findViewById(R.id.details_save_edit);
        what                    = (ImageView) rootView.findViewById(R.id.profile_info);

        imageBarCode            = (ImageView) rootView.findViewById(R.id.profile_barcode);

        passport_sex            = (Spinner) rootView.findViewById(R.id.profile_passport_sex);

        logoutButton            = (Button) rootView.findViewById(R.id.logout_button);

        identity                = (TextView) rootView.findViewById(R.id.profile_identity);

        bottomSheet             = (BottomSheetLayout) rootView.findViewById(R.id.bottomsheet);
    }

    private void initFields() {
        try {
            user            = AppDelegate.getInstance().getUser();
            Calendar cal    = Calendar.getInstance();

            fullName.setText(user.full_name);

            if ( !user.phone.equals("null") ) {
//                phone.setText(String.format("%s %s", setCountryPhone(user.phone_country), user.phone));
                if (user.phone.startsWith("+") ) {
                    phone.setText(user.phone);
                } else {
                    phone.setText("+" + user.phone);
                }
            }

            email.setText(user.email);
            country.setText(setCountryName(user.country));
            passport_citizenship.setText(setCountryName(user.passport_citizenship));
            passport_country.setText(setCountryName(user.passport_country));
            passport_number.setText(user.passport_number);
            profile_postcode.setText(user.post_code);

            address.setText(user.address);

            city.setText(user.city);

            if ( user.passport_date > 0 ) {
                cal.setTimeInMillis(user.passport_date * 1000);
                passport_date.setText(String.format("%s / %s / %s", String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)),
                        String.format("%02d", cal.get(Calendar.MONTH) + 1), String.format("%02d", cal.get(Calendar.YEAR))));
            }

            if ( user.passport_birthday > 0 ) {
                cal.setTimeInMillis(user.passport_birthday * 1000);
                passport_birthday.setText(String.format("%s / %s / %s", String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)),
                        String.format("%02d", cal.get(Calendar.MONTH) + 1), String.format("%02d", cal.get(Calendar.YEAR))));
            }



//            File passportFile =  new File(getActivity().getExternalFilesDir(null), "passportPhoto.jpg");

            if ( AppDelegate.getInstance().getHasPassport() && user.passport_file.length() > 0) {
                passportImage.setVisibility(View.VISIBLE);
                imageLoader.displayImage(user.passport_file , passportImage, options);

                passport_photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showImage();
                    }
                });

            } else {
                passportImage.setVisibility(View.GONE);

                passport_photo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottomSheet.showWithSheetView(LayoutInflater.from(getActivity()).inflate(R.layout.my_sheet_layout, bottomSheet, false));
                        TextView txtTakePhoto   = (TextView) bottomSheet.findViewById(R.id.txtTakePhoto);
                        TextView txtGallery     = (TextView) bottomSheet.findViewById(R.id.txtGallery);

                        txtTakePhoto.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

                                    File mFile = new File(getActivity().getExternalFilesDir(null), "passportPhoto.jpg");
                                    Uri imageUri = Uri.fromFile(mFile);
                                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                    File passportImage =  new File(getActivity().getExternalFilesDir(null), "passportPhoto.jpg");

                                    if (passportImage.exists()) {
                                        passportImage.delete();
                                    }

                                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_PASSPORT);
                                }
                            }
                        });

                        txtGallery.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                File passportImage =  new File(getActivity().getExternalFilesDir(null), "passportPhoto.jpg");
                                if(passportImage.exists()) {
                                    passportImage.delete();
                                }

                                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(galleryIntent, REQUEST_PASSPORT);

                            }
                        });
                    }
                });

            }

        } catch (NullPointerException e) {}
    }

    public void myToast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    private String setCountryName(long country) {
        for ( int i = 0; i < countries.size(); i++ ) {
            if ( countries.get(i).id == country ) {
                return countries.get(i).name;
            }
        }

        return "";
    }

    private String setCountryPhone(long country) {
        for ( int i = 0; i < countries.size(); i++ ) {
            if ( countries.get(i).id == country ) {
                return countries.get(i).phone_code;
            }
        }

        return "";
    }

    @Override
    public void doWork() {
        initFields();
    }

    @Override
    public void saveChanges() {
        wasEditTrip = true;

        editArrow.setVisibility(View.VISIBLE);
    }

    @Override
    public void showImage(int position) {

    }

    @Override
    public void retakeReceipt(int id) {

    }

    @Override
    public void deleteReceipt(int id) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if ( fragmentHolder == null ) {
            fragmentHolder = (AppInterface) getActivity();
        }

        bottomSheet.dismissSheet();

        user = AppDelegate.getInstance().getUser();
        initFields();

        Log.d(TAG, "user.passport_sex: " + user.passport_sex);

        if ( wasEditTrip ) {
            editArrow.setVisibility(View.VISIBLE);
        }

        if ( user.errorMessage.length() > 0 ) {
            final Snackbar snackBar =
                    Snackbar.make(rootView.findViewById(R.id.content_ll), user.errorMessage, Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(getActivity().getResources().getColor(R.color.error_red));

            snackBar.setAction("Dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentHolder.setNotifications(0, 3);
                    snackBar.dismiss();
                }
            });
            snackBar.show();
        }


        String[]            data = {getString(R.string.male), getString(R.string.female)};

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, data);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        passport_sex.setAdapter(adapter);

        passport_sex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( position == 0 ) {
                    user.passport_sex = "m";
                } else if ( position == 1 ) {
                    user.passport_sex = "fm";
                }

                wasEditTrip = true;
                editArrow.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                passport_sex.setPrompt("sex");
            }
        });

        if ( user.passport_sex.equals("m") ){
            passport_sex.setSelection(0);
        } else if ( user.passport_sex.equals("fm") ){
            passport_sex.setSelection(1);
        }
    }

    @Override
    public void onPause() {
        user.full_name          = fullName.getText().toString();
        user.phone              = phone.getText().toString();
        user.email              = email.getText().toString();

        user.passport_number    = passport_number.getText().toString();
        user.city               = city.getText().toString();
        user.address            = address.getText().toString();
        user.post_code          = profile_postcode.getText().toString();

        AppDelegate.getInstance().saveUser(user);
        super.onPause();
    }

    public void showImage() {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);

        dialog.setContentView(R.layout.dialog_passport_layout);

        TouchImageView image      = (TouchImageView)  dialog.getWindow().findViewById(R.id.dialog_image);
        ImageView cancel          = (ImageView)  dialog.getWindow().findViewById(R.id.dialog_image_cancel);
        ImageView retake          = (ImageView)  dialog.getWindow().findViewById(R.id.dialog_image_retake);
        TextView  reject          = (TextView)  dialog.getWindow().findViewById(R.id.dialog_image_reject);

        imageLoader.displayImage(user.passport_file, image, options);

        if ( user.errorMessage.length() > 5 ) {
            reject.setText(user.errorMessage);
            dialog.getWindow().findViewById(R.id.dialog_main).setBackgroundResource(R.color.error_red);
        }

        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                File passportImage =  new File(getActivity().getExternalFilesDir(null), "passportPhoto.jpg");
                if(passportImage.exists()) {
                    passportImage.delete();
                }

                dialog.dismiss();
                bottomSheet.showWithSheetView(LayoutInflater.from(getActivity()).inflate(R.layout.my_sheet_layout, bottomSheet, false));
                TextView txtTakePhoto   = (TextView) bottomSheet.findViewById(R.id.txtTakePhoto);
                TextView txtGallery     = (TextView) bottomSheet.findViewById(R.id.txtGallery);

                txtTakePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

                            File mFile = new File(getActivity().getExternalFilesDir(null), "passportPhoto.jpg");
                            Uri imageUri = Uri.fromFile(mFile);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

                            startActivityForResult(takePictureIntent, REQUEST_IMAGE_PASSPORT);
                        }
                    }
                });

                txtGallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, REQUEST_PASSPORT);

                    }
                });
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
         if (requestCode == REQUEST_IMAGE_PASSPORT && resultCode == RESULT_OK) {
            File mFile = new File(getActivity().getExternalFilesDir(null), "passportPhoto.jpg");

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            Bitmap bitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath(), options);

            fragmentHolder.savePhotoPassport(bitmap);
        } else if ( requestCode == REQUEST_PASSPORT && resultCode == RESULT_OK ) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                Bitmap bmp = BitmapFactory.decodeFile(picturePath);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                fragmentHolder.savePhotoPassport(bmp);
            }
        }
    }

}