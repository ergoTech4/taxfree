package eu.taxfree4u.client.fragments;


import android.content.Context;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Locale;

import eu.taxfree4u.client.R;
import eu.taxfree4u.client.activities.MainActivity;
import eu.taxfree4u.client.adapters.CountriesAdapter;
import eu.taxfree4u.client.createTrip.CreateTripActivity;
import eu.taxfree4u.client.createTrip.RegistrationStep3Fragment;
import eu.taxfree4u.client.interfaces.AppInterface;
import eu.taxfree4u.client.interfaces.CreateTripInterface;
import eu.taxfree4u.client.model.Country;
import eu.taxfree4u.client.tools.AppDelegate;

public class CountriesChoicerFragment extends Fragment {
    public static final String TAG = "CountriesChoicerFragment";

    private View                rootView;
    private EditText            search;
    private ListView            listView;
    private CountriesAdapter    adapter;
    private ArrayList<Country>  countries, euroCountries;

    private CreateTripInterface fragmentHolder;
    private AppInterface        appInterface;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if ( context instanceof CreateTripActivity ) {
                fragmentHolder = (CreateTripInterface) context;
            } else if ( context instanceof MainActivity) {
                appInterface = (AppInterface) context;
            }

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString());
        }
    }

    public static CountriesChoicerFragment newInstance(String tag) {
        CountriesChoicerFragment fragment = new CountriesChoicerFragment();

        Bundle args = new Bundle();
        args.putString("tag", tag);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fr_countries_choiser, container, false);

        listView    = (ListView) rootView.findViewById(R.id.countries_choise_list);
        search      = (EditText) rootView.findViewById(R.id.search_countries);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = search.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( fragmentHolder == null && getActivity() instanceof CreateTripActivity ) {
            fragmentHolder = (CreateTripInterface) getActivity();
        } else if ( appInterface == null && getActivity() instanceof MainActivity ) {
            appInterface = (AppInterface) getActivity();
        }


        countries       = AppDelegate.getInstance().getCountries();
        euroCountries   =  new ArrayList<>();

        for ( int i = 0; i < countries.size(); i++ ) {
            if (countries.get(i).is_europe) {
                euroCountries.add(countries.get(i));
            }
        }

        if ( RegistrationStep3Fragment.TAG_DEPARTURE.equals(getArguments().getString("tag"))
                || DetailsFragment.TAG_DEP.equals(getArguments().getString("tag")) ) {
            if (fragmentHolder != null) {
                adapter = new CountriesAdapter(euroCountries, getActivity(), getArguments().getString("tag"), fragmentHolder, null);
            } else if (appInterface != null) {
                adapter = new CountriesAdapter(euroCountries, getActivity(), getArguments().getString("tag"), null, appInterface);
            }
        } else {
            if (fragmentHolder != null) {
                adapter = new CountriesAdapter(countries, getActivity(), getArguments().getString("tag"), fragmentHolder, null);
            } else if (appInterface != null) {
                adapter = new CountriesAdapter(countries, getActivity(), getArguments().getString("tag"), null, appInterface);
            }
        }

        listView.setAdapter(adapter);

        if ( appInterface != null ) {
            appInterface.hideBottomMenu(true);
        }
    }

    @Override
    public void onPause() {
        if ( appInterface != null ) {
            appInterface.hideBottomMenu(false);
            appInterface.prepareFragmentStack();
        }
        super.onPause();
    }
}
