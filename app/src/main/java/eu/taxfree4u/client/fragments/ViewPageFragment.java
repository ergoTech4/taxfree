package eu.taxfree4u.client.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import eu.taxfree4u.client.R;

public class ViewPageFragment extends Fragment {
    private static final String TAG = "ViewPageFragment";
    private ImageView   imageView, logo;
    private TextView    previewTv;
    public String       text;
    private int         background;
    private int         textColor;

    public static Fragment instantiate(String text, int image_preview4, int color) {
        ViewPageFragment fragment = new ViewPageFragment();
        Bundle          preview1 = new Bundle();

        preview1.putString("text", text);
        preview1.putInt("image", image_preview4);
        preview1.putInt("color", color);

        fragment.setArguments(preview1);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_main_start, container, false);

        imageView   = (ImageView) view.findViewById(R.id.main_image);
        previewTv   = (TextView)  view.findViewById(R.id.preview_text);
        logo        = (ImageView) view.findViewById(R.id.image_logo);

        text        = getArguments().getString("text");
        background  = getArguments().getInt("image");
        textColor   = getArguments().getInt("color");


        if ( R.color.white == textColor ) {
            logo.setVisibility(View.GONE);
        }

        imageView.setImageResource(background);
        previewTv.setText(text);
        previewTv.setTextColor(getResources().getColor(textColor));

        return view;
    }
}
